#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    string entityParser(string text) {
        int len = text.size();
        string ans = "";
        for (int i = 0; i < len; i++) {
            int j = 0;
            if (text[i] == '&') {
                if (i + 5 < len && text[i + 1] == 'q') {
                    if (text.substr(i, 6) == "&quot;") {
                        ans += "\"";
                        i += 5;
                    }
                    else ans += "&";
                }
                else if (i + 5 < len && text[i + 2] == 'p') {
                    if (text.substr(i, 6) == "&apos;") {
                        ans += "\'";
                        i += 5;
                    }
                    else ans += "&";
                }
                else if (i + 4 < len && text[i + 2] == 'm') {
                    if (text.substr(i, 5) == "&amp;") {
                        ans += "&";
                        i += 4;
                    }
                    else ans += "&";
                }
                else if (i + 3 < len && text[i + 1] == 'g') {
                    if (text.substr(i, 4) == "&gt;") {
                        ans += ">";
                        i += 3;
                    }
                    else ans += "&";
                }
                else if (i + 3 < len && text[i + 1] == 'l') {
                    if (text.substr(i, 4) == "&lt;") {
                        ans += "<";
                        i += 3;
                    }
                    else ans += "&";
                }
                else if (i + 6 < len && text[i + 1] == 'f') {
                    if (text.substr(i, 7) == "&frasl;") {
                        ans += "/";
                        i += 6;
                    }
                    else ans += "&";
                }
                else {
                    ans += text[i];
                }
            }
            else {
                ans += text[i];
            }
        }
        return ans;
    }
};