#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
typedef long long LL;
const int N = 1e5 + 10;
LL sa[N];
LL sb[N];
int main() {
    int n, k;
    cin >> n >> k;

    for (int i = 1; i <= n; i++) {
        int x;
        cin >> x;
        sa[i] = sa[i - 1] + x;
    }
    for (int i = 1; i <= n; i++) {
        int x;
        cin >> x;
        sb[i] = sb[i - 1] + x;
    }

    LL res1 = 0;//����ֵ
    LL res2 = 1e9;//�߳�
    int ans = 0;
    for (int i = 1; i + k - 1 <= n; i++) {
        LL a = sa[i + k - 1] - sa[i - 1];
        LL b = sb[i + k - 1] - sb[i - 1];
        if (res1 <= a) {
            if (res1 == a && res2 >= b) {
                if (res2 == b) {
                    ans = min(ans, i);
                }
                else {
                    res2 = b;
                    ans = i;
                }
            }
            else {
                res1 = a;
                res2 = b;
                ans = i;
            }
        }
        // cout<<res1<<" "<<res2<<endl;
    }
    cout << ans << endl;
    return 0;
}
