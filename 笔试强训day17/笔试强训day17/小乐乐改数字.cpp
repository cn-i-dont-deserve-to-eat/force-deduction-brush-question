#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<string>
using namespace std;

int main() {
    string str;
    cin >> str;
    int ans = 0;
    for (int i = 0; i < str.size(); i++) {
        int u = str[i] - '0';
        if (u % 2)ans = ans * 10 + 1;
        else ans *= 10;
    }
    cout << ans << endl;
    return 0;
}
