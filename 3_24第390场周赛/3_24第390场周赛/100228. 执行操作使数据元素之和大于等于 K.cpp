#define _CRT_SECURE_NO_WARNINGS 1

class Solution {
public:

    int minOperations(int k) {
        int ans = 1e9;
        if (k == 1)return 0;
        for (int i = 0; i <= k - 1; i++) {
            int a = i + 1;
            int b = (k + 1 + i - 1) / (i + 1) - 1;
            ans = min(ans, a - 1 + b);
        }
        return ans;
    }
};