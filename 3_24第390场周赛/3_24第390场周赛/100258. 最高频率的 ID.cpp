#define _CRT_SECURE_NO_WARNINGS 1
typedef long long LL;
typedef pair<LL, int> PII;
class Solution {
public:
    vector<long long> mostFrequentIDs(vector<int>& nums, vector<int>& freq) {
        int n = nums.size();
        vector<LL> ans(n);
        priority_queue<PII> q;
        for (int i = 0; i < n; i++)q.push({ 0,nums[i] });
        int cnt = 0;
        unordered_map<int, LL> mp;
        for (int i = 0; i < n; i++) {
            mp[nums[i]] += freq[i];
            q.push({ mp[nums[i]],nums[i] });
            auto it = q.top();
            while (mp[it.second] != it.first && q.size()) {
                q.pop();
                it = q.top();
            }
            ans[i] = it.first;
        }
        return ans;
    }
};