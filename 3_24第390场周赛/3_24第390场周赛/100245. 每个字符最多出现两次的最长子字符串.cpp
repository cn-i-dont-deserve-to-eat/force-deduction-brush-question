#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int maximumLengthSubstring(string s) {
        int a[26] = { 0 };
        int ans = 0;

        for (int i = 0, j = 0; i < s.size(); i++) {
            int u = s[i] - 'a';
            a[u]++;
            while (a[u] > 2 && j < s.size()) {
                int k = s[j] - 'a';
                a[k]--;
                j++;
            }
            ans = max(ans, i - j + 1);
        }
        return ans;
    }
};