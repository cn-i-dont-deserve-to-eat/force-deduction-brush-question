#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
const int N = 1e7 + 10;
int a[N];
int main() {
    int n;
    int x;
    cin >> n >> x;
    for (int i = 1; i <= n; i++) {
        cin >> a[i];
    }
    int ans = 1e9;
    long long s = 0;
    int ml = 0;

    for (int r = 1, l = 1; r <= n; r++) {
        s += a[r];
        while (l <= r && s >= x) {
            if (r - l + 1 < ans) {
                ml = l;
                ans = r - l + 1;
            }
            s -= a[l];
            l++;
        }
    }
    cout << ml << " " << ml + ans - 1 << endl;
    return 0;
}
// 64 λ������� printf("%lld")