#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<queue>
using namespace std;

int main() {
    int n, k;
    cin >> n >> k;
    priority_queue<int> q;
    long long ans = 0;
    for (int i = 1; i <= n; i++) {
        int x;
        cin >> x;
        ans = (long long)ans + x;
        if (x % 2 == 0) q.push(x);
    }

    k = min(k, n * 32);
    //cout<<ans<<endl;
    while (k-- && !q.empty()) {
        int t = q.top();
        q.pop();
        ans = (long long)ans - t / 2;
        if (t / 2 % 2 == 0)q.push(t / 2);
    }
    cout << ans << endl;
    return 0;
}
// 64 λ������� printf("%lld")  