#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int findChampion(int n, vector<vector<int>>& edges) {
        unordered_set<int> p;
        for (auto it : edges) {
            int b = it[1];
            if (!p.count(b)) p.insert(b);
        }
        if (p.size() != n - 1)return -1;
        else {
            int ans = n * (n - 1) / 2;
            for (auto it : p) {
                ans -= it;
            }
            return  ans;
        }

    }
};