#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<int> successfulPairs(vector<int>& spells, vector<int>& potions, long long success) {
        int n = spells.size();
        vector<int> ans(n);
        sort(potions.begin(), potions.end());

        int len = potions.size();
        for (int i = 0; i < n; i++) {
            int k = spells[i];
            // int x=success/k;
            int l = -1;
            int r = potions.size();
            while (l + 1 != r) {
                int mid = (l + r) >> 1;
                if ((long long)potions[mid] * k >= success)r = mid;
                else l = mid;
            }
            if (r >= 0 && r < len) {
                // cout<<r<<endl;
                int num = len - r;
                ans[i] = num;
            }
            else {
                ans[i] = 0;
            }

        }
        return ans;

    }
};