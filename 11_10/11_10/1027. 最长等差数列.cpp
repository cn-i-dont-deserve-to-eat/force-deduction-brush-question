#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int longestArithSeqLength(vector<int>& nums) {
        int min_n = 1e9;
        int max_n = 0;
        int len = nums.size();
        for (int i = 0; i < len; i++) {
            min_n = min(min_n, nums[i]);
            max_n = max(max_n, nums[i]);
        }
        int ans = 0;
        int max_d = max_n - min_n;
        int min_d = min_n - max_n;
        int cnt = 0;

        // dp[nums[0]]=1;
        int k = 510;

        for (int d = min_d; d <= max_d; d++) {
            //dp.clear();
            vector<int> dp(2000, 0);
            // map<int,int> dp;
            cnt = 0;
            for (int i = 0; i < len; i++) {
                dp[nums[i] + k] = dp[nums[i] + k - d] + 1;
                cnt = max(cnt, dp[nums[i] + k]);
            }
            ans = max(ans, cnt);
            if (ans > len / 2)break;

        }
        return ans;
    }
};