#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int longestSubsequence(vector<int>& arr, int difference) {
        int len = arr.size();
        vector<int>dp(40010);
        int k = 2e4 + 10;
        dp[arr[0] + k] = 1;
        for (int i = 1; i < len; i++) {
            dp[arr[i] + k] = dp[arr[i] + k - difference] + 1;
            //   cout<<
               //cout<<dp[arr[i]+k]<<endl;
        }
        int ans = 0;
        for (int i = 0; i < 40010; i++) {
            ans = max(dp[i], ans);
        }
        return ans;
    }
};