#define _CRT_SECURE_NO_WARNINGS 1

class Solution {
public:
    int findLongestChain(vector<vector<int>>& pairs) {
        int len = pairs.size();
        sort(pairs.begin(), pairs.end());
        vector<int> dp(len + 1, 1);
        dp[0] = 1;
        for (int i = 1; i < len; i++) {
            for (int j = 0; j < i; j++) {
                if (pairs[i][0] > pairs[j][1]) {
                    dp[i] = max(dp[i], dp[j] + 1);
                }
            }
        }

        int ans = 0;
        for (int i = 0; i < len; i++) {
            // cout<<dp[i]<<endl;
            ans = max(ans, dp[i]);
        }
        return ans;
    }
};