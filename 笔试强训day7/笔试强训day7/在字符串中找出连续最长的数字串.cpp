#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<string>
using namespace std;

int main() {
    string str;
    cin >> str;
    int ans = 0;
    int star = 0;
    for (int i = 0; i < str.size(); i++) {
        if (str[i] >= '0' && str[i] <= '9') {
            int j = i;
            while (j < str.size() && str[j] >= '0' && str[j] <= '9') {
                j++;
            }
            if (j - i > ans) {
                star = i;
                ans = j - i;
            }
            i = j;
        }
    }
    cout << str.substr(star, ans);
    return 0;
}
