#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<cstring>
#include<vector>
using namespace std;
int a[6] = { 0 };
bool st[6];
bool ans;
bool is_triangle(int a, int b, int c) {
    if (a + b <= c || a + c <= b || b + c <= a)return false;
    return true;
}

void dfs(int u, vector<int> t) {
    if (u == 6) {
        if (is_triangle(t[0], t[1], t[2]) && is_triangle(t[3], t[4], t[5]))ans = true;
        return;
    }

    for (int i = 0; i < 6; i++) {
        if (st[i])continue;
        st[i] = true;
        t.push_back(a[i]);
        dfs(u + 1, t);
        st[i] = false;
        t.pop_back();
    }
}

int main() {
    int t;

    cin >> t;
    while (t--) {
        ans = false;
        memset(st, 0, sizeof st);
        for (int i = 0; i < 6; i++)cin >> a[i];
        vector<int> t;
        dfs(0, t);
        if (ans)cout << "Yes\n";
        else cout << "No\n";
    }
    return 0;
}
