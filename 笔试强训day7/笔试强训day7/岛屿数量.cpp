#define _CRT_SECURE_NO_WARNINGS 1
const int N = 210;
typedef pair<int, int> PII;
bool st[N][N];
int dx[] = { 0,0,-1,1 };
int dy[] = { -1,1,0,0 };
int m, n;
class Solution {
public:
    void bfs(int x, int y, vector<vector<char> >& grid) {
        queue<PII> q;
        q.push({ x,y });
        st[x][y] = true;

        while (!q.empty()) {
            auto it = q.front();
            q.pop();
            int aa = it.first;
            int bb = it.second;
            for (int i = 0; i < 4; i++) {
                int a = aa + dx[i];
                int b = bb + dy[i];

                if (a >= 0 && a < m && b >= 0 && b < n && !st[a][b] && grid[a][b] == '1') {
                    st[a][b] = true;
                    q.push({ a,b });
                }
            }
        }
    }
    int solve(vector<vector<char> >& grid) {
        m = grid.size();
        n = grid[0].size();
        int ans = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (!st[i][j] && grid[i][j] == '1') {
                    ans++;
                    bfs(i, j, grid);
                }
            }
        }
        return  ans;
    }
};