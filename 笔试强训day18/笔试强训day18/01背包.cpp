#define _CRT_SECURE_NO_WARNINGS 1��
class Solution {
public:
    int knapsack(int V, int n, vector<vector<int> >& vw) {
        vector<int> dp(V + 1);

        for (int i = 1; i <= n; i++) {
            for (int j = V; j >= vw[i - 1][0]; j--) {
                dp[j] = max(dp[j - vw[i - 1][0]] + vw[i - 1][1], dp[j]);
            }
        }
        return dp[V];
    }
};