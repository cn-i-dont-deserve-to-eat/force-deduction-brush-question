#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<algorithm>
using namespace std;
const int N = 2e5 + 10;
struct orange {
    int t;
    int s;
}o[N];

bool cmp(orange& a, orange& b) {
    if (a.t == b.t)return a.s < b.s;
    return a.t > b.t;
}

int main() {
    int n, k;
    cin >> n >> k;
    for (int i = 0; i < n; i++) {
        cin >> o[i].s;
    }
    for (int i = 0; i < n; i++)cin >> o[i].t;

    sort(o, o + n, cmp);
    long long ans1 = 0, ans2 = 0;
    for (int i = 0; i < k; i++) {
        ans1 += o[i].t;
        ans2 += o[i].s;
        //cout<<o[i].s<<"--- "<<o[i].t;
    }
    cout << ans2 << " " << ans1 << endl;
}
