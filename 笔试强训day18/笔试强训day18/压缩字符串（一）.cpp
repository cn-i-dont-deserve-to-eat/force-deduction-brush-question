#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param param string字符串
     * @return string字符串
     */
    string compressString(string param) {
        int n = param.size();
        if (n <= 1)return param;
        string ans = "";
        int i, j;
        for (i = 1, j = 0; i < n; i++) {
            if (param[i] != param[i - 1]) {
                ans += param[j];
                if (i - j > 1) {
                    ans += to_string(i - j);
                }
                j = i;
            }
        }
        ans += param[j];
        if (n - j > 1) {
            ans += to_string(n - j);
        }
        return ans;
    }
};