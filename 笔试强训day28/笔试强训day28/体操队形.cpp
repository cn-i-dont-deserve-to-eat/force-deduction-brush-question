#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<vector>
using namespace std;
const int N = 11;
int a[N];
int n;
bool st[N];
int pos[N];
int ans;
void dfs(int u, vector<int> p) {
    if (u == n + 1) {
        for (int i = 0; i < n; i++) {
            pos[p[i]] = i + 1;
        }
        int f = 0;
        for (int i = 1; i <= n; i++) {
            if (a[i] == i)continue;
            if (pos[a[i]] >= pos[i]) {
                f = 1;
                break;
            }
        }
        if (!f)ans++;
    }

    for (int i = 1; i <= n; i++) {
        if (!st[i]) {
            p.push_back(i);
            st[i] = true;
            dfs(u + 1, p);
            st[i] = false;
            p.pop_back();
        }
    }
}

int main() {

    cin >> n;
    for (int i = 1; i <= n; i++) {
        cin >> a[i];
    }
    vector<int> p;
    dfs(1, p);
    cout << ans << endl;
    return 0;
}
