#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

int main() {
    int q;
    cin >> q;
    while (q--) {
        string num;
        cin >> num;
        int n = num.size();
        if (num[n - 1] % 2 == 0) {
            cout << num << endl;
            continue;
        }
        int pos = -1;
        for (int i = 0; i < num.size(); i++) {
            if (num[i] % 2 == 0) {
                pos = i;
                break;
            }
        }
        if (pos == -1) {
            cout << -1 << endl;
        }
        else {
            swap(num[pos], num[num.size() - 1]);
            cout << num << endl;
        }
    }
    return 0;
}
