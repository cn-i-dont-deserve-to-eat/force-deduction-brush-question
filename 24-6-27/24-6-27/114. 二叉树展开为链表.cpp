#define _CRT_SECURE_NO_WARNINGS 1
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

TreeNode* newTreeNode;
class Solution {
public:

    void getpre(TreeNode* root) {
        if (root == nullptr)return;
        newTreeNode->right = root;
        newTreeNode->left = nullptr;
        newTreeNode = newTreeNode->right;
        TreeNode* r = root->right;
        getpre(root->left);
        getpre(r);
    }
    void flatten(TreeNode* root) {
        if (!root)return;
        TreeNode* ans = new TreeNode(-1);
        newTreeNode = ans;
        getpre(root);
        root = ans->right;
    }
};