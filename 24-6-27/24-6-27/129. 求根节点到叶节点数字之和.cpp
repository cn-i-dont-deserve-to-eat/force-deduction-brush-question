#define _CRT_SECURE_NO_WARNINGS 1
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

long long ans;
class Solution {
public:
    void fun(TreeNode* root, int s) {
        if (root == nullptr) {
            return;
        }
        s = s * 10 + root->val;
        if (root->left)fun(root->left, s);
        if (root->right)fun(root->right, s);
        if (root->left == nullptr && root->right == nullptr) {
            // cout<<s<<endl;
            ans += s;
        }
    }

    int sumNumbers(TreeNode* root) {
        int s = 0;
        ans = 0;
        fun(root, s);
        return ans;
    }
};