//#define _CRT_SECURE_NO_WARNINGS 1
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//
//void ShowValueBin(short int bits, short int value)
//{
//    int i;
//    printf("%d的二进制为:", value);
//    for (i = bits - 1; i >= 0; i--)
//    {
//        if (value & (1 << i))
//        {
//            printf("1");
//        }
//        else
//        {
//            printf("0");
//        }
//    }
//}
//
//int GetSign(short int iNum)//获取符号位
//{
//    int iSign = iNum >> 15;
//    if (iSign)//高位为1为负数
//        return 1;
//    else
//        return 0;
//}
//
///*输出要求
//-4的反码二进制为:11111111111111111111111111111011
//形式
//*/
//short int Ones_complement_Code(short int iNum)//反码
//{
//    /*todo*/
//    if (GetSign(iNum)) {
//        iNum^=(-1);
//        iNum|=(1<<15);
//        return iNum;
//    }
//    return iNum;
//}
//
///*输出要求
//-4的补码二进制为:11111111111111111111111111111100
//形式
//*/
//int  Complemental_Code(int iNum)//补码
//{
//    /*todo*/
//    if (GetSign(iNum)) {//负数
//        return Ones_complement_Code(iNum) + 1;
//    }
//    else {
//        return iNum;
//    }
//}
//
///*输出要求
//-4的真值二进制为:-100
//形式
//*/
//int True_Code(short int iNum)
//{
//    /*todo*/
//    if (GetSign(iNum)) {
//        return Complemental_Code(iNum);
//    }
//    else {
//        return iNum;
//    }
//
//}
//
//
//
//
///*输出要求
//-4的移码二进制为:01111111111111111111111111111100
//形式
//*/
//int Frame_Shift(short int iNum)//移码
//{
//    /*todo*/
//    return Complemental_Code(iNum) ^ (1 << 15);
//
//}
//
//
//int main(int argc, char* argv[])
//{
//    /*****************************************************
//    1. 正数：
//    正数的原码、反码、补码都相同。
//    2. 负数：
//    原码：负数的绝对值，并且最高位为1。
//    反码：原码的符号位不变，其他位按位取反。
//    补码：在反码的基础上+1。
//    3. 移码：
//    无论是正/负数，都是在补码的基础上，符号位取反。
//    ******************************************************/
//
//
//    printf("int 型的长度为：%d位\n", sizeof(short int) * 8);
//    printf("接收输入的int型10进制数，输出真值、原码、反码、补码、移码。\n");
//    /*todo
//    接受输入的10进制数，输出真值、原码、反码、补码、移码。
//    */
//
//
//    short int True_Code1 = True_Code(4);
//    short int True_Code2 = True_Code(-4);
//
//    short int Complemental_Code1 = Complemental_Code(4);
//    short int Complemental_Code2 = Complemental_Code(-4);
//
//    short int Ones_complement1 = Ones_complement_Code(4);
//    short int Ones_complement2 = Ones_complement_Code(-4);
//
//    short int Frame_Shift1 = Frame_Shift(4);
//    short int Frame_Shift2 = Frame_Shift(-4);
//
//    printf("True_Code1=%d\r\n", True_Code1);
//    printf("True_Code2=%d\r\n", True_Code2);
//
//    printf("Complemental_Code1=%d\r\n", Complemental_Code1);
//    printf("Complemental_Code2=%d\r\n", Complemental_Code2);
//
//    printf("Ones_complement1=%d\r\n", Ones_complement1);
//    printf("Ones_complement2=%d\r\n", Ones_complement2);
//
//    printf("Frame_Shift1=%d\r\n", Frame_Shift1);
//    printf("Frame_Shift2=%d\r\n", Frame_Shift2);
//
//    printf("\r\n");
//    ShowValueBin(16, 4);
//    printf("\r\n");
//    ShowValueBin(16, -4);
//    printf("\r\n");
//
//    return 0;
//
//}
