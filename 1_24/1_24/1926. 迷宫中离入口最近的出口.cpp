#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
#include<vector>
#include<queue>
using namespace std;
int dx[4] = { 0,-1,0,1 };
int dy[4] = { -1,0,1,0 };
typedef pair<int, int> PII;
class Solution {
public:
    int bfs(vector<vector<char>>& maze, vector<int>& entrance) {
        queue<PII> q;
        q.push({ entrance[0],entrance[1] });
        int m = maze.size();
        int n = maze[0].size();
        bool vis[m][n];
        memset(vis, false, sizeof vis);
        int d = 0;
        while (!q.empty()) {
            int sz = q.size();
            for (int i = 0; i < sz; i++) {
                auto it = q.front();
                q.pop();
                int x = it.first;
                int y = it.second;
                if (vis[x][y]) {
                    continue;
                }
                vis[x][y] = true;
                if ((x == 0 || x == m - 1 || y == 0 || y == n - 1) && maze[x][y] == '.' && d)return d;

                for (int j = 0; j < 4; j++) {
                    int a = x + dx[j];
                    int b = y + dy[j];
                    if (a >= 0 && a < m && b >= 0 && b < n && maze[a][b] == '.' && !vis[a][b]) {
                        q.push({ a,b });
                    }
                }

            }
            d++;
        }
        return -1;
    }
    int nearestExit(vector<vector<char>>& maze, vector<int>& entrance) {
        return bfs(maze, entrance);
    }
};  

int main() {



    return 0;
}