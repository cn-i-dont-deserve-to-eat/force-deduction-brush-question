
class Solution {
public:
    int longestAlternatingSubarray(vector<int>& nums, int threshold) {
        int ans = 0;

        for (int i = 0; i < nums.size(); i++) {
            int j = i + 1;
            int cnt = 0;
            if (nums[i] > threshold) {
                continue;
            }
            if (nums[i] % 2 == 0) {
                cnt = 1;
                while (j < nums.size() && nums[j] % 2 != nums[j - 1] % 2 && nums[j] <= threshold) {
                    j++;
                    cnt++;
                }
                ans = max(ans, cnt);
                i = j - 1;
            }
        }
        return ans;
    }
};