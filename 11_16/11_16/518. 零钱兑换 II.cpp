#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int change(int amount, vector<int>& coins) {
        sort(coins.begin(), coins.end());
        int len = coins.size();

        vector<int> dp(amount + 1);
        dp[0] = 1;
        for (int i = 0; i < len; i++) {
            int res = 0;

            for (int j = 1; j <= amount; j++) {
                if (coins[i] <= j)
                    dp[j] += dp[j - coins[i]];
            }
            // dp[i]=res;
            // cout<<dp[j]<<" ";

        }
        return dp[amount];
    }
};