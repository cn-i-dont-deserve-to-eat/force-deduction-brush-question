#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int combinationSum4(vector<int>& nums, int target) {
        int len = nums.size();
        sort(nums.begin(), nums.end());
        vector<unsigned long long> dp(target + 1);
        dp[0] = 1;
        for (int i = 1; i <= target; i++) {
            for (int j = 0; j < len; j++) {
                if (nums[j] > i)break;
                dp[i] = (unsigned long long)dp[i] + dp[i - nums[j]];
            }
        }
        return dp[target];
    }
};