#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:

    int findMaxForm(vector<string>& strs, int m, int n) {
        int len = strs.size();
        //   vector<vector<int>> num(len,vector<int>(2));
        vector<vector<int>>dp(m + 1, vector<int>(n + 1));

        for (int i = 1; i <= len; i++) {
            int n0 = 0;
            int n1 = 0;
            for (int z = 0; z < strs[i - 1].size(); z++) {
                if (strs[i - 1][z] == '0')n0++;
                else n1++;
            }

            for (int j = m; j >= 0; j--) {
                for (int k = n; k >= 0; k--) {
                    dp[j][k] = dp[j][k];
                    if (j >= n0 && k >= n1) {
                        dp[j][k] = max(dp[j][k], dp[j - n0][k - n1] + 1);
                    }
                }
            }

        }
        return dp[m][n];
    }
};