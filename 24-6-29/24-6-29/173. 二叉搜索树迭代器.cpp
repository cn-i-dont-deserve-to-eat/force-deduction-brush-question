#define _CRT_SECURE_NO_WARNINGS 1

class BSTIterator {
    void getfun(TreeNode* root) {
        if (root == nullptr)return;

        getfun(root->left);
        arr.push_back(root->val);
        getfun(root->right);
    }
public:
    BSTIterator(TreeNode* root) {
        getfun(root);
        idx = 0;
    }

    int next() {
        return arr[idx++];
    }

    bool hasNext() {
        if (idx >= 0 && idx < arr.size())return true;
        return false;
    }


    vector<int> arr;
    int idx;
};

