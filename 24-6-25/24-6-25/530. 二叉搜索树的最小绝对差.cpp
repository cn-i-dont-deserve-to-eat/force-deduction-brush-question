#define _CRT_SECURE_NO_WARNINGS 1

class Solution {
public:

    void getfun(TreeNode* root, vector<int>& v) {
        if (root == nullptr)return;
        v.push_back(root->val);
        getfun(root->left, v);
        getfun(root->right, v);
    }
    int getMinimumDifference(TreeNode* root) {
        vector<int> t;
        getfun(root, t);
        sort(t.begin(), t.end());

        int ans = 1e9;

        for (int i = 1; i < t.size(); i++) {
            ans = min(ans, t[i] - t[i - 1]);
        }
        return ans;
    }
};