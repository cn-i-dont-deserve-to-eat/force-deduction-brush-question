#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int findNonMinOrMax(vector<int>& nums) {
        int mi = 1e9;
        int ma = 0;
        for (int i = 0; i < nums.size(); i++) {
            mi = min(mi, nums[i]);
            ma = max(ma, nums[i]);
        }
        for (int i = 0; i < nums.size(); i++) {
            if (nums[i] != mi && nums[i] != ma) {
                return nums[i];
            }
        }
        return -1;
    }
};