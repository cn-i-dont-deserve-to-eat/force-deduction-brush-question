#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    long long minCost(vector<int>& nums, int x) {
        int n = nums.size();
        long long sum[n + 1];
        for (int i = 0; i < n; i++) {
            sum[i] = (long long)i * x;
        }
        for (int i = 0; i < n; i++) {
            int m = nums[i];
            for (int j = i; j < n + i; j++) {
                m = min(m, nums[j % n]);
                sum[j - i] += m;
            }
        }
        long long ans = 2e18;
        for (int i = 0; i < n; i++) {
            ans = min(ans, sum[i]);
        }
        return ans;
    }
};