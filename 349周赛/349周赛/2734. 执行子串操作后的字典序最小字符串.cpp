#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    string smallestString(string s) {
        string ans = "";
        int len = s.size();
        int pos = -1;
        for (int i = 0; i < s.size(); i++) {
            if (s[i] == 'a') {
                ans += s[i];
            }
            else {
                pos = i;
                break;
            }
        }
        if (pos == -1) {
            string str = "";
            if (len == 1)return "z";
            for (int i = 0; i < len; i++) {
                if (i != len - 1)str += 'a';
                else str += 'z';
            }
            return str;
        }
        ans += (s[pos] - 1);
        int f = 0;
        for (int i = pos + 1; i < len; i++) {
            if (s[i] == 'a' || f == 1) {
                ans += s[i];
                f = 1;
            }
            else if (s[i] != 'a' && f == 0) {
                ans += (s[i] - 1);
            }
        }
        return ans;
    }
};