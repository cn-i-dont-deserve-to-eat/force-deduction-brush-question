class Solution {
public:
    vector<int> getSubarrayBeauty(vector<int>& nums, int k, int x) {
        int len = nums.size();
        vector<int> ans;
        map<int, int> mp;
        for (int i = 0; i < k; i++) {
            mp[nums[i]]++;
        }
        int cnt = x;
        for (int i = -50; i <= 50; i++) {
            cnt -= mp[i];
            if (cnt <= 0) {
                ans.push_back(i);
                break;
            }
        }

        for (int i = k; i < len; i++) {
            mp[nums[i]]++;
            mp[nums[i - k]]--;
            int cnt = x;
            for (int i = -50; i <= 0; i++) {
                cnt -= mp[i];
                if (cnt <= 0) {
                    ans.push_back(i);
                    break;
                }
            }
            if (cnt > 0) {
                ans.push_back(1);
            }
        }
        for (int i = 0; i < ans.size(); i++) {
            if (ans[i] > 0)ans[i] = 0;
        }
        return ans;
    }
};