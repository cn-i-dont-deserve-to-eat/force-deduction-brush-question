#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<vector>
#include<algorithm>
using namespace std;

int main() {
    int n;
    cin >> n;
    vector<int> v(n + 1);
    for (int i = 1; i <= n; i++)v[i] = i;
    int pos = 2;
    while (pos < n) {
        if (v[pos] > v[pos - 1] && v[pos + 1] > v[pos]) {
            swap(v[pos], v[pos + 1]);
        }
        pos++;
    }
    for (int i = 1; i <= n; i++)cout << v[i] << " ";
    cout << endl;
    return 0;
}
