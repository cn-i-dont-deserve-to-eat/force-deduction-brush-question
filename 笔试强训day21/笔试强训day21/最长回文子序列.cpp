#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<string>
#include<vector>
using namespace std;

int main() {
    string str;
    cin >> str;
    int n = str.size();
    vector<vector<int>> f(n + 1, vector<int>(n + 1));
    int ans = 1;
    for (int len = 1; len <= n; len++) {
        for (int l = 0; l + len - 1 < n; l++) {
            int r = l + len - 1;
            if (len == 1) {
                f[l][r] = 1;
                continue;
            }
            if (str[l] == str[r]) {
                f[l][r] = f[l + 1][r - 1] + 2;
            }
            else {
                f[l][r] = max(f[l + 1][r], f[l][r - 1]);
            }
            ans = max(ans, f[l][r]);
        }
    }
    cout << ans << endl;


    return 0;
}
