#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<vector>
#include<set>
#include<queue>
using namespace std;
class Cmp {
public:
    bool operator()(const int& a, const int& b) {
        return a < b;
    }
};
int main() {

    set<int> s;
    int n, m;
    cin >> n >> m;
    for (int i = 0; i < n + m; i++) {
        int x;
        cin >> x;
        s.insert(x);
    }

    for (auto& it : s) {
        cout << it << " ";
    }
    return 0;
}
// 64 λ������� printf("%lld")