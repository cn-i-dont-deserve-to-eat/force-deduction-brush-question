#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<unordered_map>
#include<string>
#include<algorithm>
using namespace std;
const int N = 60;
int main() {
    int n;
    cin >> n;
    string str;
    int ans = 0;
    unordered_map<string, int> mp;
    for (int i = 1; i <= n; i++) {
        cin >> str;
        sort(str.begin(), str.end());
        if (!mp.count(str)) {
            ans++;
            mp[str]++;
        }
    }
    cout << ans << endl;
    return 0;
}
