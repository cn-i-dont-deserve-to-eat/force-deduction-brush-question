#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

int main() {
    int t;
    cin >> t;
    while (t--) {
        int h, a, H, A;
        cin >> h >> a >> H >> A;
        if (a >= H) {
            cout << "-1";
            if (t)cout << endl;
            continue;
        }
        int k1 = (H + a - 1) / a;//几个回合杀死小怪
        int c = (k1 - 1) * A;//每杀死一个小怪寇多少血
        cout << (h - 1) / c;
        if (t)cout << endl;
    }
    return 0;
}
// 64 位输出请用 printf("%lld")