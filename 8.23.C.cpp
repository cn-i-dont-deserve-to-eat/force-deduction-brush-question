//#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
#include<vector>
#include<unordered_map>
using namespace std;

int main() {
	int n;
	cin >> n;
    vector<int> num;
	unordered_map<int, vector<int>> mp;
	for (int i = 1; i <= n; i++) {
		int x;
		cin >> x;
		num.push_back(x);
		mp[x].push_back(i);
	}
	int k;
	cin >> k;
	int ans = 0;
	for (auto it : mp) {
		auto y = it.second;
		//int temp
		for (int i = 0, j = 0; i < y.size(); i++) {
			while (j <= i && y[i] - y[j] + 1 - (i - j + 1) > k)j++;
			ans = max(ans, i - j + 1);
		}
	}

	cout << ans;

	return 0;
}