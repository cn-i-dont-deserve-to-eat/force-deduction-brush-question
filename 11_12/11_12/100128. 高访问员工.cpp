#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<string> findHighAccessEmployees(vector<vector<string>>& access_times) {
        map<string, vector<int>> mp;
        int len = access_times.size();

        for (int i = 0; i < len; i++) {
            string name = access_times[i][0];
            string time = access_times[i][1];
            int t = 0;

            t += (time[2] - '0') * 10 + (time[3] - '0');
            t += ((time[0] - '0') * 10 + (time[1] - '0')) * 60;
            mp[name].push_back(t);
            //cout<<mp[name][t]<<" "<<t<<endl;
        }

        for (auto& it : mp) {
            string name = it.first;
            sort(mp[name].begin(), mp[name].end());
            // for(int i=0;i<it.second.size();i++){
            //     cout<<mp[name][i]<<endl;
            // }
        }

        vector<string> ans;
        for (auto it : mp) {
            string name = it.first;
            int n = it.second.size();
            //  int cnt=0;
            vector<int> temp = it.second;
            for (int i = 0; i < n; i++) {
                if (i + 1 < n && i + 2 < n && temp[i + 1] - temp[i] + temp[i + 2] - temp[i + 1] < 60) {
                    ans.push_back(name);
                    break;
                }
            }
            // if(cnt>=3)
        }
        return ans;
    }
};