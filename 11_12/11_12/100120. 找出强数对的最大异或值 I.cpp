#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int maximumStrongPairXor(vector<int>& nums) {
        int len = nums.size();
        int ans = 0;
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len; j++) {
                if (abs(nums[i] - nums[j]) <= min(nums[i], nums[j])) {
                    ans = max(ans, nums[i] ^ nums[j]);
                }

            }
        }
        return ans;
    }
};