#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool isprim(int x) {
        if ((x % 4 == 0 && x % 100 != 0) || (x % 400 == 0)) {
            return true;
        }
        return false;
    }
    string dayOfTheWeek(int day, int month, int year) {
        int mon[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        int s = 0;
        for (int i = 1971; i <= year - 1; i++) {
            if (isprim(i))s++;
            s += 365;
        }
        for (int i = 1; i <= month - 1; i++) {
            s += mon[i - 1];
            if (i == 2 && isprim(year)) {
                s++;
            }
        }
        s += day;
        s += 4;
        s = s % 7;
        string a[7] = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
        return a[s];
    }
};