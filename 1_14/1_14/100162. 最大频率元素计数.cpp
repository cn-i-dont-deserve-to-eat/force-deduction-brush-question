#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int maxFrequencyElements(vector<int>& nums) {
        int maxcnt = 0;
        unordered_map<int, int> mp;
        for (int i = 0; i < nums.size(); i++) {
            mp[nums[i]]++;
            maxcnt = max(maxcnt, mp[nums[i]]);
        }
        int ans = 0;
        for (auto it : mp) {
            if (it.second == maxcnt)ans += maxcnt;
        }
        return ans;
    }
};