#define _CRT_SECURE_NO_WARNINGS 1
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode* deleteDuplicates(struct ListNode* head) {
    if (!head || head->next == NULL)return head;
    struct ListNode* pre = head;
    struct ListNode* cur = head->next;
    while (cur) {
        if (cur->val == pre->val) {
            struct ListNode* temp = cur->next;
            free(cur);
            pre->next = temp;
            cur = temp;
        }
        else {
            cur = cur->next;
            pre = pre->next;
        }
    }
    return head;
}