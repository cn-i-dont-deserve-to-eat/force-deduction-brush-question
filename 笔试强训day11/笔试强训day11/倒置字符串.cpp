#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;

int main() {
    string str;
    getline(cin, str);
    string ans;
    int n = str.size();
    reverse(str.begin(), str.end());
    for (int i = 1, j = 0; i < n; i++) {
        if ((str[i] == ' ' && str[i - 1] != ' ') || i == n - 1) {
            string t = "";
            if (i != n - 1) {
                t = str.substr(j, i - j);
            }
            else {
                t = str.substr(j, i - j + 1);
            }
            reverse(t.begin(), t.end());
            ans += t;
            ans += ' ';
            j = i + 1;


        }
    }
    cout << ans << endl;
    return 0;
}
