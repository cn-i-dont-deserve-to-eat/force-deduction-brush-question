#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
typedef long long LL;

int main() {
    int n, m, a, b;
    cin >> n >> m >> a >> b;
    int k1 = min(n / 2, m);
    LL ans = 0;
    for (int i = 0; i <= k1; i++) {
        int nn = n - i * 2;
        int mm = m - i;
        int k2 = min(nn, mm / 2);
        ans = max(ans, (LL)a * i + b * k2);
    }
    cout << ans << endl;
    return 0;
}
