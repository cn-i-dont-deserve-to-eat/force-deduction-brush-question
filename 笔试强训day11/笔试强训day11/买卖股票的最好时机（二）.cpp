#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
const int N = 1e5 + 10;
int dp[N][2];
int a[N];
int main() {
    int n;
    cin >> n;
    for (int i = 1; i <= n; i++)cin >> a[i];
    dp[1][0] = 0;
    dp[1][1] = -a[1];
    for (int i = 2; i <= n; i++) {
        dp[i][0] = max(dp[i - 1][1] + a[i], dp[i - 1][0]);
        dp[i][1] = max(dp[i - 1][0] - a[i], dp[i - 1][1]);
    }

    cout << dp[n][0];
    return 0;
}

//
#include <iostream>
using namespace std;

int main() {
    int n;
    cin >> n;
    int x;
    int a1 = 0;
    int a2 = 0;
    int b1 = 0;
    int b2 = 0;
    cin >> b1;
    b1 = -b1;//初始化第一天
    for (int i = 2; i <= n; i++) {
        cin >> x;
        a2 = max(b1 + x, a1);
        b2 = max(a1 - x, b1);
        a1 = a2;//迭代
        b1 = b2;
    }
    cout << a2 << endl;
    return 0;
}