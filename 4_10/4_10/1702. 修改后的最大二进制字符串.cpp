#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    string maximumBinaryString(string binary) {
        int n = binary.size();
        int pos = -1;
        int cnt = 0;
        for (int i = 0; i < n; i++) {
            if (binary[i] == '0') {
                cnt++;
                if (pos == -1)pos = i;
            }
        }
        if (!cnt)return binary;
        string ans = "";
        for (int i = 0; i < pos; i++)ans += '1';
        for (int i = 1; i < cnt; i++)ans += '1';
        ans += '0';
        for (int i = pos + cnt; i < n; i++)ans += '1';
        return ans;
    }
};