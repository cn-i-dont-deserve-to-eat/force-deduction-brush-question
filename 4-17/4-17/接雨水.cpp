#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int trap(vector<int>& height) {
        int n = height.size();
        vector<int> lm(n);
        vector<int> rm(n);
        lm[0] = height[0];
        for (int i = 1; i < n; i++) {
            lm[i] = max(height[i], lm[i - 1]);
        }
        rm[n - 1] = height[n - 1];
        for (int i = n - 2; i >= 0; i--) {
            rm[i] = max(height[i], rm[i + 1]);
        }

        int ans = 0;
        for (int i = 0; i < n; i++) {
            ans += min(lm[i], rm[i]) - height[i];
        }
        return ans;
    }
};

//������ ջ
typedef pair<int, int> PII;
class Solution {
public:
    int trap(vector<int>& height) {
        int n = height.size();
        stack<int> st;
        st.push(0);
        int ans = 0;
        for (int i = 1; i < n; i++) {
            if (st.empty() || height[i] < height[st.top()]) {
                st.push(i);
                continue;
            }
            // if(height[i]==height[st.top()]){
            //     st.pop();
            //     st.push(i);
            //     continue;
            // }
            while (!st.empty() && height[i] > height[st.top()]) {
                int it1 = st.top();
                st.pop();
                if (st.empty())break;

                int it2 = st.top();
                int w = (i - it2 - 1);
                int h = min(height[it2], height[i]) - height[it1];
                //cout<<w*h<<" ";
                ans += w * h;
            }
            st.push(i);

        }
        return ans;
    }
};