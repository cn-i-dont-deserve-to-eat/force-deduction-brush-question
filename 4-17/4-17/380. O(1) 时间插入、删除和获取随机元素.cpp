#define _CRT_SECURE_NO_WARNINGS 1
class RandomizedSet {
public:
    RandomizedSet() {

    }

    bool insert(int val) {
        if (!mp.count(val)) {
            num.push_back(val);
            mp[val] = num.size() - 1;
            return true;
        }
        return false;
    }

    bool remove(int val) {
        if (mp.count(val)) {
            int index = mp[val];
            int last = num.size() - 1;
            mp[num[last]] = index;
            swap(num[index], num[last]);
            mp.erase(val);
            num.pop_back();
            return true;
        }
        return false;
    }

    int getRandom() {
        int x = rand() % num.size();
        // pos=(pos+1)%num.size();
        return num[x];

    }

    unordered_map<int, int> mp;
    vector<int> num;
    int pos;
};

/**
 * Your RandomizedSet object will be instantiated and called as such:
 * RandomizedSet* obj = new RandomizedSet();
 * bool param_1 = obj->insert(val);
 * bool param_2 = obj->remove(val);
 * int param_3 = obj->getRandom();
 */