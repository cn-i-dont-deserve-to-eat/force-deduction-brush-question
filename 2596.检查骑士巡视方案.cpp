

public:
    //分别给出行和列的偏移量，代表八个方向
    int dx[8]={-1,-2,-2,-1,1,2,2,1};
    int dy[8]={-2,-1,1,2,2,1,-1,-2};
    bool dfs(int x,int y,vector<vector<int>>& grid,int u,int n){// u表示当前坐标的grid值，n表示目标值
        if(u==n){//达到目标就返回true
            return true;
        }
       for(int i=0;i<8;i++){//遍历八个方向
           int a=dx[i]+x;
           int b=y+dy[i];
           if(a>=0&&a<grid.size()&&b>=0&&b<grid.size()&&grid[a][b]==u+1){//a,b这个点可以走，且符合题意
               if(dfs(a,b,grid,u+1,n)){
                   return true;
               }else{
                   return false;
               }
           }
       }
       return false;
    }
    bool checkValidGrid(vector<vector<int>>& grid) {
        if(grid[0][0]!=0)return false;
        int sz=grid.size();
        for(int i=0;i<sz;i++){
            for(int j=0;j<sz;j++){
                if(grid[i][j]==0){
                     if(dfs(i,j,grid,0,sz*sz-1)){//找到出发点
                      return true;
                    }else{
                        return false;
                    }
                }
            }
        }
       
       
        return false;
    }
