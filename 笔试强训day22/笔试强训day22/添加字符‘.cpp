#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<vector>
#include<string>
using namespace std;

int main() {
    string a;
    string b;
    cin >> a >> b;
    int n1 = a.size();
    int n2 = b.size();

    int ans = 0;
    for (int i = 1; i + n1 - 1 <= n2; i++) {
        int pos = i;
        int cnt = 0;
        for (int j = 1; j <= n1; j++, pos++) {
            if (a[j - 1] == b[pos - 1]) {
                cnt++;
            }
        }
        ans = max(ans, cnt);
    }
    cout << n2 - (n2 - n1 + ans) << endl;
    return 0;
}

