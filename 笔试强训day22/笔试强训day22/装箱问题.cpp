#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<vector>
using namespace std;
int main() {
    int v;
    int n;
    cin >> v >> n;
    vector<int> a(n + 1);
    vector<vector<int>> f(n + 1, vector<int>(v + 1));
    for (int i = 1; i <= n; i++)cin >> a[i];

    for (int i = 1; i <= n; i++) {
        for (int j = 0; j <= v; j++) {
            if (j >= a[i])f[i][j] = max(f[i - 1][j - a[i]] + a[i], f[i - 1][j]);
            else f[i][j] = f[i - 1][j];
        }
    }
    cout << v - f[n][v];
    return 0;
}
