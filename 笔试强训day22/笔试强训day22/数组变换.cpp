#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<vector>
#include<algorithm>
using namespace std;

typedef long long LL;

bool isfun(LL x, LL y) {
    if (x == y)return true;
    int p = 2;
    while (x < y) {
        x = x * p;
    }
    if (x == y)return true;
    return false;
}
int main() {
    int n;
    cin >> n;
    vector<int> a(n);
    for (int i = 0; i < n; i++) {
        cin >> a[i];
    }

    sort(a.begin(), a.end());
    int f = 0;
    for (int i = 1; i < n; i++) {
        if (!isfun(a[i - 1], a[i])) {
            f = 1;
            break;
        }
    }
    if (f)cout << "NO" << endl;
    else cout << "YES" << endl;
    return 0;
}
