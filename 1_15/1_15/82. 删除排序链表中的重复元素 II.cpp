#define _CRT_SECURE_NO_WARNINGS 1
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode* deleteDuplicates(struct ListNode* head) {

    if (!head || head->next == NULL)return head;
    struct ListNode* hh = malloc(sizeof(struct ListNode));
    hh->next = head;
    hh->val = -1;
    struct ListNode* cur = head;
    struct ListNode* pre = hh;
    while (cur && cur->next) {
        if (cur->val == cur->next->val) {
            int x = cur->next->val;
            while (cur && cur->val == x) {
                struct ListNode* temp = cur->next;
                free(cur);
                cur = temp;
                pre->next = temp;
            }
        }
        else {
            pre = cur;
            cur = cur->next;
        }
    }
    return hh->next;
}