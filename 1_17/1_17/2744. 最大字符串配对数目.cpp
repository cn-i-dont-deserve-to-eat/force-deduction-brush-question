#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int maximumNumberOfStringPairs(vector<string>& words) {
        int len = words.size();
        vector<vector<int>>mp(26, vector<int>(26));
        int ans = 0;
        for (int i = 0; i < len; i++) {
            int x = words[i][0] - 'a';
            int y = words[i][1] - 'a';
            if (mp[y][x]) {
                ans++;
                mp[y][x]--;
            }
            else {
                mp[x][y]++;
            }
        }

        return ans;

    }
};