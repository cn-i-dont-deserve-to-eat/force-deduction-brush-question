#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    void dfs(int n, vector<bool>& st, vector<vector<int>>& g) {
        if (st[n] == true)return;
        st[n] = true;
        for (auto it : g[n]) {
            if (!st[it]) {
                dfs(it, st, g);
            }
        }
    }
    bool canVisitAllRooms(vector<vector<int>>& rooms) {
        int n = rooms.size();
        vector<vector<int>> g(n);
        vector<bool> st(n, false);
        for (int i = 0; i < n; i++) {
            for (auto it : rooms[i]) {
                g[i].push_back(it);
            }
        }

        dfs(0, st, g);
        for (int i = 0; i < n; i++) {
            if (!st[i])return false;
        }
        return true;
    }
};