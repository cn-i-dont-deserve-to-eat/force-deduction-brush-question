#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:

    int minmumNumberOfHost(int n, vector<vector<int> >& startEnd) {
        vector<int>st(n);
        vector<int> en(n);
        for (int i = 0; i < n; i++) {
            st[i] = startEnd[i][0];
            en[i] = startEnd[i][1];
        }

        sort(st.begin(), st.end());
        sort(en.begin(), en.end());
        int ans = 0;
        for (int i = 0, j = 0; i < n; i++) {
            if (st[i] >= en[j])j++;
            else ans++;
        }
        return ans;
    }
};

class Solution {
public:

    int minmumNumberOfHost(int n, vector<vector<int> >& startEnd) {
        priority_queue<int, vector<int>, greater<int>> q;

        sort(startEnd.begin(), startEnd.end());

        q.push(startEnd[0][1]);
        int ans = 1;
        for (int i = 1; i < n; i++) {
            if (startEnd[i][0] >= q.top()) {
                q.pop();
                q.push(startEnd[i][1]);
            }
            else {
                q.push(startEnd[i][1]);
                ans++;
            }
        }
        return ans;
    }
};