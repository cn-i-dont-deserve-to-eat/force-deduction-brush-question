#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
typedef long long LL;
const int p = 109;
LL kuimi(LL a, LL b) {
    LL res = 1;
    while (b) {
        if (b & 1)res = res * a % p;
        b >>= 1;
        a = a * a % p;
    }
    return res;
}
int main() {
    int n, m;
    cin >> n >> m;
    cout << kuimi(n - 1, m - 1) * n % p << endl;
    return 0;
}
