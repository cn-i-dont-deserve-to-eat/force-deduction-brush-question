#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<queue>
using namespace std;
typedef pair<int, int> PII;
const int N = 1100;
char g[N][N];
bool st[N][N];
int dx[] = { 0,0,1,-1 };
int dy[] = { -1,1,0,0 };
int n, m;

int bfs(int sx, int sy, int ex, int ey) {
    queue<PII> q;
    q.push({ sx,sy });
    int res = 0;

    while (!q.empty()) {
        int sz = q.size();
        for (int i = 0; i < sz; i++) {
            auto it = q.front();
            q.pop();

            int x = it.first;
            int y = it.second;
            if (x == ex && y == ey)return res;
            st[x][y] = true;
            for (int j = 0; j < 4; j++) {
                int a = dx[j] + x;
                int b = dy[j] + y;

                if (a >= 0 && a < n && b >= 0 && b < m && !st[a][b] && g[a][b] == '.') {
                    q.push({ a,b });
                    st[a][b] = true;
                }
            }
        }
        res++;
    }
    return -1;
}

int main() {
    cin >> n >> m;
    int sx, sy, ex, ey;
    cin >> sx >> sy >> ex >> ey;
    getchar();
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cin >> g[i][j];
        }
    }
    cout << bfs(sx - 1, sy - 1, ex - 1, ey - 1) << endl;
    return 0;
}
