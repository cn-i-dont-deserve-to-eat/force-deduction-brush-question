#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int minLength(string s) {
        stack<char> st;
        int res = 0;
        int n = s.size();
        for (int i = 0; i < n; i++) {
            if (s[i] == 'A' || s[i] == 'C') {
                st.push(s[i]);
            }
            else if (s[i] == 'B') {
                if (!st.empty() && st.top() == 'A') {
                    st.pop();
                    res += 2;
                }
                else {
                    while (!st.empty()) {
                        st.pop();
                    }
                }
            }
            else if (s[i] == 'D') {
                if (!st.empty() && st.top() == 'C') {
                    st.pop();
                    res += 2;
                }
                else {
                    while (!st.empty()) {
                        st.pop();
                    }
                }
            }
            else {
                while (!st.empty()) {
                    st.pop();
                }
            }
        }
        return n - res;
    }
};