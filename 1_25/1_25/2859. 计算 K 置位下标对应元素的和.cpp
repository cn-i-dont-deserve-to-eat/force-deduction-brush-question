#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int sumIndicesWithKSetBits(vector<int>& nums, int k) {
        if (k == 0)return nums[0];
        int ans = 0;
        int n = nums.size();
        for (int i = 1; i < n; i++) {
            int res = 0;
            int x = i;
            while (x) {
                if (x % 2)res++;
                x /= 2;
            }
            if (res == k)ans += nums[i];
        }
        return ans;
    }
};