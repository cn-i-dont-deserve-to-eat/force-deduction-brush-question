#define _CRT_SECURE_NO_WARNINGS 1
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
        if (root == nullptr)return {};
        queue<TreeNode*> q;
        q.push(root);
        vector<vector<int>> ans;
        int k = 0;
        while (!q.empty()) {
            int sz = q.size();
            k++;
            vector<int> res;
            for (int i = 0; i < sz; i++) {
                TreeNode* t = q.front();
                q.pop();

                if (t->left) {
                    q.push(t->left);
                }
                if (t->right) {
                    q.push(t->right);
                }
                res.push_back(t->val);
            }
            if (k % 2 == 0) {
                reverse(res.begin(), res.end());
            }
            ans.push_back(res);
        }
        return ans;

    }
};