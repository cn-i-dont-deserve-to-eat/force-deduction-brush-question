#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
class Solution {
public:
    vector<int> divisibilityArray(string word, int m) {
        int n = word.size();
        long long  res = 0;
        vector<int>ans(n);
        for (int i = 0; i < n; i++) {
            res = (res * 10) % m + (word[i] - '0') % m;
            if (res % m == 0)ans[i] = 1;
            else ans[i] = 0;
            res = res % m;
        }
        return ans;
    }
};