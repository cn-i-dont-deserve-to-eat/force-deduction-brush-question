#define _CRT_SECURE_NO_WARNINGS 1
struct ListNode* reverseList(struct ListNode* head) {
    struct ListNode* cur = head;
    struct ListNode* nowhead = NULL;
    while (cur) {
        struct ListNode* next = cur->next;
        cur->next = nowhead;
        nowhead = cur;
        cur = next;
    }

    return nowhead;


}