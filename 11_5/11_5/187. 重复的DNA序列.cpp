#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<string> findRepeatedDnaSequences(string s) {
        map<string, int> mp;
        vector<string> ans;
        int len = s.size();

        for (int i = 0; i + 10 - 1 < len; i++) {
            int j = i + 10 - 1;
            string t = s.substr(i, 10);
            mp[t]++;
        }
        for (auto it : mp) {
            if (it.second > 1) {
                ans.push_back(it.first);
            }
        }
        return ans;
    }
};