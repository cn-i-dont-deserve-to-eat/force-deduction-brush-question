#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int majorityElement(vector<int>& nums) {
        int len = nums.size();
        map<int, int> mp;
        int ans = 0;
        for (int i = 0; i < len; i++) {
            mp[nums[i]]++;
            if (mp[nums[i]] > len / 2)return nums[i];
        }
        return ans;
    }
};