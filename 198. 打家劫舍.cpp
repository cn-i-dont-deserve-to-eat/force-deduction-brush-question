#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int rob(vector<int>& nums) {
        int len = nums.size();
        int  f[110] = { 0 };
        for (int i = 0; i <= len; i++)f[i] = 0;
        f[0] = nums[0];
        if (len == 1)return f[0];
        f[1] = max(f[0], nums[1]);
        for (int i = 2; i < len; i++) {
            f[i] = max(f[i - 1], f[i - 2] + nums[i]);
        }
        return f[len - 1];
    }
};