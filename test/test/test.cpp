#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <setjmp.h>
#include <stdlib.h>
#include<iostream>
using namespace std;
// 记录递归深度的全局变量
volatile int depth = 0;
jmp_buf env;

// 递归函数，用于占用栈空间
void recurse() {
    int stack_var[1024]; // 每次递归占用1KB栈空间
    depth++;
    if (depth % 100 == 0) {
        printf("Current depth: %d\n", depth);
    }
    recurse();
}

//int main() {
//    printf("Testing stack size...\n");
//
//    // 尝试调用递归函数，捕获栈溢出
//    if (setjmp(env) == 0) {
//        recurse();
//    }
//    else {
//        printf("Stack overflow detected at depth: %d\n", depth);
//        printf("Estimated stack size: %d KB\n", depth);
//    }
//
//    return 0;
//}
//

class A {
    virtual void fun() {

    }
};

class B : virtual A 
{
    int a;
};

int main() {

    cout << sizeof A << endl;
    cout << sizeof B;
    return 0;
}