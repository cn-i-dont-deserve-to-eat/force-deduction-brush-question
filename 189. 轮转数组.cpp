#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        int len = nums.size();
        vector<int> ans(2 * len + 10);
        for (int i = 0; i < len; i++) {
            ans[i] = nums[i];
        }
        for (int i = len; i < 2 * len; i++) {
            ans[i] = nums[i - len];
        }

        int l = len, r = 2 * len - 1;
        k = k % len;
        while (k) {
            l--;
            r--;
            k--;
        }
        for (int i = l, j = 0; i <= r; i++) {
            nums[j++] = ans[i];
        }
    }
};