#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    long long minIncrementOperations(vector<int>& nums, int k) {
        int len = nums.size();
        long long dp[1000000];
        dp[0] = max(k - nums[0], 0);
        dp[1] = max(k - nums[1], 0);
        dp[2] = max(k - nums[2], 0);
        for (int i = 3; i < len; i++) {
            dp[i] = max(0, k - nums[i]) + min(min(dp[i - 1], dp[i - 2]), dp[i - 3]);
        }
        return min({ dp[len - 1],dp[len - 2],dp[len - 3] });
    }
};  