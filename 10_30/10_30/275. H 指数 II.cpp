#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int hIndex(vector<int>& citations) {
        sort(citations.begin(), citations.end());
        int len = citations.size();
        int max_h = 0;
        int t = -1;
        for (int i = 0; i < len; i++) {
            if (citations[i] != t) {
                int num = len - i;
                if (num >= citations[i]) {
                    max_h = max(max_h, citations[i]);
                }
                else {
                    max_h = max(max_h, num);
                }
                t = citations[i];
            }
        }
        return max_h;
    }
};