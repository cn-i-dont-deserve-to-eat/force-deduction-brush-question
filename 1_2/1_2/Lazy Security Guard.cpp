#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
#include<math.h>

using namespace std;

int main() {
	int n;
	cin >> n;
	int d = sqrt(n);
	int ans = d * 4;
	int m = n - d * d;
	int k = (m + d - 1) / d;
	ans += k * 2;
	cout << ans << endl;
	return 0;
}