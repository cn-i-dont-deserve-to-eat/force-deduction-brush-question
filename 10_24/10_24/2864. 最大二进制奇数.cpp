#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    string maximumOddBinaryNumber(string s) {
        int num_1 = 0;
        int len = s.size();
        for (int i = 0; i < len; i++) {
            if (s[i] == '1')num_1++;
        }
        string ans = "";
        num_1--;
        for (int i = 0; i < len - 1; i++) {
            if (num_1) {
                ans += '1';
                num_1--;
            }
            else {
                ans += '0';
            }
        }
        ans += '1';
        return ans;
    }
};