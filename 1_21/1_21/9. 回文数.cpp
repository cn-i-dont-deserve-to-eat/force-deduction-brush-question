#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool isPalindrome(int x) {
        if (x < 0 || (x % 10 == 0 && x != 0))return false;
        long long ans = 0;
        long long k = x;
        while (x) {
            ans = ans * 10 + x % 10;
            x /= 10;
        }
        return k == ans;
    }
};