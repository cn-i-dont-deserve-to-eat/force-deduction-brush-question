#define _CRT_SECURE_NO_WARNINGS 1

class Solution {
public:
    vector<int> canSeePersonsCount(vector<int>& heights) {
        int len = heights.size();
        vector<int> ans(len);
        vector<int> st;

        if (len == 1)return ans;
        st.push_back(heights.back());
        for (int i = len - 2; i >= 0; i--) {
            // ans[i]=st.size();
             //ans[i]++;
            while (!st.empty() && heights[i] > st.back()) {
                st.pop_back();
                ans[i]++;
            }
            if (!st.empty())ans[i]++;
            st.push_back(heights[i]);
            // ans[i]=st.size();

        }
        return ans;
    }
};