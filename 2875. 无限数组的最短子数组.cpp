#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int minSizeSubarray(vector<int>& nums, int target) {
        int len = nums.size();
        for (int i = 0; i < len; i++) {
            nums.push_back(nums[i]);
        }
        vector<int> num(2 * len + 10);
        for (int i = 0; i < nums.size(); i++) {
            num[i + 1] = nums[i];
        }
        int cnt = 0;
        vector<long long> s(2 * len + 10);
        s[0] = 0;
        for (int i = 1; i <= 2 * len; i++) {
            s[i] = (long long)num[i] + s[i - 1];
        }
        int k = target / s[len];
        int ans = 2e9;
        int p = target % s[len];
        if (p == 0)return k * len;
        for (int i = 1, j = 1; i <= 2 * len; i++) {
            while (s[i] - s[j - 1] > p && j <= i) {
                j++;
            }
            if (s[i] - s[j - 1] == p) {
                ans = min(ans, i - j + 1);
            }
        }
        // cout<<ans<<" "<<k<<endl;
        if (ans > len)return -1;
        return ans + k * len;
    }
};