#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    unordered_map<TreeNode*, int> f, g;

    void dfs(TreeNode* node) {
        if (!node) {
            return;
        }

        dfs(node->left);
        dfs(node->right);
        f[node] = node->val + g[node->left] + g[node->right];
        g[node] = max(f[node->left], g[node->left]) + max(f[node->right], g[node->right]);
    }
    int rob(TreeNode* root) {
        dfs(root);

        return max(g[root], f[root]);
    }
};