#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int findPeakElement(vector<int>& nums) {
        int len = nums.size();
        int l = 0, r = len - 1;
        while (l < r) {
            int mid = (l + r) >> 1;
            if (r - l + 1 >= 3 && nums[mid] > nums[mid - 1] && nums[mid] > nums[mid + 1])return mid;
            if (nums[mid] < nums[mid + 1]) {
                l = mid + 1;
            }
            else {
                r = mid;
            }
        }
        return r;
    }
};