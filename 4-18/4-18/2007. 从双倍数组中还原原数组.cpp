#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<int> findOriginalArray(vector<int>& changed) {
        unordered_map<int, int> mp;
        if (changed.size() % 2)return {};
        for (int i = 0; i < changed.size(); i++) {
            mp[changed[i]]++;
        }
        sort(changed.begin(), changed.end());
        int t = 0;
        vector<int> nums;
        for (int i = 0; i < changed.size(); i++) {
            if (changed[i] == 0) {
                if (mp[0] < 2)continue;
            }
            if (mp[changed[i]] >= 1 && mp[changed[i] * 2] >= 1) {
                t++;
                mp[changed[i]]--;
                mp[changed[i] * 2]--;
                nums.push_back(changed[i]);
            }
        }
        if (t >= changed.size() / 2)return nums;
        return vector<int>();
    }
};