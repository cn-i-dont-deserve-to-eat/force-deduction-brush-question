#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int candy(vector<int>& ratings) {
        int n = ratings.size();
        vector<int> s(n, 1);
        int ans = 0;
        for (int i = 1; i < n; i++) {
            if (ratings[i] > ratings[i - 1]) {
                s[i] = s[i - 1] + 1;
                //   cout<<i<<" "<<s[i]<<endl;
            }
        }

        for (int i = n - 2; i >= 0; i--) {
            if (ratings[i] > ratings[i + 1]) {
                s[i] = max(s[i], s[i + 1] + 1);
            }
        }

        for (int i = 0; i < n; i++) {
            ans += s[i];
        }
        return ans;
    }
};