#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
        int n = gas.size();
        int g = 0;
        //  int ans=-1;
        for (int i = 0, j = 0; i < n; i++) {
            j = i;
            g = gas[i];
            // cout<<i<<"--"<<j<<endl;
            while (g >= cost[j]) {
                g -= cost[j];
                j = (j + 1) % n;
                if (j == i) {
                    return i;
                }
                g += gas[j];
            }
            if (j < i)break;

            i = j;
        }
        return -1;

    }
};