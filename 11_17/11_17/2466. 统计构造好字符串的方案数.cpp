#define _CRT_SECURE_NO_WARNINGS 1
const int mod = 1e9 + 7;
class Solution {
public:
    int countGoodStrings(int low, int high, int zero, int one) {
        vector<int> dp(high + 1);
        int ans = 0;
        dp[0] = 1;
        for (int i = 1; i <= high; i++) {
            if (i >= zero && i >= one) {
                dp[i] = (dp[i - zero] + dp[i - one]) % mod;
            }
            else if (i >= zero) {
                dp[i] = dp[i - zero];
            }
            else if (i >= one) {
                dp[i] = dp[i - one];
            }
            // cout<<dp[i]<<" ";
            if (i >= low)ans = (ans + dp[i]) % mod;
        }
        return ans;
    }
};