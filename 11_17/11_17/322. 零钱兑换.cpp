#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int coinChange(vector<int>& coins, int amount) {
        vector<int> dp(amount + 1);
        sort(coins.begin(), coins.end());
        dp[0] = 0;
        for (int i = 1; i <= amount; i++) {
            int res = 1e9;
            for (int j = 0; j < coins.size(); j++) {
                if (coins[j] > i)break;
                res = min(res, dp[i - coins[j]]);
            }
            dp[i] = res + 1;
            // cout<<dp[i]<<" ";
        }
        if (dp[amount] >= 1e9)return -1;
        return dp[amount];
    }
};