#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int maxScore(vector<int>& nums) {
        int len = nums.size();
        // int mxa=-1e9;
        vector<int> res;
        long long s = 0;
        for (int i = 0; i < len; i++) {
            if (nums[i] < 0)res.push_back(-nums[i]);
            else s += nums[i];
        }
        int len2 = res.size();
        int ans = len - len2;
        if (s == 0)return 0;
        sort(res.begin(), res.end());
        for (int i = 0; i < len2; i++) {
            s -= res[i];
            if (s <= 0) {
                break;
            }
            else {
                ans++;
            }
        }
        return ans;
    }
};