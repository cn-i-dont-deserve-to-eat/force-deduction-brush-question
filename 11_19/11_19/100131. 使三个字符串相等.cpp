#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int findMinimumOperations(string s1, string s2, string s3) {
        int n1 = s1.size();
        int n2 = s2.size();
        int n3 = s3.size();
        int n = min({ n1,n2,n3 });
        int ans = 0;
        for (int i = 0; i < n; i++) {
            if (s1[i] == s2[i] && s1[i] == s3[i]) {
                ans++;
            }
            else {
                break;
            }
        }
        if (ans == 0)return -1;
        return n1 + n2 + n3 - ans * 3;
    }
};