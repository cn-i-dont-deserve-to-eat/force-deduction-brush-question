#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    long long minimumSteps(string s) {
        long long cnt = 0;
        long long sum = 0;
        int len = s.size();
        for (int i = 0; i < len; i++) {
            if (s[i] == '1') {
                cnt++;
                sum += i;
            }
        }
        long long res = (len - 1) * cnt - cnt * (cnt - 1) / 2;
        return res - sum;

    }
};