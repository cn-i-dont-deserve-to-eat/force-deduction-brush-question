#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<algorithm>

using namespace std;

int main() {
    long long a, b, c;
    int t;
    cin >> t;
    while (t--) {
        cin >> a >> b >> c;
        long long ans = 0;
        int k = min(min(a, b), c);
        ans = k * 2;
        ans = ans + max((long long)b - k - 1, (long long)0);
        cout << ans << endl;
    }

    return 0;
}
