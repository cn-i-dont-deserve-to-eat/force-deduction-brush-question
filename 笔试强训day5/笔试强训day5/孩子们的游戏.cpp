#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:

    int LastRemaining_Solution(int n, int m) {
        queue<int> q;
        for (int i = 0; i < n; i++)q.push(i);
        int cnt = 0;
        while (q.size() > 1) {
            int t = q.front();
            q.pop();
            if (cnt != m - 1) {
                q.push(t);
            }
            // cout<<t<<" "<<cnt<<endl;

            cnt = (cnt + 1) % m;
        }

        return q.front();
    }
};


class Solution {
public:
    int f(int n, int m) {
        if (n == 1)return 0;
        return (f(n - 1, m) + m) % n;
    }
    int LastRemaining_Solution(int n, int m) {

        return f(n, m);
    }
};