#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int chartoi(char u) { return u - '0'; }
    int calculate(string s) {
        string str = "";
        int n = s.size();
        for (int i = 0; i < n; i++) {
            if (s[i] != ' ')
                str += s[i];
        }
        // cout<<str<<endl;
        n = str.size();
        int ans = 0;
        stack<int> st;
        char pre = '+';
        int t = 0;
        for (int i = 0; i < n; i++) {
            if (str[i] >= '0' && str[i] <= '9') {
                t = t * 10 + (str[i] - '0');
            }
            else {

                if (st.empty()) {
                    st.push(t);
                    t = 0;
                    pre = str[i];
                    continue;
                }
                if (pre == '+') {
                    st.push(t);
                }
                else if (pre == '-') {
                    st.push(-1 * t);
                }
                else if (pre == '*') {
                    int res = st.top();
                    st.pop();
                    st.push(res * t);
                }
                else {
                    int res = st.top();
                    st.pop();
                    st.push(res / t);
                }

                t = 0;
                pre = str[i];
            }
        }

        if (pre == '+') {
            st.push(t);
        }
        else if (pre == '-') {
            st.push(-1 * t);
        }
        else if (pre == '*') {
            int res = st.top();
            st.pop();
            st.push(res * t);
        }
        else {
            int res = st.top();
            st.pop();
            st.push(res / t);
        }

        while (!st.empty()) {
            int res = st.top();
            st.pop();
            cout << res << endl;
            ans += res;
        }
        return ans;
    }
};