#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool areSimilar(vector<vector<int>>& mat, int k) {
        int m = mat.size();
        int n = mat[0].size();
        k = k % n;
        if (k % n == 0)return true;
        for (auto it : mat) {
            for (int j = 0; j < n; j++) {
                if (it[j] != it[(j + k) % n]) {
                    return false;
                }
            }
        }
        return true;
    }
};