#define _CRT_SECURE_NO_WARNINGS 1
const int N = 1e5 + 10;
vector<int> prim;
vector<bool> st(N+10, false);
void get_prim() {
    if (!prim.empty()) return;
    st[0] = st[1] = true;

    st[1] = true;
    for (int i = 2; i <= N; i++) {
        if (!st[i])prim.push_back(i);
        for (int j = 0; i <= N / prim[j]; j++) {
            st[prim[j] * i] = true;
            if (i % prim[j] == 0) {
                break;
            }
        }
    }
    return;
}


class Solution {
public:
    int finds(int x, vector<int>& p) {
        if (x != p[x])p[x] = finds(p[x], p);
        return p[x];
    }

    void fun(int a, int b, vector<int>& p, vector<int>& nums) {
        int x = finds(a, p);
        int y = finds(b, p);

        if (x == y)return;
        if (nums[x] > nums[y]) swap(x, y);
        nums[y] += nums[x];
        p[x] = y;

        return;
    }

    long long countPaths(int n, vector<vector<int>>& edges) {

        get_prim();
        st[1] = true;
        vector<int> p(n + 10);
        vector<int> nums(n + 10, 1);
        map<int, vector<int>> mp;
        for (int i = 0; i <= n; i++)p[i] = i, nums[i] = 1;
        int cnt = 0;
        for (int i = 1; i <= 100000; i++) {
            if (!st[i])cnt++;
        }
        cout << cnt << endl;
        for (auto it : edges) {
            // cout<<it[0]<<" "<<it[1]<<" "<<st[it[0]]<<" "<<st[it[1]]<<endl;
            if (!st[it[0]]) {
                if (st[it[1]]) {
                    mp[it[0]].push_back(it[1]);
                }
                else {
                    continue;
                }
            }
            else {
                if (!st[it[1]]) {
                    mp[it[1]].push_back(it[0]);
                }
                else {
                    fun(it[0], it[1], p, nums);
                }
            }
        }
        // for(int i=1;i<=n;i++){
        //     cout<<p[i]<<" "<<nums[i]<<endl;
        // }
        long long ans = 0;
        long long sum1 = 0;
        long long sum2 = 0;
        for (auto it : mp) {
            sum1 = sum2 = 0;
            for (auto son : it.second) {
                son = p[son];
                // cout<<nums[son]<<endl;
                sum1 += (long long)nums[son];
                sum2 += (long long)nums[son] * nums[son];
            }
            cout << sum1 << " " << sum2 << endl;
            ans += sum1 + (sum1 * sum1 - sum2) / 2;
        }
        return ans;
    }
};
//const int M_2867 = 100001;
//const int N = 1e5 + 10;
//// 线性筛找所有质数
//vector<int> primers_2867; // 求出[1, 100000]所有的质数
//vector<bool> isPrimer_2867 = vector<bool>(M_2867, true);
//vector<int> prim;
//vector<bool> st(N, false);
//
//bool isfun(int x) {
//    for (int i = 2; i <= x / i; i++) {
//        if (x % i == 0)return false;
//    }
//    return true;
//}
//
//void get_prim() {
//    if (!prim.empty()) return;
//    st[0] = st[1] = true;
//
//    st[1] = true;
//    for (int i = 2; i < N; i++) {
//        if (!st[i])prim.push_back(i);
//        for (int j = 0; j < prim.size() && i < N / prim[j]; j++) {
//            st[prim[j] * i] = true;
//            if (i % prim[j] == 0) {
//                break;
//            }
//        }
//    }
//    return;
//}
//void initPrimer_2867() {
//    if (!primers_2867.empty()) return;
//    isPrimer_2867[0] = isPrimer_2867[1] = false;
//    for (int i = 2; i < M_2867; ++i) {
//        if (isPrimer_2867[i]) {
//            primers_2867.emplace_back(i);
//        }
//        for (int j = 0; j < primers_2867.size() && i * primers_2867[j] < M_2867; ++j) {
//            isPrimer_2867[i * primers_2867[j]] = false;
//            if (i % primers_2867[j] == 0) break;
//        }
//    }
//}
//class Solution {
//public:
//    // 并查集实现
//    int findFa_2867(int x, vector<int>& fa) {
//        if (fa[x] < 0) return x; // 父节点为 负，则此时为根节点
//        return fa[x] = findFa_2867(fa[x], fa);
//    }
//    void union_2867(int x, int y, vector<int>& fa, vector<int>& nums) {
//        x = findFa_2867(x, fa);
//        y = findFa_2867(y, fa);
//        if (x == y) return; // 同根则直接返回
//        if (nums[x] > nums[y]) swap(x, y); // 集合大的作根节点
//        fa[x] = y;
//        nums[y] += nums[x];
//    }
//    long long countPaths(int n, vector<vector<int>>& edges) {
//        initPrimer_2867();
//        get_prim();
//        cout << isfun(99893) << " " << isfun(99899) << endl;
//        vector<int> fa(n + 1, -1), nums(n + 1, 1);
//        unordered_map<int, vector<int>> priLnk;
//        for (auto&& edge : edges) {
//            if (isPrimer_2867[edge[0]]) {
//                if (isPrimer_2867[edge[1]]) continue;
//                else priLnk[edge[0]].emplace_back(edge[1]);
//            }
//            else {
//                if (isPrimer_2867[edge[1]]) priLnk[edge[1]].emplace_back(edge[0]);
//                else union_2867(edge[0], edge[1], fa, nums);
//            }
//        }
//        long long cnt = 0, sum1 = 0, sum2 = 0; // 总数， 相邻非质数集合的节点总数， 相邻非质数集合的节点总数 的平方
//        int cnt1 = 0;
//        for (int i = 1; i <= 100000; i++) {
//            if (!st[i] && !isPrimer_2867[i]) {
//                cout << i << " ";
//            }
//
//        }
//        cout << endl;
//        cout << cnt1 << endl;
//        for (auto&& [pri, nodes] : priLnk) {
//            sum1 = sum2 = 0;
//            for (auto&& x : nodes) {
//                x = findFa_2867(x, fa);
//                sum1 += nums[x];
//                sum2 += pow(nums[x], 2);
//            }
//            cout << sum1 << " " << sum2 << endl;
//            cnt += (sum1 + (pow(sum1, 2) - sum2) / 2);
//        }
//        return cnt;
//    }
//};
