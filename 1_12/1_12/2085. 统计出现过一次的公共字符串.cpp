#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int countWords(vector<string>& words1, vector<string>& words2) {
        unordered_map<string, int> mp;
        for (int i = 0; i < words1.size(); i++) {
            mp[words1[i]]++;
        }
        int ans = 0;
        unordered_map<string, int> mp1;
        for (int i = 0; i < words2.size(); i++) {
            mp1[words2[i]]++;
            if (mp[words2[i]] == 1) {
                if (mp1[words2[i]] == 1)ans++;
                if (mp1[words2[i]] == 2)ans--;
            }
        }
        return ans;
    }
};