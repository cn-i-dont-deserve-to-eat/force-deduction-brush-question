#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int find(int x, vector<int>& p) {
        if (p[x] != x) p[x] = find(p[x], p);
        return p[x];
    }
    int findCircleNum(vector<vector<int>>& isConnected) {
        int n = isConnected.size();
        vector<int> p(n + 1);
        for (int i = 0; i <= n; i++) {
            p[i] = i;
        }
        int ans = n;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (!isConnected[i][j])continue;
                int a = i;
                int b = j;
                int x = find(a, p);
                int y = find(b, p);
                if (x != y) {
                    p[x] = y;
                }
            }
        }
        int res = 0;
        for (int i = 0; i < n; i++) {
            if (find(i, p) == i)res++;
        }
        return res;
    }
};