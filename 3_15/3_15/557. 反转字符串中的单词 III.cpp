#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    string reverseWords(string s) {
        int n = s.size();
        for (int i = 0, j = 0; i < n; i++) {
            if (s[i] == ' ' || i == n - 1) {
                if (i == n - 1)reverse(s.begin() + j, s.end());
                else  reverse(s.begin() + j, s.begin() + i);
                j = i + 1;
            }
        }
        return  s;
    }
};