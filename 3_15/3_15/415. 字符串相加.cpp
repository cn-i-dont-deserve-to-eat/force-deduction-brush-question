#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    string addStrings(string num1, string num2) {
        string ans = "";
        int n1 = num1.size();
        int n2 = num2.size();
        reverse(num1.begin(), num1.end());
        reverse(num2.begin(), num2.end());
        int t = 0;
        for (int i = 0; i < n1 || i < n2; i++) {
            if (i < n1)t += (num1[i] - '0');
            if (i < n2)t += (num2[i] - '0');
            ans.push_back(t % 10 + '0');
            t /= 10;
        }
        if (t)ans.push_back(t + '0');
        reverse(ans.begin(), ans.end());
        return ans;
    }
};