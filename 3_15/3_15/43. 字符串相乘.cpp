#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    string multiply(string num1, string num2) {
        vector<int> A;
        vector<int>B;
        if (num1 == "0" || num2 == "0")return "0";
        int n1 = num1.size();
        int n2 = num2.size();
        for (int i = n1 - 1; i >= 0; i--)A.push_back(num1[i] - '0');
        for (int i = n2 - 1; i >= 0; i--)B.push_back(num2[i] - '0');
        string ans;
        //ans.reserve(n1+n2);
       // ans.resize(n1+n2);
        //reverse(A.begin(),A.end());
        //reverse(B.begin(),B.end());
        int t = 0;
        vector<int>C(n1 + n2);
        for (int i = 0; i < n1; i++) {
            for (int j = 0; j < n2; j++) {
                t = (A[i]) * (B[j]);
                C[i + j] += t;
                C[i + j + 1] += C[i + j] / 10;
                C[i + j] %= 10;
            }
            //cout<<t<<"--t--";
            // for(auto it:C){
            //     cout<<it<<" ";
            // }
            // cout<<endl;
        }
        int f = 0;
        for (int i = n1 + n2 - 1; i >= 0; i--) {
            if (C[i] == 0 && f == 0) {
                continue;
            }
            f = 1;
            char u = C[i] + '0';
            ans += u;
        }
        return ans;
    }
};