#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    string reverseStr(string s, int k) {
        int n = s.size();
        int t = 0;
        for (int i = 0; i < n; i++) {
            t++;
            if (t == 2 * k) {
                reverse(s.begin() + i - t + 1, s.begin() + i - k + 1);
                t = 0;
            }
        }
        if (t < k) {
            reverse(s.begin() + n - t, s.end());
        }
        else {
            reverse(s.begin() + n - t, s.begin() + n - t + k);
        }
        return s;
    }
};