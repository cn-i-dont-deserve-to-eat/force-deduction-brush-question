#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    long long sellingWood(int m, int n, vector<vector<int>>& prices) {
        vector<vector<int>> pr(m + 1, vector<int>(n + 1));
        for (auto it : prices) {
            pr[it[0]][it[1]] = it[2];
        }
        vector<vector<long long>> f(m + 1, vector<long long>(n + 1));
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                f[i][j] = pr[i][j];
                for (int k = 1; k <= i; k++) {
                    f[i][j] = max(f[i][j], f[k][j] + f[i - k][j]);
                }
                for (int k = 1; k <= j; k++) {
                    f[i][j] = max(f[i][j], f[i][k] + f[i][j - k]);
                }
            }
        }
        return f[m][n];
    }
};