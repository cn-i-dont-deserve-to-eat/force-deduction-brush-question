#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    void reverseString(vector<char>& s) {
        int n = s.size();
        int l = 0, r = n - 1;
        while (l + 1 <= r) {
            swap(s[l], s[r]);
            l++;
            r--;
        }
    }
}
