#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) {
        int n = nums.size();
        vector<int> l(n + 1);
        vector<int> r(n + 1);
        l[0] = 1;
        r[n] = 1;
        for (int i = 1; i <= n; i++)l[i] = l[i - 1] * nums[i - 1];
        for (int i = n - 1; i >= 0; i--)r[i] = r[i + 1] * nums[i];
        vector<int> ans;
        for (int i = 0; i < n; i++)
        {
            ans.push_back(l[i] * r[i + 1]);
        }
        return ans;
    }
};

class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) {
        int n = nums.size();
        vector<int> ans(n);
        ans[0] = 1;
        for (int i = 1; i < n; i++)ans[i] = ans[i - 1] * nums[i - 1];
        //cout<<ans[n-1]<<endl;
        int r = 1;
        for (int i = n - 1; i >= 0; i--)
        {
            ans[i] = ans[i] * r;
            r = r * nums[i];
        }
        return ans;
    }
};