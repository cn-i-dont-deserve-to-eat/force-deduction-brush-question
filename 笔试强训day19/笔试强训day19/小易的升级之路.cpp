#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
typedef long long LL;
LL gcd(LL a, LL b) {
    return b == 0 ? a : gcd(b, a % b);
}

int main() {
    int n;
    LL x;
    cin >> n >> x;
    LL a;
    for (int i = 0; i < n; i++) {
        cin >> a;
        if (x >= a) {
            x += a;
        }
        else {
            x = x + gcd(x, a);
        }
    }
    cout << x << endl;
}
