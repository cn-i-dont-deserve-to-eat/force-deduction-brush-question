#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<cstring>
using namespace std;
const int N = 110;
int a[N];
int main() {
    int t;
    cin >> t;
    while (t--) {
        memset(a, 0, sizeof a);
        int n;
        cin >> n;
        string str = "";
        for (int i = 0; i < n; i++) {
            cin >> str;
            for (int j = 0; j < str.size(); j++) {
                int u = str[j] - 'a';
                a[i] |= (1 << u);
            }
        }
        int l = 0, r = n - 1;
        int f = 0;
        while (l < r) {
            if ((a[l] & a[r]) != 0) {
                l++;
                r--;
            }
            else {
                f = 1;
                break;
            }
        }
        if (!f) {
            cout << "Yes" << endl;
        }
        else cout << "No" << endl;
    }
}
