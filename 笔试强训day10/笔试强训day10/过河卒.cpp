#define _CRT_SECURE_NO_WARNINGS 1
//#include <iostream>
//#include<queue>
//#include<algorithm>
//using namespace std;
//const int N = 50;
//int g[N][N];
//bool st[N][N];
//int n, m, x1, y1;
//int dx[] = { 0, -2, -1, 1, 2, 2, 1, -1, -2 };
//int dy[] = { 0, 1, 2, 2, 1, -1, -2, -2, -1 };
//int ans;
//bool check(int x, int y) {
//    if (x >= 0 && x <= 20 && y >= 0 && y <= 20)return true;
//    return false;
//}
//
//void dfs(int x, int y) {
//    if (x == n && y == m) {
//        ans++;
//    }
//    // cout<<x<<" "<<y<<endl;
//    if (x + 1 <= n && y <= m && !st[x + 1][y]) {
//        st[x + 1][y] = true;
//        dfs(x + 1, y);
//        st[x + 1][y] = false;
//    }
//    if (x <= n && y + 1 >= 0 && y + 1 <= m && !st[x][y + 1]) {
//        st[x][y + 1] = true;
//        dfs(x, y + 1);
//        st[x][y + 1] = false;
//    }
//}
//
//int main() {
//    cin >> n >> m >> x1 >> y1;
//    st[x1][y1] = true;
//    for (int i = 0; i < 9; i++) {
//        int a = dx[i] + x1;
//        int b = dy[i] + y1;
//        if (check(a, b))st[a][b] = true;
//    }
//    dfs(0, 0);
//    cout << ans << endl;
//    return 0;
//}
#include <iostream>
#include<queue>
#include<algorithm>
using namespace std;
const int N = 50;
long long f[N][N];
bool st[N][N];
int n, m, x, y;
int dx[] = { 0, -2, -1, 1, 2, 2, 1, -1, -2 };
int dy[] = { 0, 1, 2, 2, 1, -1, -2, -2, -1 };

bool check(int a, int b) {
    if (x == a && y == b)return false;
    if (abs(a - x) + abs(b - y) == 3) {
        return false;
    }
    // cout<<abs(a - x) + abs(b - y) <<"--"<<endl;
    return true;
}
int main() {
    int n, m, x, y;
    cin >> n >> m >> x >> y;
    if (n == x && m == y) {
        cout << 0 << endl;
        return 0;
    }

    for (int i = 0; i < 9; i++) {
        int a = x + dx[i];
        int b = y + dy[i];
        if (a >= 0 && a <= n && b >= 0 && b <= m)st[a][b] = true;
    }
    for (int i = 1; i <= m; i++) {
        if (st[0][i]) {
            break;
        }
        f[0][i] = 1;
        //cout<<i<<" ";
    }
    for (int i = 1; i <= n; i++) {
        if (st[i][0]) {
            break;
        }
        f[i][0] = 1;
        //cout<<i<<" ";
    }
    f[0][0] = 1;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            if (st[i][j])continue;
            if (!st[i - 1][j])f[i][j] += f[i - 1][j];
            if (!st[i][j - 1])f[i][j] += f[i][j - 1];
            //  cout<<f[i][j]<<" ";
        }
        //cout<<endl;
    }

    cout << f[n][m] << endl;
    return 0;
}
