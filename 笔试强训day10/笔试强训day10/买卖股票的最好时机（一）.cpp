#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
const int N = 1e5 + 10;
int a[N];
int s[N];
int main() {
    int n;
    cin >> n;
    for (int i = 1; i <= n; i++) {
        cin >> a[i];
    }
    s[n] = a[n];
    for (int i = n - 1; i >= 1; i--) {
        s[i] = max(s[i + 1], a[i]);
    }

    int ans = 0;
    for (int i = 1; i < n; i++) {
        ans = max(ans, s[i + 1] - a[i]);
    }
    cout << ans;

    return 0;
}
