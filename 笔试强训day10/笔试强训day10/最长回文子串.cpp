#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:

    int getLongestPalindrome(string A) {
        int n = A.size();
        vector<vector<bool>> dp(n + 1, vector<bool>(n + 1));
        if (A.size() == 1)return 1;

        for (int i = 1; i <= n; i++) {
            dp[i][i] = true;
        }
        int ans = 1;
        for (int len = 1; len <= n; len++) {
            for (int l = 1; l + len - 1 <= n; l++) {
                int r = l + len - 1;
                if (len == 1)continue;
                if (A[l - 1] != A[r - 1]) {
                    dp[l][r] = false;
                }
                else {
                    if (len <= 3)dp[l][r] = true;
                    else dp[l][r] = dp[l + 1][r - 1];
                }

                if (dp[l][r])ans = max(ans, r - l + 1);
            }
        }
        return ans;

    }
};