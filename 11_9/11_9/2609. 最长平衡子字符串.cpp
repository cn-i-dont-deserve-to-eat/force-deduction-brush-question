#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int findTheLongestBalancedSubstring(string s) {
        int cnt0 = 0;
        int cnt1 = 0;
        if (s[0] == '0')cnt0++;
        else cnt1++;
        int ans = 0;
        for (int i = 1; i < s.size(); i++) {

            if ((s[i] != s[i - 1] && s[i] == '0')) {
                ans = max(min(cnt0, cnt1) * 2, ans);

                cnt0 = 0;
                cnt1 = 0;
            }
            if (s[i] == '0')cnt0++;
            else cnt1++;
            cout << cnt0 << " " << cnt1 << endl;
        }
        if (s[s.size() - 1] == '1') {
            ans = max(min(cnt0, cnt1) * 2, ans);
        }
        return ans;
    }
};