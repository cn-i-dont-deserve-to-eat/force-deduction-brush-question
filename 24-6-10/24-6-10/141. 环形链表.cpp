#define _CRT_SECURE_NO_WARNINGS 1
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    bool hasCycle(ListNode* head) {
        if (head == nullptr)return false;

        ListNode* slow = head;
        ListNode* fast = head;
        while (slow && fast && fast->next) {
            //cout<<slow->val<<" "<<fast->val<<endl;
            slow = slow->next;
            fast = fast->next;
            fast = fast->next;
            if (slow == fast)return true;
        }
        return false;
    }
};