#define _CRT_SECURE_NO_WARNINGS 1
typedef pair<int, int> PII;
class Solution {
public:
    int dfs(int a, vector<vector<PII>>& g, int fa) {
        int s = 0;
        for (auto it : g[a]) {
            if (it.first == fa)continue;
            if (it.second == 1)s += 1;
            s += dfs(it.first, g, a);
        }
        // cout<<a<<" "<<s<<endl;
        return s;
    }
    int minReorder(int n, vector<vector<int>>& connections) {

        int len = connections.size();
        int ans = 0;
        vector<vector<PII>> g(len + 1);
        // vector<vector<char>>st(len+1,vector<char>(len+1,'0'));
        for (auto it : connections) {
            g[it[0]].push_back({ it[1],1 });
            g[it[1]].push_back({ it[0],0 });
            // st[it[0]][it[1]]='1';
        }
        ans += dfs(0, g, -1);
        return ans;
    }
};