#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int minSubArrayLen(int target, vector<int>& nums) {
        int len = nums.size();
        int sum = 0;
        int ans = 1e9;
        int f = 0;
        for (int r = 0, l = 0; r < len; r++) {
            if (sum + nums[r] < target) {
                sum += nums[r];
            }
            else {
                f = 1;
                ans = min(ans, r - l + 1);
                sum += nums[r];
                while (l <= r && sum >= target) {
                    ans = min(ans, r - l + 1);
                    sum -= nums[l];
                    l++;
                }
            }

        }
        if (f == 0)return 0;
        return ans;
    }
};