#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<algorithm>
#include<unordered_map>
using namespace std;
const int N = 1e5 + 10;
int a[N];
int dp[10010];
int main() {
    int n;
    cin >> n;
    unordered_map<int, int> mp;
    int mx = 0;
    int mi = 1e9;
    for (int i = 1; i <= n; i++) {
        cin >> a[i];
        mp[a[i]]++;
        mx = max(mx, a[i]);
        mi = min(mi, a[i]);
    }

    for (int i = mi; i <= mx; i++) {
        dp[i] = max(dp[i - 1], dp[i - 2] + mp[i] * i);
    }
    cout << dp[mx] << endl;
    return 0;
}
