#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

int main() {
    string a;
    string b;
    int mp[26] = { 0 };
    cin >> a >> b;
    for (int i = 0; i < b.size(); i++) {
        int u = b[i] - 'A';
        mp[u]++;
    }

    for (int i = 0; i < a.size(); i++) {
        int u = a[i] - 'A';
        mp[u]--;
    }

    for (int i = 0; i < 26; i++) {
        if (mp[i] > 0) {
            cout << "No" << endl;
            return 0;
        }
    }
    cout << "Yes" << endl;
    return 0;
}
