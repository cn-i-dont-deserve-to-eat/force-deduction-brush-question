#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<algorithm>
using namespace std;
const int N = 3e5 + 10;
int a[N];
int main() {
    int n;
    cin >> n;
    for (int i = 1; i <= 3 * n; i++)cin >> a[i];
    sort(a + 1, a + 3 * n + 1);
    long long ans = 0;
    for (int i = 3 * n - 1; i >= n + 1; i -= 2) {
        ans += a[i];
    }
    cout << ans << endl;
    return 0;
}
