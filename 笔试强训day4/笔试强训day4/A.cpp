#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<algorithm>
using namespace std;

int main() {
    int n;
    cin >> n;
    int a = 0;
    int b = 1;
    int ans = min(abs(0 - n), abs(1 - n));
    while (a + b < n) {
        ans = min(ans, abs(n - (a + b)));
        int c = a + b;
        a = b;
        b = c;
    }
    ans = min(ans, abs(n - (a + b)));
    cout << ans;
    return 0;
}