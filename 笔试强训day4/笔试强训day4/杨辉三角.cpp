#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
const int N = 35;
int a[N][N];
int main() {
    int n;
    cin >> n;
    a[1][1] = 1;
    printf("%5d\n", 1);
    for (int i = 2; i <= n; i++) {
        for (int j = 1; j <= i; j++) {
            a[i][j] = a[i - 1][j] + a[i - 1][j - 1];
            printf("%5d", a[i][j]);

        }
        if (i != n)cout << endl;
    }
    return 0;
}
