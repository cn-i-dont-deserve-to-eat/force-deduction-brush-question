#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    string convert(string s, int numRows) {
        vector<string> a(numRows);

        int n = s.size();
        int cnt = 0;
        int f1 = 0;
        int f2 = 0;
        if (numRows == 1)return s;
        while (cnt < n) {
            a[f1].push_back(s[cnt++]);
            if (f1 < numRows - 1 && !f2)f1++;
            else if (f1 == numRows - 1 && !f2) {
                f1--;
                f2 = 1;
            }
            else if (f1 > 0 && f2) {
                f1--;
            }
            else if (f1 == 0 && f2) {
                f2 = 0;
                f1++;
            }
        }

        string ans = "";
        for (auto it : a) {
            ans += it;
        }
        return ans;
    }
};