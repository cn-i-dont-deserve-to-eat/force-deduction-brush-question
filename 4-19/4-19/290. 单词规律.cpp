#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool wordPattern(string pattern, string s) {
        vector<string> t;
        int j = 0;
        int n = s.size();
        for (int i = 0; i < n; i++) {
            if ((s[i] == ' ' && s[i - 1] != ' ') || i == n - 1) {
                if (i != n - 1) t.push_back(s.substr(j, i - j));
                else t.push_back(s.substr(j, i - j + 1));
                j = i + 1;
            }
        }

        unordered_map<char, string> mp1;
        unordered_map<string, char> mp2;
        if (pattern.size() != t.size())return false;
        for (int i = 0; i < pattern.size(); i++) {
            if (mp1.count(pattern[i])) {
                if (mp1[pattern[i]] != t[i])return false;
            }
            else if (mp2.count(t[i])) {
                if (mp2[t[i]] != pattern[i])return false;
            }
            else {
                mp1[pattern[i]] = t[i];
                mp2[t[i]] = pattern[i];
            }
        }
        return true;
    }
};