#define _CRT_SECURE_NO_WARNINGS 1
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

class Solution {
public:
    TreeNode* dfs(TreeNode* root, TreeNode* target) {
        if (root->val == target->val)return root;
        TreeNode* l = nullptr;
        TreeNode* r = nullptr;
        if (root->left)l = dfs(root->left, target);
        if (root->right)r = dfs(root->right, target);

        if (l)return l;
        return r;
    }
    TreeNode* getTargetCopy(TreeNode* original, TreeNode* cloned, TreeNode* target) {
        return dfs(cloned, target);
    }
};