#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int minNumberOfFrogs(string croakOfFrogs) {
        unordered_map<char, int> mp;

        int n = croakOfFrogs.size();
        int ans = 0;
        int f = 1;
        int cnt = 0;
        for (int i = 0; i < n; i++) {
            if (croakOfFrogs[i] == 'c') {
                mp[croakOfFrogs[i]]++;
                if (cnt) {
                    ans--;
                    cnt--;
                }
                ans++;
                //if(mp['k']<mp['c'])ans++;
                if (mp['r'] >= mp['c'] || mp['o'] >= mp['c'] || mp['a'] >= mp['c'] || mp['k'] >= mp['c']) {
                    f = 0;
                    break;
                }
            }
            else if (croakOfFrogs[i] == 'r') {
                mp[croakOfFrogs[i]]++;
                if (mp['o'] >= mp['r'] || mp['a'] >= mp['r'] || mp['k'] >= mp['r']) {
                    f = 0;
                    break;
                }
            }
            else if (croakOfFrogs[i] == 'o') {
                mp[croakOfFrogs[i]]++;
                if (mp['a'] >= mp['o'] || mp['k'] >= mp['o']) {
                    f = 0;
                    break;
                }
            }
            else if (croakOfFrogs[i] == 'a') {
                mp[croakOfFrogs[i]]++;
                if (mp['k'] >= mp['a']) {
                    f = 0;
                    break;
                }
            }
            else {
                cnt++;
                mp[croakOfFrogs[i]]++;
                if (mp['k'] > mp['c'] || mp['k'] > mp['r'] || mp['k'] > mp['o'] || mp['k'] > mp['a']) {
                    f = 0;
                    break;
                }

                // if(i>5&&croakOfFrogs[i-5]=='k')continue;
                // else ans=min(ans,mp['c']-mp['k']+1);
                 //ans++;
            }
            //  cout<<ans<<endl;
        }
        // cout<<mp['c']<<endl;
        // cout<<mp['k']<<endl;
        int k = mp['a'];
        if (mp['c'] != k || mp['r'] != k || mp['o'] != k || mp['k'] != k)f = 0;
        if (f)return ans;
        return -1;
    }
};