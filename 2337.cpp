#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool canChange(string start, string target) {
        int n = start.size();
        //int n2=target.size();
        string s = "";
        string t = "";

        for (int i = 0; i < n; i++) {
            if (start[i] != '_')s += start[i];
            if (target[i] != '_')t += target[i];
        }
        bool ans = true;
        if (s != t)return false;
        for (int i = 0, j = 0; i < n; i++) {
            if (target[i] != '_') {
                while (start[j] == '_' && j < n) {
                    j++;
                }
                // cout<<i<<" "<<j<<endl;
                if (start[j] != target[i]) {
                    ans = false;
                    break;
                }
                if (target[i] == 'L' && start[j] == 'L' && i > j) {
                    ans = false;
                    break;
                }
                else if (target[i] == 'R' && start[j] == 'R' && j > i) {
                    ans = false;
                    break;
                }
                j++;
            }


        }
        return ans;
    }
};