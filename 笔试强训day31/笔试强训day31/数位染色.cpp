#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<vector>
#include<algorithm>
using namespace std;
int main() {
    string str;
    cin >> str;
    vector<int> a;
    int s = 0;
    for (int i = 0; i < str.size(); i++) {
        int u = str[i] - '0';
        s += u;
        a.push_back(u);
    }
    if (s % 2) {
        cout << "No" << endl;
        return 0;
    }
    int n = a.size();
    int f = 0;
    int ma = (1 << (n + 1)) - 1;
    for (int i = 1; i < ma; i++) {
        int t = 0;
        for (int j = 0; j < n; j++) {
            if ((i >> j) & 1)t += a[j];
        }
        if (t == s / 2) {
            f = 1;
            break;
        }
    }
    if (f)cout << "Yes" << endl;
    else cout << "No" << endl;
    return 0;
}
