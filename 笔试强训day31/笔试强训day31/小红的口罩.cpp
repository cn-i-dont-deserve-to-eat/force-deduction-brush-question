#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<queue>
using namespace std;
const int N = 1e5 + 10;
int a[N];

int main() {
    int n, k;
    cin >> n >> k;
    priority_queue<int, vector<int>, greater<int>> q;
    for (int i = 0; i < n; i++) {
        cin >> a[i];
        q.push(a[i]);
    }

    int ans = 0;

    while (!q.empty() && k > 0) {
        int t = q.top();
        q.pop();

        k -= t;
        if (k >= 0)ans++;
        q.push(t * 2);
    }

    cout << ans << endl;
    return 0;
}