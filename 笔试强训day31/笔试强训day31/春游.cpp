#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
typedef long long LL;
int main() {
    int t;
    cin >> t;
    while (t--) {
        LL n, a, b;
        cin >> n >> a >> b;
        LL ans = 0;
        if (n <= 2) {
            cout << min(a, b) << endl;
            continue;
        }
        if (3 * a >= 2 * b) {
            int k = n % 3;
            ans += n / 3 * b;
            if (k == 2)ans += min(a, b);
            else if (k == 1) {
                ans += min(min(a, b), 2 * a - b);
            }
        }
        else {
            int k = n % 2;
            ans += n / 2 * a;
            if (k) {
                ans += min(a, b - a);
            }
        }
        cout << max(ans, (LL)0) << endl;
    }


    return 0;
}

