#define _CRT_SECURE_NO_WARNINGS 1
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */

bool cmp(struct TreeNode* r1, struct TreeNode* r2) {
    if (r1 == NULL && r2 != NULL)return false;
    if (r1 != NULL && r2 == NULL)return false;
    if (r1 == NULL && r2 == NULL)return true;
    if (r1->val == r2->val) {
        return cmp(r1->left, r2->left) && cmp(r1->right, r2->right);
    }
    else {
        return false;
    }
}

bool pre(struct  TreeNode* root, struct TreeNode* r1) {
    if (root == NULL) {
        return false;
    }
    if (cmp(r1, root)) {
        //  printf("11 %d\n",root->val);
        return true;
    }
    else  return pre(root->left, r1) || pre(root->right, r1);
}

bool isSubtree(struct TreeNode* root, struct TreeNode* subRoot) {
    return pre(root, subRoot);
}