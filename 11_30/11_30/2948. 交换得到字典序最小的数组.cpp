#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<int> lexicographicallySmallestArray(vector<int>& nums, int limit) {
        int n = nums.size();
        typedef pair<int, int> PII;
        vector<PII> v;
        for (int i = 0; i < n; i++) {
            v.push_back({ nums[i],i });
        }
        sort(v.begin(), v.end());
        int st = 0;
        vector<int> t;
        vector<int> ans(n);
        while (st < n) {
            int i = st;
            t.push_back(v[i].second);
            i++;
            while (i < n && v[i].first - v[i - 1].first <= limit) {
                t.push_back(v[i].second);
                i++;
            }
            sort(t.begin(), t.end());
            for (int j = 0; j < t.size(); j++) {
                ans[t[j]] = v[st + j].first;
            }
            t.clear();
            st = i;
        }
        return ans;
    }
};