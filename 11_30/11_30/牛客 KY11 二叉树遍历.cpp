#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include<string.h>
#include<stdlib.h>
typedef struct tree {
    char val;
    struct tree* left;
    struct tree* right;
}treenode;


treenode* creat(char* arr, int* pi, int n) {
    int pos = *(pi);
    if (pos >= n || arr[pos] == '#') {
        return NULL;
    }
    treenode* node = (treenode*)malloc(sizeof(treenode));
    node->val = arr[pos];
    node->left = node->right = NULL;
    (*pi)++;
    node->left = creat(arr, pi, n);
    (*pi)++;
    node->right = creat(arr, pi, n);
    return node;
}

void lastordered(treenode* root) {
    if (root == NULL) {
        return;
    }
    lastordered(root->left);
    printf("%c ", root->val);
    lastordered(root->right);
}
int main() {
    char arr[105] = { 0 };
    while (scanf("%s", arr) != EOF) {
        int n = strlen(arr);
        //printf("%d\n",n);
        int x = 0;
        treenode* root = creat(arr, &x, n);
        lastordered(root);
        printf("\n");

    }
    return 0;
}