#define _CRT_SECURE_NO_WARNINGS 1
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */
struct TreeNode* resever(struct TreeNode* root1) {
    if (root1 == NULL) {
        return NULL;
    }
    struct TreeNode* node = (struct TreeNode*)malloc(sizeof(struct TreeNode));
    node->val = root1->val;
    node->left = node->right = NULL;
    node->right = resever(root1->left);
    node->left = resever(root1->right);
    return node;
}

struct TreeNode* invertTree(struct TreeNode* root) {
    return resever(root);
}