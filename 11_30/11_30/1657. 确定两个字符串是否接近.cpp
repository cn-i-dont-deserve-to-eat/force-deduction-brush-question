#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool closeStrings(string word1, string word2) {
        int n1 = word1.size();
        int n2 = word2.size();
        if (n1 != n2)return false;
        map<int, int>mp1;
        map<int, int>mp2;
        for (int i = 0; i < n1; i++) {
            int u = word1[i] - 'a';
            mp1[u]++;
        }
        for (int i = 0; i < n2; i++) {
            int u = word2[i] - 'a';
            if (!mp1[u])return false;
            mp2[u]++;
        }
        vector<int> cnt1;
        vector<int> cnt2;
        for (auto it : mp1) {
            cnt1.push_back(it.second);
        }
        for (auto it : mp2) {
            cnt2.push_back(it.second);
        }
        if (cnt1.size() != cnt2.size())return false;
        sort(cnt1.begin(), cnt1.end());
        sort(cnt2.begin(), cnt2.end());

        for (int i = 0; i < cnt1.size(); i++) {
            if (cnt1[i] != cnt2[i]) {
                return false;
            }
        }
        return true;

    }
};