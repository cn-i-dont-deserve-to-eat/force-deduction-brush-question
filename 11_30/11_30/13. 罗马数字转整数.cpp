#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int romanToInt(string s) {
        int ans = 0;
        int len = s.size();
        for (int i = 0; i < len; i++) {
            if (s[i] == 'I')ans += 1;
            else if (s[i] == 'V') {
                ans += 5;
                if (i >= 1 && s[i - 1] == 'I')ans -= 2;
            }
            else if (s[i] == 'X') {
                ans += 10;
                if (i >= 1 && s[i - 1] == 'I')ans -= 2;
            }
            else if (s[i] == 'L') {
                ans += 50;
                if (i >= 1 && s[i - 1] == 'X')ans -= 20;
            }
            else if (s[i] == 'C') {
                ans += 100;
                if (i >= 1 && s[i - 1] == 'X')ans -= 20;
            }
            else if (s[i] == 'D') {
                ans += 500;
                if (i >= 1 && s[i - 1] == 'C')ans -= 200;
            }
            else if (s[i] == 'M') {
                ans += 1000;
                if (i >= 1 && s[i - 1] == 'C')ans -= 200;
            }
        }
        return ans;
    }
};