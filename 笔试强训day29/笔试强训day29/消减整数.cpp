#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<queue>
using namespace std;

int main() {
    int t;
    cin >> t;
    while (t--) {
        int h;
        cin >> h;
        int d = 1;
        int ans = 0;
        while (h) {
            h -= d;
            ans++;
            if (h % (2 * d) == 0)d *= 2;
        }
        cout << ans << endl;
    }
    return 0;
}
