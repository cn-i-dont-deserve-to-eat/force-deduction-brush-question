#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
const int N = 1e5 + 10;
int a[N];
int main() {
    int n;
    cin >> n;
    for (int i = 0; i < n; i++)cin >> a[i];
    if (n <= 2) {
        cout << 1 << endl;
        return 0;
    }
    int ans = 0;
    for (int i = 1; i < n; i++) {
        if (a[i] > a[i - 1]) {
            ans++;
            while (i < n && a[i] >= a[i - 1]) {
                i++;
            }
        }
        else if (a[i] < a[i - 1]) {
            ans++;
            while (i < n && a[i] <= a[i - 1]) {
                i++;
            }
        }
        if (i == n - 1)ans++;
    }
    cout << ans << endl;
    return 0;
}
// 64 λ������� printf("%lld")