#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int maximizeSum(vector<int>& nums, int k) {
        int ma = 0;
        int len = nums.size();
        for (int i = 0; i < len; i++) {
            ma = max(ma, nums[i]);
        }
        return ma * k + k * (k - 1) / 2;

    }
};