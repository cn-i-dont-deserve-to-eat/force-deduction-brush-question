#define _CRT_SECURE_NO_WARNINGS 1
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
int ans = INT_MIN;
class Solution {
public:
    int maxsum(TreeNode* root) {
        if (root == NULL)return 0;
        int l = max(0, maxsum(root->left));
        int r = max(0, maxsum(root->right));
        if (l && r) {
            ans = max(ans, root->val + l + r);
        }
        else {
            ans = max(ans, root->val + max(l, r));
        }
        return (int)root->val + max(l, r);
    }
    int maxPathSum(TreeNode* root) {
        ans = INT_MIN;
        maxsum(root);
        return ans;
    }
};