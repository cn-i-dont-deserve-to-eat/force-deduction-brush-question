#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int numSquares(int n) {
        vector<int>dp(n + 1);

        //    for(int i=0;i<=sqrt(n);i++){
        //        dp[i]=1;
        //    }
        for (int i = 1; i <= n; i++) {
            int res = 1e5;
            for (int j = 1; j * j <= i; j++) {
                res = min(res, dp[i - j * j]);
            }
            dp[i] = res + 1;
        }
        return dp[n];
    }
};