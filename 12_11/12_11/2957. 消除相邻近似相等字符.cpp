#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool iseq(char a, char b) {
        if (a > b)swap(a, b);
        if (a == b)return true;
        if (b - a == 1)return true;
        return false;
    }
    int removeAlmostEqualCharacters(string word) {
        int len = word.size();
        int ans = 0;
        for (int i = 1; i < len; i++) {
            if (!iseq(word[i], word[i - 1]))continue;
            if (i == len - 1) {
                if (iseq(word[i], word[i - 1])) {
                    word[i] = 'D';
                    ans++;
                }
            }
            else {
                if (iseq(word[i], word[i - 1]) || iseq(word[i], word[i + 1])) {
                    word[i] = 'D';
                    ans++;
                }
            }
        }
        return ans;
    }
};class Solution {
public:
    bool iseq(char a,char b){
        if(a>b)swap(a,b);
        if(a==b)return true;
        if(b-a==1)return true;
        return false;
    }
    int removeAlmostEqualCharacters(string word) {
        int len=word.size();
        int ans=0;
        for(int i=1;i<len;i++){
            if(!iseq(word[i],word[i-1]))continue;
            if(i==len-1){
                if(iseq(word[i],word[i-1])){
                    word[i]='D';
                    ans++;
                }
            }else{
                if(iseq(word[i],word[i-1])||iseq(word[i],word[i+1])){
                    word[i]='D';
                    ans++;
                }
            }
        }
        return ans;
    }
};