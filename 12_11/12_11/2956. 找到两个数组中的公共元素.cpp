#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<int> findIntersectionValues(vector<int>& nums1, vector<int>& nums2) {
        map<int, int> mp1;
        map<int, int> mp2;
        for (int i = 0; i < nums1.size(); i++) {
            mp1[nums1[i]]++;
        }
        for (int i = 0; i < nums2.size(); i++) {
            mp2[nums2[i]]++;
        }
        int a = 0;
        int b = 0;
        for (int i = 0; i < nums1.size(); i++) {
            if (mp2[nums1[i]])a++;
        }
        for (int i = 0; i < nums2.size(); i++) {
            if (mp1[nums2[i]])b++;
        }
        return { a,b };
    }
};