#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int maxSubarrayLength(vector<int>& nums, int k) {
        map<int, int> mp;
        int len = nums.size();
        int ans = 0;
        for (int i = 0, j = 0; i < len; i++) {
            mp[nums[i]]++;
            while (mp[nums[i]] > k) {
                mp[nums[j++]]--;
            }
            ans = max(ans, i - j + 1);
        }
        return ans;
    }
};