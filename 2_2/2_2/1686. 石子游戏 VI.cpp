#define _CRT_SECURE_NO_WARNINGS 1
bool cmp(vector<int>& A, vector<int>& B) {
    return A[0] + A[1] > B[0] + B[1];
}
class Solution {
public:

    int stoneGameVI(vector<int>& aliceValues, vector<int>& bobValues) {
        int n = aliceValues.size();
        vector<vector<int>> s(n, vector<int>(2));
        for (int i = 0; i < n; i++) {
            s[i][0] = aliceValues[i];
            s[i][1] = bobValues[i];
        }
        sort(s.begin(), s.end(), cmp);
        int a = 0;
        int b = 0;
        int f = 0;
        for (int i = 0; i < n; i++) {
            if (f == 0) {
                a += s[i][0];
                f = 1;
            }
            else {
                b += s[i][1];
                f = 0;
            }
        }
        if (a == b)return 0;
        else if (a > b)return 1;
        else return -1;
    }
};