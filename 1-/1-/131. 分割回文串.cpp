#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    void dfs(int pos, string s, vector<vector<string>>& ans, vector<string>& res, int len, vector<vector<bool>>dp) {
        if (pos == len) {
            ans.push_back(res);
            return;
        }
        for (int i = pos; i < len; i++) {
            if (dp[pos][i]) {
                res.push_back(s.substr(pos, i - pos + 1));
                dfs(i + 1, s, ans, res, len, dp);
                res.pop_back();
            }
        }
    }
    vector<vector<string>> partition(string s) {
        vector<vector<string>> ans;
        int len = s.size();
        vector<vector<bool>>dp(len, vector<bool>(len, true));

        for (int i = 1; i <= len; i++) {
            for (int l = 0; l + i - 1 < len; l++) {
                int r = l + i - 1;
                if (i <= 1) {
                    dp[l][r] = true;
                    continue;
                }
                if (s[l] == s[r]) {
                    dp[l][r] = dp[l + 1][r - 1];
                }
                else {
                    dp[l][r] = false;
                }
            }
        }
        vector<string> res;
        dfs(0, s, ans, res, len, dp);
        return ans;
    }
};