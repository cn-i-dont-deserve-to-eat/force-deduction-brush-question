#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int getpow(int x) {
        int res = 0;
        while (x) {
            res++;
            x /= 10;
        }
        return res;
    }
    int fun(int i, int j, int k) {
        int res = k;
        int a = k / (int)pow(10, i - 1) % 10;
        int b = k / (int)pow(10, j - 1) % 10;
        int p1 = a * pow(10, i - 1);
        int p2 = b * pow(10, j - 1);
        // cout<<a<<" --"<<b<<endl;
        // cout<<p1<<" "<<p2<<endl;
        res -= p1;
        res -= p2;
        p1 = b * pow(10, i - 1);
        p2 = a * pow(10, j - 1);
        //  cout<<p1<<" "<<p2<<endl;
        res += p1 + p2;

        return res;
    }
    int maximumSwap(int num) {
        int n = getpow(num);
        if (n <= 1)return num;

        int ans = num;
        //cout<<fun(1,2,num)<<endl;
        for (int i = 1; i <= n; i++) {
            for (int j = i + 1; j <= n; j++) {
                //cout<<i<<" "<<j<<" "<<fun(i,j,num)<<endl;
                ans = max(ans, fun(i, j, num));
            }
        }
        return ans;
    }
};