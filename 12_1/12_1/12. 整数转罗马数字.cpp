#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    string intToRoman(int num) {
        string ans = "";
        while (num) {
            if (num >= 1000) {
                int k = num / 1000;
                num = num % 1000;
                while (k--) {
                    ans += "M";
                }
            }
            else if (num >= 900) {
                int k = num / 900;
                num = num % 900;
                while (k--) {
                    ans += "CM";
                }
            }
            else if (num >= 500) {
                int k = num / 500;
                num = num % 500;
                while (k--) {
                    ans += "D";
                }
            }
            else if (num >= 400) {
                int k = num / 400;
                num = num % 400;
                while (k--) {
                    ans += "CD";
                }
            }
            else if (num >= 100) {
                int k = num / 100;
                num = num % 100;
                while (k--) {
                    ans += "C";
                }
            }
            else if (num >= 90) {
                int k = num / 90;
                num = num % 90;
                while (k--) {
                    ans += "XC";
                }
            }
            else if (num >= 50) {
                int k = num / 50;
                num = num % 50;
                while (k--) {
                    ans += "L";
                }
            }
            else if (num >= 40) {
                int k = num / 40;
                num = num % 40;
                while (k--) {
                    ans += "XL";
                }
            }
            else if (num >= 10) {
                int k = num / 10;
                num = num % 10;
                while (k--) {
                    ans += "X";
                }
            }
            else if (num >= 9) {
                int k = num / 9;
                num = num % 9;
                while (k--) {
                    ans += "IX";
                }
            }
            else if (num >= 5) {
                int k = num / 5;
                num = num % 5;
                while (k--) {
                    ans += "V";
                }
            }
            else if (num >= 4) {
                int k = num / 4;
                num = num % 4;
                while (k--) {
                    ans += "IV";
                }
            }
            else if (num >= 1) {
                int k = num / 1;
                num = num % 1;
                while (k--) {
                    ans += "I";
                }
            }
        }
        return ans;
    }
};