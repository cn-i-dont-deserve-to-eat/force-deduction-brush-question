#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int lengthOfLastWord(string s) {
        int n = s.size();
        int j = -1;
        for (int i = n - 1; i >= 0; i--) {
            if (s[i] != ' ' && j == -1)j = i;
            if (j != -1 && s[i] == ' ' && s[i + 1] != ' ') {
                cout << i << " " << j;
                return j - i;
            }
        }
        return j + 1;
    }
};