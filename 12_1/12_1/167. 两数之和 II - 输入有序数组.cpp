#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<int> twoSum(vector<int>& numbers, int target) {
        int len = numbers.size();
        for (int i = 0, j = len - 1; i < len; i++) {
            // cout<<j<<endl;
            while (j >= 0 && j > i && numbers[i] + numbers[j] > target) {
                j--;
            }
            if (numbers[i] + numbers[j] == target) {
                return { i + 1,j + 1 };
            }
        }
        return { -1,-1 };
    }
};