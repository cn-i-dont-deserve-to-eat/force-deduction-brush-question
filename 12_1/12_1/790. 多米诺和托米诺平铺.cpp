#define _CRT_SECURE_NO_WARNINGS 1
const int mod = 1e9 + 7;
class Solution {
public:
    int numTilings(int n) {
        vector<vector<int>> dp(n + 1, vector<int>(4));
        dp[0][0] = 0;
        dp[0][1] = 0;
        dp[0][2] = 0;
        dp[0][3] = 1;
        for (int i = 1; i <= n; i++) {
            dp[i][0] = dp[i - 1][3] % mod;
            dp[i][1] = ((long long)dp[i - 1][2] + dp[i - 1][0]) % mod;
            dp[i][2] = ((long long)dp[i - 1][1] + dp[i - 1][0]) % mod;
            dp[i][3] = ((long long)dp[i - 1][3] + dp[i - 1][0] + dp[i - 1][2] + dp[i - 1][1]) % mod;

        }
        return dp[n][3];
    }
};