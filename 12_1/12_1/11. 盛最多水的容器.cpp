#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int maxArea(vector<int>& height) {
        int ans = 0;
        int len = height.size();
        int i = 0;
        int j = len - 1;
        while (i < j) {
            int k = j - i;
            int g = min(height[i], height[j]);
            ans = max(ans, k * g);
            if (height[i] >= height[j])j--;
            else i++;
        }
        return ans;
    }
};