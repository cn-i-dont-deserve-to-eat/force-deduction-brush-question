#define _CRT_SECURE_NO_WARNINGS 1

class Solution {
public:

    // ListNode* reserve_l(ListNode* list){
    //     if(list==nullptr)return nullptr;
    //     if(list->next==nullptr)return list;
    //     ListNode* pre=nullptr;
    //     ListNode* cur=list;
    //     while(cur){
    //         ListNode* temp=cur->next;
    //         cur->next=pre;
    //         pre=cur;
    //         cur=temp;
    //     }
    //     ListNode* tt=pre;

    //     while(tt){
    //         cout<<tt->val<<endl;
    //         tt=tt->next;
    //     }
    //     return pre;
    // }
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        ListNode* cur1 = l1;
        ListNode* cur2 = l2;
        ListNode* newlist = new ListNode(-1);
        ListNode* cur = newlist;
        int t = 0;
        while (cur1 || cur2) {
            if (cur1) {
                t += cur1->val;
                cur1 = cur1->next;
            }
            if (cur2) {
                t += cur2->val;
                cur2 = cur2->next;
            }
            ListNode* temp = new ListNode(t % 10);
            cur->next = temp;
            cur = cur->next;
            //  cout<<cur->val<<endl;
            t /= 10;
        }
        if (t) {
            ListNode* temp = new ListNode(t);
            cur->next = temp;
            cur = cur->next;
        }
        //   ListNode* tt=newlist->next;

        //   delete newlist;
        return  newlist->next;

    }
};