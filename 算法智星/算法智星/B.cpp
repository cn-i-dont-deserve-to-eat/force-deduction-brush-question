//#include<iostream>
//#include<algorithm>
//#include<string>
//using namespace std;
//typedef long long LL;
//
//int getp(LL x) {
//	int res = 0;
//	while (x) {
//		res++;
//		x /= 10;
//	}
//	return res;
//}
//
//
//int main() {
//	int n;
//	cin >> n;
//	int a[10] = { 0 };
//	a[0] = 0;
//	a[1] = 7;
//	a[2] = 5;
//	a[3] = 3;
//	a[4] = 1;
//	a[5] = 8;
//	a[6] = 6;
//	a[7] = 4;
//	a[8] = 2;
//	a[9] = 9;
//	while (n--) {
//		LL num;
//		cin >> num;
//		string str = to_string(num);
//		int cnt2 = str.size();
//		int ans = 0;
//		int c = 0;
//		for (int i = cnt2-1; i >=0; i--) {
//			c++;
//			if (c % 2==0) {
//				int u = str[i] - '0';
//				ans +=  u;
//				//cout << u <<" " << endl;
//				continue;
//			}
//			//
//			int u = str[i] - '0';
//			//cout << a[u] << " " << endl;
//			ans+=a[u];
//		}
//	//	cout << ans << endl;
//		if (ans % 8) {
//			cout << "F" << endl;
//		}
//		else {
//			cout << "T" << endl;
//		}
//	}
//
//
//	return 0;
//}
//
//#include<iostream>
//#include<algorithm>
//#include<string>
//using namespace std;
//const int N=1e5+10;
//int st[N];
//
//
//int d(int x) {
//	int res = 0;
//	while (x>=0&&!st[x]) {
//		x--;
//		res++;
//	}
//	return x;
//}
//
//string fun(int x) {
//	if (x == 0)return "";
//	if (x == 1)return "2(0)";
//	if (x == 2)return "2";
//	if (st[x])return "2(" + fun(st[x]) + ")";
//	else return   fun(d(x)) + "+"+ fun(x - d(x));
//}
//
//int main() {
//	st[1] = 1;
//	int p = 2;
//	for (int i = 1; i <= 15; i++) {
//		st[p] =i;
//		p = p * 2;
//
//	}
//
//	int x;
//	cin >> x;
//	cout << fun(x);
//	//cout << d(5) << endl;
//	return 0;
//}
//
//#include<iostream>
//#include<algorithm>
//using namespace std;
//const int N = 2e5 + 10;
//int a[N];
//long long s[N];
//int b[N];
//int mx[N];
//int main() {
//	int tt;
//	cin >> tt;
//	while (tt--) {
//		int n;
//		int k;
//		cin >> n >> k;
//		for (int i = 1; i <= n; i++)cin >> a[i];
//		for (int i = 1; i <= n; i++)cin >> b[i];
//
//		for (int i = 1; i <= n; i++) {
//			s[i] = (long long)s[i - 1] + a[i];
//		}
//		for (int i = 1; i <= n; i++) {
//			mx[i] = max(mx[i - 1], b[i]);
//		}
//
//		long long ans = 0;
//		for (int i = 1; i <= n; i++) {
//			if (i > k)break;
//			long long t = s[i];
//			if(k-i>=0)t += (long long)mx[i] * (k - i);
//			ans = max(ans, t);
//		}
//		cout << ans << endl;
//	}
//	
//	return 0;
//}

#include<iostream>
#include<algorithm>
#include<queue>
#include<unordered_map>
using namespace std;
int a[26] = { 0 };

int main() {
	int t;
	cin >> t;
	while (t--) {
		string str;
		int n;
		cin >> n;
		cin >> str;
		for (int i = 0; i < 26; i++)a[i] = 0;
		for (int i =0 ; i < n; i++) {
			int u = str[i] - 'a';
			a[u]++;
		//	cout << u << " ";
		}
		priority_queue<int> q;
		for (int i = 0; i < 26; i++) {
			if (a[i]) {
				q.push(a[i]);
			}
		}

		int ans = (int)str.size();
		if (q.size() < 2) {
			cout << ans << endl;
			continue;
		}
		while (q.size()>=3) {
			int t1 = q.top();
			q.pop();
			int t2 = q.top();
			q.pop();
			if (t1 != t2) {
				q.push(t1-t2);
				ans -= 2 * t2;
			}
			else {
		
				t2--;
				if(t1>0)q.push(t1);
				if(t2>0)q.push(t2);
				if(t1&&t2)ans -= 2;
			}
		}
		cout << ans << endl;

	}

	return 0;
}