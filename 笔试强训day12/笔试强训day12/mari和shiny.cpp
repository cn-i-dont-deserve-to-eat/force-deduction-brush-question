#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

int main() {
    string str;
    int n;
    cin >> n;
    cin >> str;
    long long a = 0, b = 0;
    long long ans = 0;
    long long k = 0;
    for (int i = 0; i < n; i++) {
        if (str[i] == 's')a++;
        else if (str[i] == 'h')b = a, k += b;
        else if (str[i] == 'y') {
            ans += k;
        }
    }
    cout << ans << endl;

    return 0;
}
