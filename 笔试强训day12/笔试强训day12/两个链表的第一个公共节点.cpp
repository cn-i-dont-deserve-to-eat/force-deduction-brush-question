#define _CRT_SECURE_NO_WARNINGS 1
/*
struct ListNode {
	int val;
	struct ListNode *next;
	ListNode(int x) :
			val(x), next(NULL) {
	}
};*/
class Solution {
public:
	ListNode* FindFirstCommonNode(ListNode* pHead1, ListNode* pHead2) {
		if (!pHead1 || !pHead2)return nullptr;
		ListNode* cur1 = pHead1;
		ListNode* cur2 = pHead2;
		int n1 = 0;
		int n2 = 0;
		while (cur1) {
			cur1 = cur1->next;
			n1++;
		}
		while (cur2) {
			cur2 = cur2->next;
			n2++;
		}
		cur1 = pHead1;
		cur2 = pHead2;
		if (n1 > n2) {
			while (n1 > n2) {
				cur1 = cur1->next;
				n1--;
			}

		}
		else {
			while (n1 < n2) {
				cur2 = cur2->next;
				n2--;
			}
		}

		while (cur1 && cur2 && cur1 != cur2) {
			cur1 = cur1->next;
			cur2 = cur2->next;
		}

		return cur1;
	}
};
