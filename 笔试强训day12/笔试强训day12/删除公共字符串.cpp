#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<unordered_map>
using namespace std;

int main() {
    string str1, str2;
    getline(cin, str1);
    getline(cin, str2);
    unordered_map<char, int> mp;
    // cout<<str1<<" "<<str2<<endl;
    for (int i = 0; i < str2.size(); i++) {
        mp[str2[i]]++;
    }
    string ans = "";
    for (int i = 0; i < str1.size(); i++) {
        if (mp.count(str1[i]))continue;
        ans += str1[i];
    }
    cout << ans << endl;
    return 0;
}
// 64 λ������� printf("%lld")