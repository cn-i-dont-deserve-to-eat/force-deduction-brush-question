#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int smallestValue(int n) {
        int k = n;
        int res = 1e9;
        while (1) {

            int ans = 0;
            for (int i = 2; i <= k / i; i++) {
                if (k % i == 0) {
                    int cnt = 0;
                    while (k % i == 0) {
                        k /= i;
                        cnt++;
                    }
                    ans += cnt * i;
                }
            }
            if (k > 1)ans += k;
            if (ans >= res)return res;
            k = ans;
            res = ans;
        }
        return res;
    }
};