#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int similarPairs(vector<string>& words) {
        int len = words.size();
        vector<int> q(len + 1);

        for (int i = 0; i < len; i++) {
            int k = 0;
            for (int j = 0; j < words[i].size(); j++) {
                int u = words[i][j] - 'a';
                k |= (1 << u);
            }
            q[i] = k;
        }
        int ans = 0;
        for (int i = 0; i < len; i++) {
            for (int j = i + 1; j < len; j++) {
                if (q[i] == q[j])ans++;
            }
        }
        return ans;



    }
};