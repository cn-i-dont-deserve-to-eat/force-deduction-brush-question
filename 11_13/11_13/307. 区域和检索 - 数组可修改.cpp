#define _CRT_SECURE_NO_WARNINGS 1
int tr[30010];
int n;
int a[30010];
class NumArray {
public:
    int lowbit(int x) {
        return x & -x;
    }

    void add(int x, int c, int n) {
        for (int i = x; i <= n; i += lowbit(i)) {
            tr[i] += c;
        }
    }
    int sum(int x) {
        int res = 0;
        for (int i = x; i; i -= lowbit(i)) {
            res += tr[i];
        }
        return res;
    }
    NumArray(vector<int>& nums) {
        // for(int i)
        n = nums.size();

        for (int i = 0; i <= n; i++)tr[i] = 0;
        for (int i = 1; i <= n; i++)add(i, nums[i - 1], n), a[i] = nums[i - 1];
    }

    void update(int index, int val) {
        add(index + 1, val - a[index + 1], n);
        a[index + 1] = val;
    }

    int sumRange(int left, int right) {
        return sum(right + 1) - sum(left);
    }
};

