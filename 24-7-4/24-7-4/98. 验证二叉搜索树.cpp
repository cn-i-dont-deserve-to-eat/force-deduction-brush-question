#define _CRT_SECURE_NO_WARNINGS 1
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

vector<int> ans;
class Solution {
public:

    void isfun(TreeNode* root) {
        if (root == nullptr)return;
        isfun(root->left);
        ans.push_back(root->val);
        isfun(root->right);
    }
    bool isValidBST(TreeNode* root) {
        ans = {};
        isfun(root);
        for (int i = 1; i < ans.size(); i++) {
            if (ans[i] <= ans[i - 1])return false;
        }
        return true;
    }
};


vector<int> ans;
class Solution {
public:

    bool isfun(TreeNode* root, long long l, long long r) {
        if (root == nullptr)return true;
        if (root->val <= l || root->val >= r)return false;
        return isfun(root->left, l, root->val) && isfun(root->right, root->val, r);
    }
    bool isValidBST(TreeNode* root) {
        return isfun(root, -3e9, 3e9);
    }
};