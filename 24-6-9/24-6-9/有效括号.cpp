#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool isValid(string s) {
        stack<char> st;
        int n = s.size();
        for (int i = 0; i < n; i++) {
            if (st.empty() || s[i] == '(' || s[i] == '{' || s[i] == '[') {
                st.push(s[i]);
            }
            else {
                if (st.top() == '(' && s[i] == ')')st.pop();
                else if (st.top() == '{' && s[i] == '}')st.pop();
                else if (st.top() == '[' && s[i] == ']')st.pop();
                else return false;
            }
        }
        if (st.empty())return true;
        return false;

    }
};