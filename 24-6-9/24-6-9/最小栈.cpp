#define _CRT_SECURE_NO_WARNINGS 1
class MinStack {
public:
    MinStack() {

    }

    void push(int val) {
        s.push(val);
        if (smin.empty() || smin.top() >= val)smin.push(val);
    }

    void pop() {
        int k = s.top();
        s.pop();
        if (!smin.empty() && k == smin.top()) {
            smin.pop();
        }
    }

    int top() {
        return s.top();
    }

    int getMin() {
        return smin.top();
    }
    stack<int> s;
    stack<int> smin;
};

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack* obj = new MinStack();
 * obj->push(val);
 * obj->pop();
 * int param_3 = obj->top();
 * int param_4 = obj->getMin();
 */