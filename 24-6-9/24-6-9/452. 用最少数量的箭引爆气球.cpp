#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int findMinArrowShots(vector<vector<int>>& points) {
        int n = points.size();
        sort(points.begin(), points.end());
        int ans = 1;
        pair<int, int> p = { points[0][0],points[0][1] };
        for (int i = 1; i < n; i++) {
            if (points[i][0] <= p.second) {
                p.first = points[i][0];
                p.second = min(points[i][1], p.second);
            }
            else {
                ans++;
                p = { points[i][0],points[i][1] };
            }
        }
        return ans;
    }
};