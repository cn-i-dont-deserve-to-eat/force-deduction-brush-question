#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    string simplifyPath(string path) {
        stack<string> st;
        string t = "";
        int n = path.size();
        for (int i = 1; i < n; i++) {
            if (path[i] == '/') {
                if (t.size() == 0)continue;
                if (t == "..") {
                    if (!st.empty())st.pop();
                }
                else if (t != ".")
                {
                    st.push(t);
                }
                t = "";
            }
            else {
                t += path[i];
            }
        }
        if (path[n - 1] != '/') {
            if (t == "..") {
                if (!st.empty())st.pop();
            }
            else if (t != "." && t != "")
            {
                st.push(t);
            }
            t = "";
        }

        string ans = "/";
        vector<string> str;
        while (!st.empty()) {
            str.push_back(st.top());
            st.pop();
        }
        for (int i = str.size() - 1; i >= 0; i--) {
            ans += str[i];
            if (i != 0)ans += '/';
        }
        return ans;
    }
};