#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
using namespace std;

long long qmi(long long a, long long b, long long p) {
	long long res = 1;
	while (b) {
		if (b & 1)res = res * a % p;
		a = a * a % p;
		b >>= 1;
	}
	return res;
}

int main() {
	long long  a, b, n;
	cin >> a >> b >> n;
	long long p = 1000 * b;
	long long c=a* qmi(10, n + 2, p)%p / b;

	printf("%03d\n", c);
	/*long long a, b, n;
	scanf("%lld%lld%lld", &a, &b, &n);
	long long mod = b * 1000;
	long long tmp = qmi(10, n + 2, mod);
	long long sum = a * tmp % mod / b;
	printf("%03lld\n", sum);*/
	return 0;
}