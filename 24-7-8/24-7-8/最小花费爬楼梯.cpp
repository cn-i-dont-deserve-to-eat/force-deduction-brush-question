#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<cstring>
using namespace std;
const int N = 1e5 + 10;
int f1[N];
int f2[N];
int a[N];
int main() {
    int n;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> a[i];
    }
    memset(f1, 0x3f, sizeof f1);
    memset(f2, 0x3f, sizeof f2);
    f1[0] = 0;
    f1[1] = a[0];
    for (int i = 2; i <= n; i++) {
        f1[i] = min(f1[i - 1] + a[i - 1], f1[i - 2] + a[i - 2]);
    }

    f2[1] = 0;
    f2[2] = a[1];
    for (int i = 3; i <= n; i++) {
        f2[i] = min(f2[i - 1] + a[i - 1], f2[i - 2] + a[i - 2]);
    }

    cout << min(f1[n], f2[n]);
    return 0;
}
