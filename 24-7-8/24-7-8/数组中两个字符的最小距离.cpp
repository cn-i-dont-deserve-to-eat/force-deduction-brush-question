#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<algorithm>
#include <linux/limits.h>
#include<string>
using namespace std;

int main() {
    int n;
    cin >> n;
    string str1, str2;
    cin >> str1 >> str2;
    int p1 = -1;
    int p2 = -1;
    string res;
    int ans = 1e9;
    for (int i = 0; i < n; i++) {
        cin >> res;
        if (res == str1) {
            p1 = i;
        }
        else if (res == str2) {
            p2 = i;
        }
        if (p1 != -1 && p2 != -1) {
            ans = min(ans, abs(p1 - p2));
        }
    }
    if (p1 == -1 || p2 == -1)cout << -1 << endl;
    else cout << ans << endl;
    return 0;
}
