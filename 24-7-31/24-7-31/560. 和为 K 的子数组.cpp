#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int subarraySum(vector<int>& nums, int k) {
        int sum = 0;
        int ans = 0;
        int n = nums.size();
        unordered_map<int, int> mp;
        vector<int> s(n + 1);
        s[0] = nums[0];
        for (int i = 1; i < n; i++)
        {
            s[i] = s[i - 1] + nums[i];
        }
        mp[0]++;
        for (int i = 0, j = 0; i < n; i++)
        {
            int target = s[i] - k;
            ans += mp[target];
            mp[s[i]]++;
        }
        return ans;
    }
};