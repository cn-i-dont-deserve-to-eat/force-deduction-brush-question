#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool iscmp(unordered_map<int, int>& cnt, unordered_map<int, int>& mp) {
        for (int i = 0; i < 26; i++) {
            if (cnt[i] != mp[i])
                return false;
        }
        return true;
    }
    vector<int> findAnagrams(string s, string p) {
        int n = s.size();
        int m = p.size();
        int k = 0;
        unordered_map<int, int> cnt;
        for (int i = 0; i < m; i++) {
            int u = p[i] - 'a';
            cnt[u]++;
            if (cnt[u] == 1)
                k++;
        }
        vector<int> ans;
        unordered_map<int, int> mp;

        int kk = 0;
        for (int i = 0, j = 0; i < n; i++) {
            int u = s[i] - 'a';
            mp[u]++;
            if (i - j + 1 == m) {
                if (iscmp(cnt, mp)) {
                    ans.push_back(j);
                }
                int uu = s[j] - 'a';
                mp[uu]--;
                j++;
            }
        }
        return ans;
    }
};