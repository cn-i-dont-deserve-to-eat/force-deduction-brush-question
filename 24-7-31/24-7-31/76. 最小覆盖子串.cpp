class Solution {
public:
    string minWindow(string s, string t) {
        int n = s.size();
        int m = t.size();
        unordered_map<char, int> mp1;
        unordered_map<char, int> mp2;

        int k = 0;
        for (int i = 0; i < m; i++)
        {
            mp2[t[i]]++;
            if (mp2[t[i]] == 1)k++;
        }
        int cnt = 0;
        int ans = 1e9;
        string str;
        int start = 0;
        for (int i = 0, j = 0; i < n; i++)
        {
            char u = s[i];
            mp1[u]++;
            if (mp2[u] && mp1[u] == mp2[u])
            {
                cnt++;
            }
            while (j <= i && cnt >= k)
            {
                if (ans > i - j + 1)
                {
                    ans = i - j + 1;
                    start = j;
                }
                char uu = s[j];
                mp1[uu]--;
                if (mp1[uu] == mp2[uu] - 1)cnt--;
                j++;
            }
        }
        // ABCBAC
        if (ans == 1e9)return "";
        return s.substr(start, ans);
    }
};