#define _CRT_SECURE_NO_WARNINGS 1

class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        if (head == NULL)return NULL;
        ListNode* pre = NULL;
        ListNode* cur = head;
        ListNode* last = head->next;

        while (cur != NULL) {
            cur->next = pre;
            pre = cur;
            cur = last;
            if (last)last = last->next;
        }
        return pre;
    }
};  