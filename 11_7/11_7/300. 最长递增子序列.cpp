#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int lengthOfLIS(vector<int>& nums) {
        int len = nums.size();
        vector<int> dp(len + 1, 1);
        dp[0] = 1;
        for (int i = 1; i < len; i++) {

            //dp[i]=dp[i-1];
            for (int j = 0; j < i; j++) {
                if (nums[i] > nums[j]) {
                    dp[i] = max(dp[j] + 1, dp[i]);
                }
            }

        }
        int ans = 0;
        for (int i = 0; i < len; i++) {
            ans = max(ans, dp[i]);
        }
        return ans;
    }
};