#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) {
        int s = 1;
        int s1 = 1;
        int cnt = 0;
        int pos = 0;
        for (int i = 0; i < nums.size(); i++) {
            if (nums[i] == 0) {
                cnt++;
                pos = i;
                continue;
            }
            s = s * nums[i];
        }
        int len = nums.size();
        vector<int> ans(len, 0);
        if (cnt >= 2) {
            return ans;
        }
        if (cnt == 1) {
            ans[pos] = s;
            return ans;
        }
        for (int i = 0; i < len; i++) {
            ans[i] = s / nums[i];
        }
        return ans;
    }
};