#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    string longestCommonPrefix(vector<string>& strs) {
        int len = strs.size();
        int minl = 1e9;
        for (int i = 0; i < len; i++) {
            if (strs[i].size() < minl) {
                minl = strs[i].size();
            }
        }
        for (int i = 1; i <= minl; i++) {
            char u = strs[0][i - 1];
            int f = 0;
            for (int j = 0; j < len; j++) {
                if (strs[j][i - 1] != u) {
                    return strs[0].substr(0, i - 1);
                }
            }
            if (i == minl) {
                return strs[0].substr(0, i);
            }
        }
        return "";

    }
};