#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int jump(vector<int>& nums) {
        int len = nums.size();
        if (len == 1)return 0;
        int ans = 0;
        for (int i = 0; i < nums.size();) {
            int ma = 0;
            int pos = 0;
            for (int j = 1; j <= nums[i]; j++) {
                if (i + j >= len - 1) {
                    pos = i + j;
                    break;
                    //break;
                }
                if (i + j + nums[i + j] > ma) {
                    ma = nums[i + j] + i + j;
                    pos = i + j;
                }
            }
            //  cout<<pos<<" ";
            ans++;
            i = pos;
            if (i >= nums.size() - 1) {
                return ans;
            }
        }
        return ans;
    }
};