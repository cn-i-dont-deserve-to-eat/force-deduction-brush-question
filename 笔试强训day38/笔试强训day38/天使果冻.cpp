#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<unordered_map>
#include<vector>
#include<algorithm>
using namespace std;
const int N = 1e5 + 10;
int a[2 * N];
int b[N];

int main() {
    int n;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> a[i];
    }

    int max1 = 0;
    int max2 = 0;
    for (int i = 0; i < n; i++) {
        if (max1 < a[i]) {
            max2 = max1;
            max1 = a[i];
        }
        else {
            max2 = max(max2, a[i]);
        }
        b[i] = max2;
    }

    int q;
    cin >> q;
    while (q--) {
        int x;
        cin >> x;
        cout << b[x - 1] << endl;
    }
    return 0;
}
