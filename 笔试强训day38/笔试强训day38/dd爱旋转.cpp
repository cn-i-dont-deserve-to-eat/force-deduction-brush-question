#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<cstring>
using namespace std;
const int N = 1100;
int g[N][N];
int g1[N][N];

int main() {
    int n;
    cin >> n;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            cin >> g[i][j];
        }
    }
    int q;
    cin >> q;
    int a = 0;
    int b = 0;
    while (q--) {
        int x;
        cin >> x;
        if (x == 1)a++;
        else b++;
    }
    a %= 2;
    b %= 2;

    if (a) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                g1[i][j] = g[n - i + 1][n - j + 1];
            }
        }

        memcpy(g, g1, sizeof g1);
    }
    if (b) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                g1[i][j] = g[n - i + 1][j];
            }
        }
        memcpy(g, g1, sizeof g1);
    }

    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            cout << g[i][j] << " ";
        }
        cout << endl;
    }
    return 0;
}
