
bool iscmp(struct TreeNode* p, struct TreeNode* q) {
    if (p && q) {
        if (p->val == q->val) {
            return iscmp(p->left, q->left) && iscmp(q->right, p->right);
        }
        else {
            return false;
        }
    }
    else if (!p && !q) {
        return true;
    }
    else {
        return false;
    }
}

bool isSameTree(struct TreeNode* p, struct TreeNode* q) {
    return iscmp(p, q);
}#define _CRT_SECURE_NO_WARNINGS 1