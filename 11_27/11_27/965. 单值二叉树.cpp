#define _CRT_SECURE_NO_WARNINGS 1
int deep(struct TreeNode* root) {
    if (root == NULL)return 0;
    return fmax(deep(root->left), deep(root->right)) + 1;
}

int maxDepth(struct TreeNode* root) {
    return deep(root);
} 