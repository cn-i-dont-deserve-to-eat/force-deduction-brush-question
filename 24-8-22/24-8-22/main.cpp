#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;


void mergesort(int* a, int l, int r, int* temp) {
	if (l >=r)return;
	int mid = (l + r) >> 1;
	mergesort(a, l, mid, temp);
	mergesort(a, mid + 1,r, temp);

	int start1 = l;
	int start2 = mid + 1;
	int pos = l;
	while (start1 <= mid && start2 <= r) {
		if (a[start1] < a[start2])temp[pos++] = a[start1++];
		else {
			temp[pos++] = a[start2++];
		}
	}
	while(start1<=mid)temp[pos++] = a[start1++];
	while(start2<=r)temp[pos++] = a[start2++];

	memcpy(a + l, temp + l, sizeof(int)*(r-l+1));
}


void downjust(int* a, int n, int parent) {
	int child = parent * 2 + 1;
	while (child < n) {
		if (child + 1 < n && a[child] > a[child + 1])child++;
		if (a[child] < a[parent]) {
			swap(a[child], a[parent]);
			parent = child;
			child = 2 * child + 1;
		}
		else break;
	}
}

void headsort(int* a,int n) {
	int i = 0;
	//����
	for (int i = (n - 1 - 1) / 2; i >= 0; i--) {
		downjust(a, n, i);
	}

	//������
	for (int i = 0; i < n; i++) {
		swap(a[0], a[n - i - 1]);
		downjust(a, n-i-1, 0);
	}

}


void quitsort(int* a, int l, int r) {
	if (l >= r)return;
	int mid = (l + r) >> 1;
	int k = a[mid];
	int ll = l - 1;
	int rr = r + 1;
	while (ll < rr) {
		do ll++; while (a[ll] < k);
		do rr--; while (a[rr] > k);
		if (ll < rr)swap(a[ll], a[rr]);
	}
	quitsort(a, l, rr);
	quitsort(a, rr+1, r);
}

int main() {
	int a[10] = { 7,8,9,3,4,2,5,6,1,0 };
	int temp[10] = { 0 };
	//mergesort(a, 0, 9, temp);
	//headsort(a, 10);
	quitsort(a, 0, 9);
	for (int i = 0; i < 10; i++)cout << a[i] << " ";
	

	return 0;
}