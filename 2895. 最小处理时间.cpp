#define _CRT_SECURE_NO_WARNINGS 1
class Solution {

public:
    int minProcessingTime(vector<int>& processorTime, vector<int>& tasks) {
        sort(processorTime.begin(), processorTime.end());
        sort(tasks.begin(), tasks.end());
        int len = processorTime.size() - 1;
        int mx = 0;
        int cnt = 0;
        int res = 0;
        for (int i = 0; i < tasks.size(); i++) {
            res = max(res, tasks[i]);
            cnt++;
            if (cnt == 4) {
                cnt = 0;
                int k = i / 4;
                mx = max(res + processorTime[len - k], mx);
                res = 0;
            }

        }
        return mx;
    }
};