#define _CRT_SECURE_NO_WARNINGS 1
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

int ans = -1e9;
class Solution {
public:

    int getmax(TreeNode* root) {
        if (root == nullptr)return 0;
        int l = max(0, getmax(root->left));
        int r = max(0, getmax(root->right));
        ans = max(ans, root->val + l + r);
        return root->val + max(l, r);
    }
    int maxPathSum(TreeNode* root) {
        ans = -1e9;
        getmax(root);
        return ans;
    }
};