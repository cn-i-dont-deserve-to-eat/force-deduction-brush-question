#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool isPrefixAndSuffix(string& a, string& b) {
        if (a.size() > b.size())return false;

        int n = a.size();
        int len = b.size();
        string t = b.substr(0, n);
        if (a == t) {
            if (a == b.substr(len - n, n))return true;
        }
        return false;
    }

    int countPrefixSuffixPairs(vector<string>& words) {
        int n = words.size();
        int ans = 0;
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (isPrefixAndSuffix(words[i], words[j]))ans++;
            }
        }
        return ans;
    }
};