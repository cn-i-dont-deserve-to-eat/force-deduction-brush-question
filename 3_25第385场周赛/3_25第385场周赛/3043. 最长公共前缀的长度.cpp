#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int longestCommonPrefix(vector<int>& arr1, vector<int>& arr2) {
        unordered_map<string, int> mp1;
        int n = arr1.size();
        int m = arr2.size();
        for (int i = 0; i < n; i++) {
            string res = to_string(arr1[i]);
            string t = "";
            for (int j = 0; j < res.size(); j++) {
                t += res[j];
                mp1[t]++;
            }
        }
        int ans = 0;
        for (int i = 0; i < m; i++) {
            string res = to_string(arr2[i]);
            string t = "";
            for (int j = 0; j < res.size(); j++) {
                t += res[j];
                if (mp1.count(t)) {
                    ans = max(ans, (int)t.size());
                }
            }
        }
        return ans;
    }
};