#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int splitNum(int num) {
        vector<int> arr;//存放每一位数
        int a = 0;
        int b = 0;
        while (num) {
            int k = num % 10;
            arr.push_back(k);
            num /= 10;
        }

        sort(arr.begin(), arr.end());//排序
        for (int i = 0; i < arr.size(); i++) {
            if (i % 2 == 0) {
                a = a * 10 + arr[i];
            }
            else {
                b = b * 10 + arr[i];
            }
        }
        return a + b;
    }
};