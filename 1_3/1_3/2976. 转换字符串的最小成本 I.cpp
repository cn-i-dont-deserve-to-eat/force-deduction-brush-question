#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    long long minimumCost(string source, string target, vector<char>& original, vector<char>& changed, vector<int>& cost) {
        int dist[26][26];
        memset(dist, 0x3f, sizeof dist);
        for (int i = 0; i < 26; i++)dist[i][i] = 0;
        for (int i = 0; i < cost.size(); i++) {
            int x = original[i] - 'a';
            int y = changed[i] - 'a';
            dist[x][y] = min(dist[x][y], cost[i]);
        }

        for (int k = 0; k < 26; k++) {
            for (int i = 0; i < 26; i++) {
                for (int j = 0; j < 26; j++) {
                    dist[i][j] = min(dist[i][j], dist[i][k] + dist[k][j]);
                }
            }
        }
        long long ans = 0;
        for (int i = 0; i < source.size(); i++) {
            int a = source[i] - 'a';
            int b = target[i] - 'a';
            if (dist[a][b] == 0x3f3f3f3f) {
                return -1;
            }
            ans += dist[a][b];
        }
        return ans;
    }
};