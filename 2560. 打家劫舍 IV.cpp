#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int minCapability(vector<int>& nums, int k) {
        int mi = 1e9;//左边界
        int mx = 0;//右边界
        for (auto it : nums) {
            mi = min(mi, it);
            mx = max(mx, it);
        }
        int len = nums.size();
        int l = mi - 1;
        int r = mx + 1;
        while (l + 1 != r) {
            int mid = l + r >> 1;
            int num = 0;
            int pos = -1;//记录上一个选入方案的下标
            for (int i = 0; i < len; i++) {
                if (nums[i] <= mid && (pos == -1 || i - pos > 1)) {//选入方案的数必须小于或者等于mid,而且不与pos相邻
                    num++;                                       //第一次选要特判一下
                    pos = i;
                }
            }
            if (num >= k)r = mid;//符合答案的放右边
            else l = mid;
        }
        return r;//此时的r表示所有合格答案的左边界
    }
};