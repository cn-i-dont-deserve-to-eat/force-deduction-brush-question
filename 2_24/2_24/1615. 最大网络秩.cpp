#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int maximalNetworkRank(int n, vector<vector<int>>& roads) {
        int m = roads.size();
        vector<vector<int>> g(n, vector<int>(n));
        vector<int> d(n);
        for (auto it : roads) {
            int a = it[0];
            int b = it[1];
            g[a][b] = g[b][a] = 1;
            d[a]++;
            d[b]++;
            //    g[a].push_back(b);
            //    g[b].push_back(a);
        }
        int ans = 0;
        //    for(int i=1;i<=n;i++){
        //       for(int j=0;j<g[i].size();j++){
        //           ans=max(ans,(int)(g[i].size()+g[g[i][j]].size()-1));
        //       }
        //    }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j)continue;
                if (g[i][j] || g[j][i]) {
                    ans = max(ans, d[i] + d[j] - 1);
                }
                else {
                    ans = max(ans, d[i] + d[j]);
                }
            }
        }
        return ans;
    }
};