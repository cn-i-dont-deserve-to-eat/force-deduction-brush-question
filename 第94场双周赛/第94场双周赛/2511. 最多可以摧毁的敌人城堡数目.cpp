#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int captureForts(vector<int>& forts) {
        int n = forts.size();
        int ans = 0;
        int t = -1;
        int res = 0;
        for (int i = 0; i < n; i++) {

            res += forts[i];
            if (res == 0) {
                if (forts[i] != 0) {
                    // cout<<t<<endl;
                    ans = max(ans, i - t - 1);
                    res = forts[i];
                }
            }
            if (forts[i] != 0) {
                t = i;
                res = forts[i];
            }
        }
        return ans;
    }
};