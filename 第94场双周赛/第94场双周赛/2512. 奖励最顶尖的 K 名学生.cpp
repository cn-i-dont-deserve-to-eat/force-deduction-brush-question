#define _CRT_SECURE_NO_WARNINGS 1
typedef pair<int, int> PII;
bool cmp(PII& A, PII& B) {
    if (A.first == B.first)return A.second < B.second;
    return A.first > B.first;
}
class Solution {
public:
    vector<int> topStudents(vector<string>& positive_feedback, vector<string>& negative_feedback, vector<string>& report, vector<int>& student_id, int k) {
        unordered_map<string, int> mp1;
        unordered_map<string, int> mp2;

        for (auto it : positive_feedback) {
            mp1[it]++;
        }
        for (auto it : negative_feedback) {
            mp2[it]++;
        }


        int n = report.size();
        vector<PII> ans;

        for (int i = 0; i < report.size(); i++) {
            int res = 0;
            for (int r = 0, l = 0; r < report[i].size(); r++) {
                if (report[i][r] == ' ' || r == report[i].size() - 1) {
                    string t = "";
                    if (r == report[i].size() - 1)t = report[i].substr(l, r - l + 1);
                    else t = report[i].substr(l, r - l);
                    // cout<<t<<" ";
                    if (mp1.count(t))res += 3;
                    if (mp2.count(t))res -= 1;
                    l = r + 1;
                }
            }
            ans.push_back({ res,student_id[i] });
        }
        cout << ans[k - 1].second << endl;
        sort(ans.begin(), ans.end(), cmp);
        vector<int> ans2(k);
        for (int i = 0; i < k; i++) {
            ans2[i] = ans[i].second;
        }
        return ans2;
    }
};