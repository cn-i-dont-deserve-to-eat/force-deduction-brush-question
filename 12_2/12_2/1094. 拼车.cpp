#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool carPooling(vector<vector<int>>& trips, int capacity) {
        vector<int> s(1010, 0);
        int mas = 0;
        for (auto it : trips) {
            s[it[1]] += it[0];
            s[it[2]] -= it[0];
            mas = max(mas, it[2]);
        }
        if (s[0] > capacity)return false;
        for (int i = 1; i <= mas; i++) {
            s[i] += s[i - 1];
            if (s[i] > capacity) {
                // cout<<i<<endl;
                return false;
            }
        }
        return true;
    }
};