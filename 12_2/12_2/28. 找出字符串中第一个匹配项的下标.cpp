#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int strStr(string haystack, string needle) {
        return haystack.find(needle);
    }
};