#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool isSubsequence(string s, string t) {
        int cs = 0;
        int ct = 0;
        int len = s.size();
        int num = s.size();
        for (int i = 0; i < t.size(); i++) {
            if (cs < len && t[i] == s[cs]) {
                num--;
                cs++;
            }
        }
        return num == 0;
    }
};