#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:

    bool isPalindrome(string s) {
        int l = 0;
        int r = s.size() - 1;
        while (l < r) {
            while (l < r && !(isalpha(s[l]) || isdigit(s[l])))l++;
            while (r > l && !(isalpha(s[r]) || isdigit(s[r])))r--;
            if (isalpha(s[r]))s[r] = tolower(s[r]);
            if (isalpha(s[l]))s[l] = tolower(s[l]);
            if (s[l] != s[r])return false;
            //if(tolower(s[l])==tolower(s[r]))cout<<s[l]<<endl;
            l++;
            r--;
        }
        return true;

    }
};