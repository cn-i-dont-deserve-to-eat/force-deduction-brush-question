#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {
        int len = nums.size();
        sort(nums.begin(), nums.end());
        vector<vector<int>>ans;
        for (int f = 0; f < len; f++) {
            if (f > 0 && nums[f] == nums[f - 1]) {
                continue;
            }
            int target = -nums[f];
            int t = len - 1;
            for (int s = f + 1; s <= t; s++) {
                if (s > f + 1 && nums[s] == nums[s - 1]) {
                    continue;
                }
                while (s<t && nums[s] + nums[t]>target) {
                    t--;
                }
                if (s == t) {
                    break;
                }
                if (nums[s] + nums[t] == target) {
                    ans.push_back({ nums[f],nums[s],nums[t] });
                }

            }
        }
        return ans;
    }
};