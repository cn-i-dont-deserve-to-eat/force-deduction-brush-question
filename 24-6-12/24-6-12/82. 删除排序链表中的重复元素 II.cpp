#define _CRT_SECURE_NO_WARNINGS 1

class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
        if (head == nullptr || head->next == nullptr)return head;
        ListNode* hh = new ListNode(-110);
        hh->next = head;
        ListNode* cur = head;
        ListNode* pre = hh;
        while (cur) {
            int v = cur->val;
            if (cur->next && cur->next->val == v) {
                while (cur && cur->val == v) {
                    ListNode* t = cur->next;
                    delete cur;
                    pre->next = t;
                    cur = t;
                }
            }
            else {
                pre = cur;
                cur = cur->next;
            }
        }
        return hh->next;
    }
};