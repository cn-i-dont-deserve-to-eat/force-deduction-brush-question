#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

int main() {
    int n, m, a, b;
    cin >> n >> m >> a >> b;
    long long ans = 0;
    for (int i = 0; i <= n && i * 2 <= m; i++) {
        long long t1 = (long long)i * b;
        long long t2 = (long long)min((n - i) / 2, m - 2 * i) * a;
        ans = max(t1 + t2, ans);
    }
    cout << ans << endl;
    return 0;
}
// x=(2a-b)/3
// y=a-2x