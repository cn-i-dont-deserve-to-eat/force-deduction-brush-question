#define _CRT_SECURE_NO_WARNINGS 1
#include <algorithm>
#include <iostream>
#include<string>
#include<cstring>
using namespace std;

int main() {
    string str;
    getline(cin, str);
    reverse(str.begin(), str.end());
    int n = str.size();
    string ans = "";
    //string res;
    for (int i = 1, j = 0; i < n; i++)
    {
        if ((str[i] == ' ' && str[i - 1] != ' ') || (i == n - 1)) {
            string res;
            if (i != n - 1) {
                res = str.substr(j, i - j);
            }
            else res = str.substr(j, i - j + 1);
            reverse(res.begin(), res.end());
            ans += res;
            if (i != n - 1)ans += " ";
            j = i + 1;
        }
    }
    cout << ans << endl;
    return 0;
}
