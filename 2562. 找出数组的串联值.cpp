#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
    int get_p(int x) {
        int res = 0;
        while (x) {
            x /= 10;
            res++;
        }
        return res;
    }

public:
    long long findTheArrayConcVal(vector<int>& nums) {
        int l = 0, r = nums.size() - 1;
        long long ans = 0;
        while (l < r) {
            int k = get_p(nums[r]);
            // cout<<k<<"mm"<<endl;
            ans += (long long)nums[l] * pow(10, k) + nums[r];
            //cout<<nums[l]*pow(10,k)+nums[l]<<endl;
            l++;
            r--;
        }
        if (l == r) {
            ans += nums[l];
        }
        return ans;
    }
};