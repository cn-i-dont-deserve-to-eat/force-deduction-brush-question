class Solution {
public:
    int minOperations(string s1, string s2, int x) {
        vector<int> p;
        for (int i = 0; i < s1.size(); i++) {
            if (s1[i] != s2[i])p.push_back(i);
        }
        if (p.size() % 2)return -1;
        if (p.size() == 0)return 0;
        //int dp[510]={0};
      //  for(int i=0;i<510;i++)dp[i]=-1;
        int f0 = 0, f1 = x;
        for (int i = 1; i < p.size(); i++) {
            int dp = min(f1 + x, f0 + (p[i] - p[i - 1]) * 2);
            f0 = f1;
            f1 = dp;
        }

        return f1 / 2;

    }
};