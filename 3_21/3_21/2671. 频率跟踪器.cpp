#define _CRT_SECURE_NO_WARNINGS 1
class FrequencyTracker {
public:
    FrequencyTracker() {
        mp1.clear();
        mp2.clear();
    }

    void add(int number) {
        if (mp2[mp1[number]])mp2[mp1[number]]--;
        mp1[number]++;
        mp2[mp1[number]]++;
    }

    void deleteOne(int number) {
        if (mp1[number]) {
            mp2[mp1[number]]--;
            mp1[number]--;
            mp2[mp1[number]]++;
        }
    }

    bool hasFrequency(int frequency) {
        //cout<<mp2.count(frequency)<<endl;
       // cout<<mp2[frequency]<<endl;
        if (mp2[frequency])return true;
        return false;
    }

private:
    unordered_map<int, int> mp1;
    unordered_map<int, int> mp2;

};

/**
 * Your FrequencyTracker object will be instantiated and called as such:
 * FrequencyTracker* obj = new FrequencyTracker();
 * obj->add(number);
 * obj->deleteOne(number);
 * bool param_3 = obj->hasFrequency(frequency);
 */