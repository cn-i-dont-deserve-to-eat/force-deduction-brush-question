#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    long long maximumHappinessSum(vector<int>& happiness, int k) {

        sort(happiness.begin(), happiness.end());
        int res = 0;
        long long ans = 0;
        for (int i = happiness.size() - 1; i >= 0 && k; i--) {
            if (happiness[i] - res < 0) {
                ans += 0;
            }
            else {
                ans += (long long)happiness[i] - res;
            }
            k--;
            res++;
        }
        return ans;
    }
};