#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int minimumBoxes(vector<int>& apple, vector<int>& capacity) {
        int sum = 0;
        for (auto it : apple) {
            sum += it;
        }

        sort(capacity.begin(), capacity.end());
        int ans = 0;
        for (int i = capacity.size() - 1; i >= 0; i--) {
            if (sum - capacity[i] > 0) {
                sum -= capacity[i];
                ans++;
            }
            else {
                ans++;
                break;
            }
        }
        return ans;
    }
};