class Solution {
public:
     int h[110],ne[10000],e[10000],idx;
     int d[110];
     int q[110];
     bool f[110][110];
     int hh=0,tt=-1;
     void add(int a,int b){
         e[idx]=b,ne[idx]=h[a],h[a]=idx++;
     }


    void topu(int numCourses){
       while(hh<=tt){
           int t=q[hh++];

           for(int i=h[t];i!=-1;i=ne[i]){
               int j=e[i];

               f[t][j]=true;
               for(int h=0;h<numCourses;h++){
                   if(f[h][t]==true)f[h][j]=true;
               }
               d[j]--;
               if(d[j]==0){
                   q[++tt]=j;
               }
           }
       }
    }
    vector<bool> checkIfPrerequisite(int numCourses, vector<vector<int>>& prerequisites, vector<vector<int>>& queries) {

       memset(h,-1,sizeof h);
       for(int i=0;i<numCourses;i++)d[i]=0;
       for(int i=0;i<10000;i++){
           ne[i]=0;
           e[i]=0;
       }
        for(auto it:prerequisites){
            int x=it[0];
            int y=it[1];
            add(x,y);
            d[y]++;
        }
        
        for(int i=0;i<numCourses;i++){
            if(d[i]==0){
                q[++tt]=i;
            }
        }
        topu(numCourses);
        vector<bool> ans;
       for(auto it:queries){
           int x=it[0];
           int y=it[1];
           
          ans.push_back(f[x][y]);
       }
        return ans;
    }
};
