#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<string> splitWordsBySeparator(vector<string>& words, char separator) {
        int len = words.size();
        vector<string> ans;
        for (int i = 0; i < len; i++) {
            int n = words[i].size();
            if (n < 1)continue;
            int j, a;
            for (j = 0, a = 0; j < n; j++) {
                if (words[i][j] == separator) {
                    if (j != a)ans.push_back(words[i].substr(a, j - a));
                    a = j + 1;
                }
            }
            if (words[i][n - 1] != separator)ans.push_back(words[i].substr(a, j - a));
        }
        return ans;
    }
};