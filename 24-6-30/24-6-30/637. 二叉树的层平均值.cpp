#define _CRT_SECURE_NO_WARNINGS 1

class Solution {
public:
    vector<int> rightSideView(TreeNode* root) {
        if (root == nullptr)return {};
        queue<TreeNode*> q;

        q.push(root);
        vector<int> ans;

        while (!q.empty()) {
            int sz = q.size();
            for (int i = 0; i < sz; i++) {
                TreeNode* t = q.front();
                q.pop();

                if (t->left)q.push(t->left);
                if (t->right)q.push(t->right);
                if (i == sz - 1)ans.push_back(t->val);
            }
        }
        return ans;
    }
};