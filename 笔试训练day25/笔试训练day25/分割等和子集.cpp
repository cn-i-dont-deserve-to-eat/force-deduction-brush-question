#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<vector>
using namespace std;
const int N = 5e4;
bool f[N + 10];

int main() {
    int n;
    cin >> n;
    int s = 0;
    vector<int> a(n + 1);
    for (int i = 1; i <= n; i++) {
        cin >> a[i];
        s += a[i];
    }
    if (s % 2) {
        cout << "false" << endl;
        return 0;
    }
    f[0] = true;
    for (int i = 1; i <= n; i++) {
        for (int j = a[i]; j <= N; j++) {
            f[j] |= f[j - a[i]];
        }
    }
    if (f[s / 2])cout << "true" << endl;
    else cout << "false" << endl;

}