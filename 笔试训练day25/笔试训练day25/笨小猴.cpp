#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<string>
#include<unordered_map>
using namespace std;


bool is_prim(int x) {
    if (x < 2)return false;
    for (int i = 2; i <= x / i; i++) {
        if (x % i == 0)return false;
    }
    return true;
}

int main() {
    string str;
    cin >> str;
    int maxn = 0;
    int minn = 1e9;
    unordered_map<char, int> mp;
    for (int i = 0; i < str.size(); i++) {
        mp[str[i]]++;
    }
    for (auto& it : mp) {
        maxn = max(it.second, maxn);
        minn = min(it.second, minn);
    }
    if (is_prim(maxn - minn)) {
        cout << "Lucky Word" << endl;
        cout << maxn - minn << endl;
    }
    else {
        cout << "No Answer" << endl;
        cout << 0 << endl;
    }




    return 0;
}
