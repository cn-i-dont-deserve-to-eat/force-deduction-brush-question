#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool hostschedule(vector<vector<int> >& schedule) {
        sort(schedule.begin(), schedule.end());
        int n = schedule.size();

        for (int i = 1; i < n; i++) {
            if (schedule[i][0] < schedule[i - 1][1]) {
                return false;
            }
        }
        return true;
    }
};