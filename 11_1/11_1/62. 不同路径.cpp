#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int uniquePaths(int m, int n) {
        vector<vector<int>> f(m + 1, vector<int>(n + 1, 0));
        for (int i = 1; i <= n; i++) {
            f[1][i] = 1;
        }
        for (int i = 1; i <= m; i++) {
            f[i][1] = 1;
        }
        //f[1][1]=0;

        for (int i = 2; i <= m; i++) {
            for (int j = 2; j <= n; j++) {
                f[i][j] = f[i - 1][j] + f[i][j - 1];
            }
        }
        return f[m][n];

    }
};