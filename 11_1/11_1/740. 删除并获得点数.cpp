#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int deleteAndEarn(vector<int>& nums) {
        int len = nums.size();
        int max_v = 0;
        for (int i = 0; i < len; i++) {
            if (nums[i] > max_v) {
                max_v = nums[i];
            }
        }
        vector<int> sum(max_v + 10, 0);
        for (int i = 0; i < len; i++) {
            sum[nums[i]] += nums[i];
        }
        vector<int> f(max_v + 1, 0);

        f[0] = sum[0];
        f[1] = max(sum[1], sum[0]);
        for (int i = 2; i <= max_v; i++) {
            f[i] = max(f[i - 1], f[i - 2] + sum[i]);
        }
        return f[max_v];
    }
};