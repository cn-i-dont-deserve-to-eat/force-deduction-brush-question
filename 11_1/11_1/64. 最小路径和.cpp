#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int minPathSum(vector<vector<int>>& grid) {
        if (grid.size() == 0 || grid[0].size() == 0) {
            return 0;
        }
        int m = grid.size();
        int n = grid[0].size();

        vector<vector<int>> f(m + 10, vector<int>(n + 10, 0));
        f[0][0] = grid[0][0];
        for (int i = 1; i < n; i++) {
            f[0][i] = grid[0][i] + f[0][i - 1];
        }

        for (int i = 1; i < m; i++) {
            f[i][0] = grid[i][0] + f[i - 1][0];
        }

        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                f[i][j] = min(f[i - 1][j] + grid[i][j], f[i][j - 1] + grid[i][j]);
            }
        }
        return f[m - 1][n - 1];
    }
};