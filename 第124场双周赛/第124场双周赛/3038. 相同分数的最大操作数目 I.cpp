#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int maxOperations(vector<int>& nums) {
        int n = nums.size();
        int ans = 1;
        int t = nums[0] + nums[1];
        for (int i = 2; i < n - 1; i += 2) {
            if (nums[i] + nums[i + 1] == t)ans++;
            else break;
        }
        return ans;
    }
};