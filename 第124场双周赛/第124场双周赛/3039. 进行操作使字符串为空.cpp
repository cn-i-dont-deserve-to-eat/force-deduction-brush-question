#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    string lastNonEmptyString(string s) {
        int cnt[26] = { 0 };
        int res = 0;
        for (int i = 0; i < s.size(); i++) {
            int u = s[i] - 'a';
            cnt[u]++;
            res = max(res, cnt[u]);
        }

        for (int i = 0; i < 26; i++) {
            if (cnt[i] != res)cnt[i] = 0;
        }
        string ans = "";
        for (int i = s.size() - 1; i >= 0; i--) {
            int u = s[i] - 'a';
            if (cnt[u]) {
                ans += s[i];
                cnt[u] = 0;
            }
        }

        reverse(ans.begin(), ans.end());
        return ans;
    }
}; 