#define _CRT_SECURE_NO_WARNINGS 1
int ans;
const int N = 2005;
int f[N][N];
class Solution {
public:

    int dfs(int l, int r, int s, vector<int>& nums) {
        if (l >= r) {
            return  1;
        }
        int res = 1;
        if (f[l][r] != -1)return f[l][r];
        if (nums[l] + nums[r] == s) res = max(res, dfs(l + 1, r - 1, s, nums) + 1);
        if (nums[l] + nums[l + 1] == s) res = max(res, dfs(l + 2, r, s, nums) + 1);
        if (nums[r] + nums[r - 1] == s) res = max(res, dfs(l, r - 2, s, nums) + 1);
        f[l][r] = res;
        return res;
    }
    int maxOperations(vector<int>& nums) {
        int n = nums.size();
        memset(f, -1, sizeof f);
        ans = 0;
        //int f=0;
       // vector<vector<int>> f(n,vector<int>(n));
        ans = max(ans, dfs(2, n - 1, nums[0] + nums[1], nums));
        // memset(f,-1,sizeof f);
        ans = max(ans, dfs(0, n - 3, nums[n - 1] + nums[n - 2], nums));
        //memset(f,-1,sizeof f);
        ans = max(ans, dfs(1, n - 2, nums[0] + nums[n - 1], nums));
        return ans;
    }
};