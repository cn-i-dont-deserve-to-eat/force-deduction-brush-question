#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:

    bool IsContinuous(vector<int>& numbers) {
        int mx = 0;
        int mi = 1e9;
        int s = 0;
        for (int i = 0; i < numbers.size(); i++) {
            if (numbers[i] == 0) {
                continue;
            }
            mx = max(mx, numbers[i]);
            mi = min(mi, numbers[i]);
            if ((s >> numbers[i]) & 1) {
                return false;
            }
            else {
                s |= (1 << numbers[i]);

            }
        }
        if (mx - mi > 4) {
            return false;
        }
        return true;
    }
};