#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<string>
#include<algorithm>
using namespace std;
const int N = 1100;
int c[N];
int g[N];
int main() {
    string str;
    int ll;
    cin >> str >> ll;
    str.insert(str.begin(), '0');

    int n = str.size();
    for (int i = 1; i <= n; i++) {
        c[i] = c[i - 1] + (str[i] == 'C' ? 1 : 0);
        g[i] = g[i - 1] + (str[i] == 'G' ? 1 : 0);

    }

    string ans = "";
    double res = 0;
    int len = ll;
    for (int l = 1; l + len - 1 <= n; l++) {
        int r = l + len - 1;
        double s = c[r] - c[l - 1] + g[r] - g[l - 1];
        double k = (double)s / len;

        if (res < k) {
            ans = str.substr(l, len);
            res = k;
        }
    }

    cout << ans << endl;

    return 0;
}
