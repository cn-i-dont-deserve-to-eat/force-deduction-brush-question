#include <iostream>
#include<vector>
using namespace std;
const int N = 110;
int prim[N];
int st[N];
int cnt = 0;
void getprim() {
    st[1] = true;
    for (int i = 2; i < N; i++) {
        if (!st[i])prim[cnt++] = i;
        for (int j = 0; i < N / prim[j]; j++) {
            st[prim[j] * i] = true;
            if (i % prim[j] == 0)break;
        }
    }
}



int main() {

    int a, b;
    cin >> a >> b;
    int ans = 0;
    getprim();

    for (int i = a; i <= b; i++) {
        vector<int> res;
        int k = i;
        while (k) {
            res.push_back(k % 10);
            k /= 10;
        }
        int f = 0;
        for (int j = 0; j < res.size(); j++) {
            if (f == 1)break;
            for (int k = 0; k < res.size(); k++) {
                if (j == k || res[j] == 0)continue;
                if (!st[res[j] * 10 + res[k]]) {
                    f = 1;
                    break;
                }
            }
        }
        if (f == 1) {
            //  cout<<i<<endl;
            ans++;
        }

    }
    cout << ans << endl;
}
