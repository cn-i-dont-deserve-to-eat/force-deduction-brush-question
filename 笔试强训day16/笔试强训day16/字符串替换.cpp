#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
   
    string formatString(string str, vector<char>& arg) {
        string ans = "";
        int pos = 0;
        for (int i = 0; i < str.size(); i++) {
            if (i != str.size() - 1 && str[i] == '%' && str[i + 1] == 's') {
                ans += arg[pos++];
                i++;
            }
            else {
                ans += str[i];
            }
        }
        //  cout<<ans<<endl;
        while (pos < arg.size()) {
            ans += arg[pos++];
        }
        return ans;
    }
};