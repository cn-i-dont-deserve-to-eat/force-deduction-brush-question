#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int minDeletion(vector<int>& nums) {
        int cnt = 0;
        int len = nums.size();
        for (int i = 0; i < len - 1; i++) {
            if ((i - cnt) % 2 == 0 && nums[i] == nums[i + 1]) {
                cnt++;
            }
        }
        if ((len - cnt) % 2)return cnt + 1;
        return cnt;
    }
};  