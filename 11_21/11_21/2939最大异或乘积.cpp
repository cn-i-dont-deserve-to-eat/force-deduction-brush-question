#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int maximumXorProduct(long long a, long long b, int n) {
        long long ans = 0;
        const int mod = 1e9 + 7;
        if (a < b)swap(a, b);
        long long p = (a >> n) << n;
        long long q = (b >> n) << n;
        for (int i = n - 1; i >= 0; i--) {
            //  cout<<((a>>i)&1)<<" "<<((b>>i)&1)<<endl;
            if (((a >> i) & 1) == ((b >> i) & 1)) {
                q |= 1LL << i;
                p |= 1LL << i;
            }
            else if (q < p) {
                q |= 1LL << i;
            }
            else {
                p |= 1LL << i;
            }
        }
        //  cout<<p<<" "<<q<<endl;
         // cout<<ans<<endl;
        p %= mod;
        q %= mod;
        return p * q % mod;

    }
};