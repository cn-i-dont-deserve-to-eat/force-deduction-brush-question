#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int missingInteger(vector<int>& nums) {
        int len = nums.size();
        if (len == 1)return nums[0] + 1;
        vector<int> s(len + 1);
        vector<int> st(2510, false);
        for (int i = 1; i <= len; i++) {
            s[i] = s[i - 1] + nums[i - 1];
            st[nums[i - 1]] = true;
        }
        int j = 0;
        int maxl = 0;
        int maxs = 0;
        int f = 0;
        for (int i = 1; i < len; i++) {
            if (nums[i] != nums[i - 1] + 1) {
                f++;
                if (i - j > maxl || (i - j == maxl && s[i] - s[j] < maxs)) {
                    maxl = i - j;
                    maxs = s[i] - s[j];
                }
                break;

            }
        }
        if (f == 0) {
            maxs = s[len];
        }
        if (f + 1 == len)return s[1] + 1;
        // cout<<f<<" 000"<<len<<endl;
        cout << maxs << "---" << endl;
        int ans = 0;
        for (int i = maxs;; i++) {
            if (!st[i]) {
                ans = i;
                break;
            }
        }
        return ans;

    }
};