#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int minOperations(vector<int>& nums, int k) {
        int s = 0;
        int len = nums.size();
        for (int i = 0; i < len; i++)s ^= nums[i];
        int ans = 0;
        for (int i = 31; i >= 0; i--) {
            if (((k >> i) & 1) != ((s >> i) & 1)) {
                if ((s >> i) & 1 == 0) {
                    ans += len;
                }
                else {
                    ans++;
                }
            }
        }
        return ans;
    }
};