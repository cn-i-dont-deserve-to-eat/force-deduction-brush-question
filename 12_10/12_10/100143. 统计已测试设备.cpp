#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int countTestedDevices(vector<int>& batteryPercentages) {
        int ans = 0;
        int s = 0;
        int len = batteryPercentages.size();
        for (int i = 0; i < len; i++) {
            if (batteryPercentages[i] - s > 0) {
                ans++;
                s++;
            }
        }
        return ans;
    }
};