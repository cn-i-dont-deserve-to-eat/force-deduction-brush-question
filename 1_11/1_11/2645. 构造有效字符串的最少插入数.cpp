#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int addMinimum(string word) {
        stack<char> st;
        int res = 0;
        if (word[0] == 'a')st.push('a');
        else if (word[0] == 'b') {
            st.push('a');
            st.push('b');
        }
        else {
            st.push('a');
            st.push('b');
            st.push('c');
        }
        for (int i = 1; i < word.size(); i++) {
            if (word[i] == 'a') {
                if (st.top() != 'c') {
                    if (st.top() == 'a') {
                        st.push('b');
                        st.push('c');
                    }
                    else st.push('c');
                }
            }
            else if (word[i] == 'b') {
                if (st.top() != 'a') {
                    if (st.top() == 'b') {
                        st.push('c');
                        st.push('a');
                    }
                    else st.push('a');
                }
            }
            else {
                if (st.top() != 'b') {
                    if (st.top() == 'c') {
                        st.push('a');
                        st.push('b');
                    }
                    else st.push('b');
                }
            }
            st.push(word[i]);
        }

        cout << st.size();
        if (st.top() == 'c' && st.size() >= 3)return st.size() - word.size();
        return st.size() - word.size() + (3 - st.size() % 3);
    }
};