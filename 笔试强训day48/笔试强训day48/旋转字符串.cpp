    #define _CRT_SECURE_NO_WARNINGS 1
    class Solution {
    public:

        bool solve(string A, string B) {
            string a = A + A;
            if (B.size() != A.size())return false;
            if (a.find(B) != -1)return true;
            return false;
        }
    };