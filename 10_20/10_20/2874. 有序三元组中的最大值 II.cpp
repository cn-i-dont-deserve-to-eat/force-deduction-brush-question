#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    long long maximumTripletValue(vector<int>& nums) {
        // long long ans=-1e9;
        int l_ma = -1;
        int len = nums.size();
        vector<int> mr(len + 1);

        mr[len - 1] = nums[len - 1];
        for (int i = nums.size() - 2; i >= 0; i--) {
            mr[i] = max(nums[i], mr[i + 1]);
        }
        l_ma = nums[0];
        long long ans = -1e9;
        for (int i = 1; i < len - 1; i++) {
            if (l_ma >= nums[i]) {
                ans = max(ans, (long long)((long long)l_ma - nums[i]) * mr[i + 1]);
            }

            l_ma = max(l_ma, nums[i]);
        }

        if (ans < 0)return 0;
        return ans;
    }
};