#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
using namespace std;
const int N = 5e5 + 10;
typedef long long LL;
typedef pair<LL, int> PII;
LL cnt[N];
int l[N], r[N];
LL a[N];
int main() {
    int n, k;
    cin >> n >> k;
    priority_queue<PII, vector<PII>, greater<PII>>q;//最小堆
    r[0] = 1;//边界0
    l[n + 1] = n;//边界n+1
    for (int i = 1; i <= n; i++) {
        scanf("%lld", &a[i]);
        q.push({ a[i],i });//元素的值在左边，最小堆默认按左边第一个值排序
        r[i] = i + 1;//模拟双链表
        l[i] = i - 1;
    }

    while (q.size() != n - k) {//要删除k个元素
        auto it = q.top();
        q.pop();
        LL v = it.first;
        int dix = it.second;
        if (cnt[dix]) {
            v += cnt[dix];
            q.push({ v,dix });
            cnt[dix] = 0;
        }
        else {
            cnt[l[dix]] += v;//修改隔壁的增量
            cnt[r[dix]] += v;
            l[r[dix]] = l[dix];//双链表的删除操作
            r[l[dix]] = r[dix];

        }
    }

    while (!q.empty()) {//剩下元素按下标存入数组a中
        auto it = q.top();
        q.pop();
        a[it.second] = it.first;
    }

    int ne = 0;
    while (r[ne] != n + 1) {//遍历双链表
        printf("%lld ", a[r[ne]] + cnt[r[ne]]);
        ne = r[ne];
    }

    return 0;
}