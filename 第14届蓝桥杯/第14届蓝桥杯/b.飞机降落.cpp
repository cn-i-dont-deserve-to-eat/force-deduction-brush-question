#define _CRT_SECURE_NO_WARNINGS 1

//#include<iostream>
//#include<vector>
//#include<algorithm>
//using namespace std;
//const int N = 15;
//typedef pair<int, int> PII;
//
//bool cmp(vector<int>& A, vector<int>& B) {
//    return A[0] + A[1] + A[2] < B[0] + B[1] + B[2];
//
//}
//int main() {
//    int t;
//    cin >> t;
//    while (t--) {
//        int n;
//        cin >> n;
//        vector<vector<int>> A(n, vector<int>(3));
//        for (int i = 0; i < n; i++) {
//            int a, b, c;
//            cin >> a >> b >> c;
//            A[i] = { a,b,c };
//        }
//
//        sort(A.begin(), A.end(), cmp);
//        for (int i = 0; i < n; i++)cout << A[i][0] << " " << A[i][1] << " " << A[i][2] << endl;
//        int t = 0;
//        int f = 0;
//        for (int i = 0; i < n; i++) {
//            if (A[i][0] + A[i][1] < t) {
//                f = 1;
//                break;
//            }
//            if (A[i][0] >= t)t = A[i][0];
//            t += A[i][2];
//        }
//        if (f)cout << "NO" << endl;
//        else cout << "YES" << endl;
//    }
//
//
//    return 0;
//}
#include<iostream>
#include<vector>
#include<algorithm>
#include<cstring>
using namespace std;
const int N = 15;
int st[N];//标记已经降落的飞机
int a[N][3];//记录每一台飞机的起飞时间、盘旋时间、降落时间
int n;
bool ans;//标记答案
void dfs(int u, int t) {
    if (u == n) {//遍历到这里，长度已经够了，说明已经存在一个序列符合答案
        ans = true;
        return;
    }
    //安排下一台飞机
    for (int i = 1; i <= n; i++) {
        if (t > a[i][0] + a[i][1])continue;//不符合要求
        if (!st[i] && t <= a[i][0] + a[i][1]) {
            st[i] = 1;//标记序号为i的飞机要降落
            if (t <= a[i][0])dfs(u + 1, a[i][0] + a[i][2]);//如果当前的t比最早的起飞时间还早，那就等到时间为a[i][0]再降落
            else dfs(u + 1, t + a[i][2]);//否则就马上降落，更新时间
            st[i] = 0;//回溯
        }
    }
}
int main() {
    int t;
    cin >> t;
    while (t--) {
        cin >> n;
        for (int i = 1; i <= n; i++) {
            cin >> a[i][0] >> a[i][1] >> a[i][2];
        }
        memset(st, 0, sizeof st);
        ans = false;
        dfs(0, 0);
        if (ans)cout << "YES" << endl;
        else cout << "NO" << endl;
    }


    return 0;
}