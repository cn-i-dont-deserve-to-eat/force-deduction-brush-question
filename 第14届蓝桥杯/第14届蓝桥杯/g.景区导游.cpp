#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;
typedef long long LL;
const int N = 1e5 + 10;
int f[N][21];
int deth[N];
LL dis[N];
int a[N];
vector<int>e[N], w[N];

void dfs(int u, int fa) {
    deth[u] = deth[fa] + 1;
    f[u][0] = fa;

    for (int i = 1; i <= 20; i++) {
        f[u][i] = f[f[u][i - 1]][i - 1];
    }
    int sz = e[u].size();
    for (int i = 0; i < sz; i++) {
        int v = e[u][i], s = w[u][i];
        if (v == fa)continue;
        dis[v] = dis[u] + s;
        //cout<<u<<"-->"<<v<<" ll "<<s<<" kk "<<dis[u]<<endl;
        dfs(v, u);
    }
}

int LCA(int u, int v) {
    if (deth[u] < deth[v])swap(u, v);
    for (int i = 20; i >= 0; i--) {
        if (deth[f[u][i]] >= deth[v]) {
            u = f[u][i];
        }
    }
    if (u == v)return u;
    for (int i = 20; i >= 0; i--) {
        if (f[u][i] != f[v][i]) {
            u = f[u][i];
            v = f[v][i];
        }
    }
    return f[u][0];
}

LL getpath(int u, int v) {
    if (!u || !v)return 0;
    return dis[u] + dis[v] - 2 * dis[LCA(u, v)];
}

int main() {
    int n, k;
    cin >> n >> k;
    for (int i = 0; i < n - 1; i++) {
        int a, b, c;
        cin >> a >> b >> c;
        e[a].push_back(b);
        w[a].push_back(c);
        e[b].push_back(a);
        w[b].push_back(c);

    }

    dfs(1, 0);
    LL ans = 0;
    cin >> a[0];
    for (int i = 1; i < k; i++) {
        cin >> a[i];
        ans += getpath(a[i], a[i - 1]);
    }
    for (int i = 0; i < k; i++) {
        LL d1 = 0;
        if (i != 0)d1 += getpath(a[i], a[i - 1]);
        if (i != k - 1)d1 += getpath(a[i + 1], a[i]);
        LL d2 = 0;
        if (i != 0 && i != k - 1) {
            d2 += getpath(a[i + 1], a[i - 1]);
        }
        cout << ans - d1 + d2 << " ";
    }

    return 0;
}