#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<math.h>
#include<cstring>
#include<algorithm>
using namespace std;
const int N = 1e5 + 10;
int f[10];
int a[N];
int s[10];
int get_n(int x) {
    int res = 0;
    while (x) {
        x /= 10;
        res++;
    }
    return res;
}
int gethh(int x) {
    int k = get_n(x);
    int hh = x / pow(10, k - 1);
    return hh;
}
int main() {
    int n;
    cin >> n;
    if (n == 1) {
        cout << 0 << endl;
        return 0;
    }
    for (int i = 1; i <= n; i++) {
        cin >> a[i];
    }

    int ans = 0;
    for (int i = 1; i <= n; i++) {
        int k1 = gethh(a[i]);
        int k2 = a[i] % 10;
        f[k2] = max(f[k1] + 1, f[k2]);
        ans = max(ans, f[k2]);

    }
    cout << n - ans << endl;
    return 0;

}   