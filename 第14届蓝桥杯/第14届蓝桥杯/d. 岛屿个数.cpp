#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
#include<queue>
#include<cstring>
using namespace std;
const int N = 60;
typedef pair<int, int> PII;
int m, n;
char g[N][N];
bool st[N][N];//标记哪些坐标被遍历过了
int dx[] = { 1,0,-1,0 }, dy[] = { 0,1,0,-1 };//遍历岛屿的位移偏移量
int dxx[] = { 0,-1,-1,-1,0,1,1,1 }, dyy[] = { -1,-1,0,1,1,1,0,-1 };//遍历海水的位移偏移量
int ans;
void bfs1(int x, int y) {//第一次遍历海水
    queue<PII> q;
    q.push({ x,y });
    while (!q.empty()) {
        int sz = q.size();
        for (int i = 0; i < sz; i++) {
            auto it = q.front();
            q.pop();
            int xx = it.first;
            int yy = it.second;
            // g[xx][yy]=1;
            if (st[xx][yy])continue;
            st[xx][yy] = true;
            for (int j = 0; j < 8; j++) {
                int a = dxx[j] + xx;
                int b = dyy[j] + yy;
                if (a >= 0 && a < m && b >= 0 && b < n && g[a][b] == '0' && !st[a][b]) {
                    q.push({ a,b });
                }
            }
        }
    }
}
void bfs2(int x, int y) {//遍历岛屿
    queue<PII> q;
    q.push({ x,y });
    while (!q.empty()) {
        int sz = q.size();
        for (int i = 0; i < sz; i++) {
            auto it = q.front();
            q.pop();
            int xx = it.first;
            int yy = it.second;
            // g[xx][yy]=1;
            if (st[xx][yy])continue;
            st[xx][yy] = true;
            for (int j = 0; j < 4; j++) {
                int a = dx[j] + xx;
                int b = dy[j] + yy;
                if (a >= 0 && a < m && b >= 0 && b < n && g[a][b] == '1' && !st[a][b]) {
                    q.push({ a,b });
                }
            }
        }
    }
    return;
}

int main() {
    int t;
    cin >> t;
    while (t--) {
        memset(st, false, sizeof st);
        cin >> m >> n;
        for (int i = 0; i < m; i++) {//存图
            for (int j = 0; j < n; j++) {
                cin >> g[i][j];
            }
        }
        //开始遍历一定不在环内的海水
        for (int j = 0; j < n; j++) {
            if (!st[0][j] && g[0][j] == '0') {
                bfs1(0, j);
            }
            if (!st[m - 1][j] && g[m - 1][j] == '0') {
                bfs1(m - 1, j);
            }
        }

        for (int i = 0; i < m; i++) {
            if (!st[i][0] && g[i][0] == '0') {
                bfs1(i, 0);
            }
            if (!st[i][n - 1] && g[i][n - 1] == '0') {
                bfs1(i, n - 1);
            }
        }
        //把在环内的海水设置为陆地
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (!st[i][j]) {
                    g[i][j] = '1';
                }
            }
        }

        ans = 0;
        //计算岛屿数量
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (!st[i][j] && g[i][j] == '1') {
                    ans++;
                    bfs2(i, j);
                }
            }
        }
        cout << ans << endl;


    }


    return 0;
}