#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
#include<vector>
#include<string>
using namespace std;

int main()
{
    int k;
    char a, b;
    string str;
    cin >> k >> str >> a >> b;
    int n = str.size();
    //int ans;
    vector<long long > s(n, 0);
    if (str[0] == a)s[0] = 1;
    for (int i = 1; i < n; i++) {
        if (str[i] == a)s[i] = 1;//如果是c1我们就把这个位置标记为1，方便前缀和计算
        s[i] += s[i - 1];
    }
    long long ans = 0;
    for (int i = 0; i < n; i++) {
        if (i >= k - 1 && str[i] == b) {
            ans += s[i - k + 1];
        }
    }
    cout << ans << endl;
    return 0;
}