#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
using namespace std;
const int N = 1e4 + 10;
int arr[N][2];//冶炼金属的记录

int main() {
    int t;
    cin >> t;
    int maxv = 1e9;
    int minv = 0;
    for (int i = 0; i < t; i++) {
        int a, b;
        cin >> a >> b;
        arr[i][0] = a;
        arr[i][1] = b;
        maxv = min(maxv, a / b);//区间取交集，右端点要不断地取最小值，
        minv = max(minv, (a) / (b + 1));//区间取交集，左端点要不断地取最大值，
    }

    //两次二分分别得到最大值和最小值
    int l = minv;
    int r = maxv + 1;
    int ans1 = 0, ans2 = 0;
    while (l + 1 != r) {
        int mid = (l + r) >> 1;
        int f = 0;

        for (int i = 0; i < t; i++) {
            if (arr[i][0] / mid != arr[i][1]) {
                f = 1;
                break;
            }
        }
        if (f) {
            l = mid;
        }
        else r = mid;
    }
    ans1 = r;
    l = minv;
    r = maxv + 1;
    //int ans1=0,ans2=0;
    while (l + 1 != r) {
        int mid = (l + r) >> 1;
        int f = 0;

        for (int i = 0; i < t; i++) {
            if (arr[i][0] / mid != arr[i][1]) {
                f = 1;
                break;
            }
        }
        if (f) {
            r = mid;
        }
        else l = mid;
    }
    ans2 = l;
    cout << ans1 << " " << ans2 << endl;

    return 0;
}