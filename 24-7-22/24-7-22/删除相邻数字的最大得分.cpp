#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<vector>
#include<unordered_map>
using namespace std;
const int N = 1e5 + 10;
typedef pair<int, int> PII;
int a[N];
int main() {
    int n;
    cin >> n;
    unordered_map<int, int> mp;
    int ma = 0;
    int mi = 1e9;
    for (int i = 1; i <= n; i++) {
        cin >> a[i];
        mp[a[i]]++;
        ma = max(ma, a[i]);
        mi = min(mi, a[i]);
    }
    //cout<<ma<<" "<<mi<<endl;
    vector<int> dp(ma + 1);

    for (int i = mi; i <= ma; i++) {
        dp[i] = max(dp[i - 1], dp[i - 2] + mp[i] * i);
    }
    cout << dp[ma] << endl;


    return 0;
}
