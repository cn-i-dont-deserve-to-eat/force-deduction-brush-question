#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<string>
using namespace std;

int main() {
    string a;
    string b;
    int mp[26] = { 0 };
    cin >> a >> b;
    int n = a.size();
    if (a.size() < b.size()) {
        cout << "No" << endl;
        return 0;
    }
    for (int i = 0; i < n; i++)
    {
        int u = a[i] - 'A';
        mp[u]++;
    }
    int f = 0;
    for (int i = 0; i < b.size(); i++) {
        int u = b[i] - 'A';
        if (mp[u] == 0)
        {
            f = 1;
            break;
        }
        mp[u]--;
    }

    if (f) {
        cout << "No" << endl;
    }
    else {
        cout << "Yes" << endl;
    }


    return 0;
}
    