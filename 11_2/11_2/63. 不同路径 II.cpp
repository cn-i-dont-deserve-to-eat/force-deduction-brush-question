#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
        int m = obstacleGrid.size();
        int n = obstacleGrid[0].size();

        vector<vector<int>> f(m + 1, vector<int>(n + 1, 0));

        f[0][0] = 1;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (obstacleGrid[i][j] == 1) {
                    f[i][j] = -1e9;
                }
            }
        }
        if (f[0][0] == 1e9)return 0;
        int flag = 1;
        for (int i = 0; i < n; i++) {
            if (obstacleGrid[0][i] == 0 && flag) {
                f[0][i] = 1;
                //  flag=0;
            }
            else {
                flag = 0;
            }
        }
        flag = 1;
        for (int i = 0; i < m; i++) {
            if (obstacleGrid[i][0] == 0 && flag) {
                f[i][0] = 1;
                //  flag=0;
            }
            else {
                flag = 0;
            }
        }

        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                if (obstacleGrid[i][j] == 1)continue;
                if (obstacleGrid[i - 1][j] != 1) {
                    f[i][j] = f[i - 1][j];
                }
                if (obstacleGrid[i][j - 1] != 1) {
                    f[i][j] += f[i][j - 1];
                }
            }
        }
        return max(0, f[m - 1][n - 1]);

    }
};