#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int countPoints(string rings) {
        int len = rings.size();
        vector<vector<int>> f(10, vector<int>(5, 0));
        for (int i = 1; i < len; i += 2) {
            int u = rings[i] - '0';
            if (rings[i - 1] == 'R') {
                f[u][0]++;
            }
            else if (rings[i - 1] == 'G') {
                f[u][1]++;
            }
            else {
                f[u][2]++;
            }
        }

        int ans = 0;
        for (int i = 0; i < 10; i++) {
            int cnt = 0;
            for (int j = 0; j < 3; j++) {
                if (f[i][j]) {
                    cnt++;
                }
            }
            if (cnt == 3)ans++;
        }
        return ans;
    }
};