#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:

    string solve(string s, string t) {
        vector<int> a;
        vector<int> b;
        if (s == "0" || t == "0")return "0";
        for (int i = s.size() - 1; i >= 0; i--)a.push_back(s[i] - '0');
        for (int i = t.size() - 1; i >= 0; i--)b.push_back(t[i] - '0');
        int n = a.size();
        int m = b.size();
        vector<int> c(m + n);
        int tt = 0;
        for (int i = 0; i < a.size(); i++) {
            tt = 0;
            for (int j = 0; j < b.size(); j++) {
                tt = a[i] * b[j];
                c[i + j] += tt;
                // cout<<i*j<<"--"<<c[i*j]<<" ";
                c[i + j + 1] += c[i + j] / 10;
                c[i + j] %= 10;
            }
        }
        int p = c.size() - 1;
        while (c[p] == 0 && p >= 0) {
            p--;
        }
        string ans = "";
        for (int i = p; i >= 0; i--) {
            string k = to_string(c[i]);
            ans += k;
        }
        return ans;
    }
};