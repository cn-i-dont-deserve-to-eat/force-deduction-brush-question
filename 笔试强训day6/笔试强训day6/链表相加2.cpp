#define _CRT_SECURE_NO_WARNINGS 1
/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 *	ListNode(int x) : val(x), next(nullptr) {}
 * };
 */
class Solution {
public:
    ListNode* relist(ListNode** head) {
        ListNode* a = (*head)->next;
        ListNode* pre = *head;
        pre->next = nullptr;
        if (a == nullptr)return *head;
        while (a) {
            //cout<<a->val<<endl;
            ListNode* temp = a->next;
            a->next = pre;
            pre = a;
            if (temp == nullptr)break;
            a = temp;
        }
        //  cout<<a->val<<endl;
        return a;
    }
    ListNode* addInList(ListNode* head1, ListNode* head2) {
        ListNode* a = relist(&head1);

        ListNode* b = relist(&head2);

        ListNode* c = (ListNode*)new ListNode(-1);
        ListNode* p3 = c;
        ListNode* p1 = a;
        ListNode* p2 = b;
        int t = 0;
        while (p1 || p2) {
            if (p1) {
                t += p1->val;
                p1 = p1->next;
            }
            if (p2) {
                t += p2->val;
                p2 = p2->next;
            }
            ListNode* temp = (ListNode*)new ListNode(t % 10);
            p3->next = temp;
            p3 = temp;
            t /= 10;
        }
        if (t) {
            ListNode* temp = (ListNode*)new ListNode(t % 10);
            p3->next = temp;
            p3 = temp;
            t /= 10;
        }
        return relist(&c->next);
    }
};