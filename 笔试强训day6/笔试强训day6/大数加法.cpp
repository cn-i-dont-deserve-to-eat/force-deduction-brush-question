#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:

    string solve(string s, string t) {
        vector<int> a;
        vector<int> b;

        for (int i = s.size() - 1; i >= 0; i--)a.push_back(s[i] - '0');
        for (int i = t.size() - 1; i >= 0; i--)b.push_back(t[i] - '0');
        if (s.size() == 0)a.push_back(0);
        if (t.size() == 0)b.push_back(0);

        vector<int> c;
        int tt = 0;
        for (int i = 0; i < a.size() || i < b.size(); i++) {
            if (i < a.size())tt += a[i];
            if (i < b.size())tt += b[i];

            c.push_back(tt % 10);
            tt /= 10;
        }
        if (tt)c.push_back(tt);
        string ans = "";
        for (int i = c.size() - 1; i >= 0; i--) {
            char u = c[i] + '0';
            ans += u;
        }
        return ans;
    }
};