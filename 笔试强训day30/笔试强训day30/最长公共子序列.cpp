#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
const int N = 1100;
int f[N];
int main() {
    int n, m;
    cin >> n >> m;

    string a, b;
    cin >> a >> b;
    if (a.size() < b.size())swap(a, b);
    int p = 0;
    for (int i = 1; i <= a.size(); i++) {
        for (int j = 1, p = 0; j <= b.size(); j++) {
            int t = f[j];
            if (a[i - 1] == b[j - 1]) {
                f[j] = p + 1;
            }
            else {
                f[j] = max(f[j], f[j - 1]);
            }
            //cout<<f[j]<<endl;
            p = t;
        }
    }
    cout << f[b.size()] << endl;

    return 0;
}
