#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
typedef long long LL;

bool isprem(LL x) {
    if (x == 1)return false;
    for (int i = 2; i <= x / i; i++) {
        if (x % i == 0)return false;
    }
    return true;
}

int main() {
    int t;
    cin >> t;
    while (t--) {
        LL a, b;
        cin >> a >> b;
        if ((a == 1 && isprem(b)) || (b == 1 && isprem(a))) {
            cout << "YES" << endl;
        }
        else {
            cout << "NO" << endl;
        }
    }

    return 0;
}
