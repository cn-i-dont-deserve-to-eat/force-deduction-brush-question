#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<algorithm>
using namespace std;
const int N = 2e5 + 10;
int a[N];
int main() {
    int n, k;
    cin >> n >> k;
    for (int i = 0; i < n; i++)cin >> a[i];
    sort(a, a + n);
    int ans = 0;
    for (int i = 0; i < n; i++) {
        int l = i - 1;
        int r = n;
        while (l + 1 != r) {
            int mid = (l + r) >> 1;
            if (a[mid] <= a[i] + k)l = mid;
            else r = mid;
        }
        ans = max(l - i + 1, ans);
    }
    cout << ans << endl;
    return 0;
}
