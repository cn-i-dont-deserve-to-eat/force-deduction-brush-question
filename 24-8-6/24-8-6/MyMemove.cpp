#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<assert.h>
using namespace std;


void MyMemmove(void* dest, const void* src, size_t n)
{
	assert(dest && src);
	
	void* res = dest;
	if (src > dest)//��ǰ���󿽱�
	{
		while (n--)
		{
			*(char*)res = *(char*)src;
			res = (char*)res + 1;
			src = (char*)src + 1;
		}
	}
	else {
		while (n--)
		{
			*((char*)res + n) = *((char*)src + n);
		}
	}
	
}

int main()
{

	int arr1[] = { 1,2,3,4,5 };
	int arr2[] = { 9,9,9,9,9 };
	MyMemmove(arr1, arr1+2, 12);
	for (int i = 0; i < 5; i++)
	{
		cout << arr1[i] << " ";
	}
	cout << endl;

	return 0;
}