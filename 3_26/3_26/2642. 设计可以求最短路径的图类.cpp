#define _CRT_SECURE_NO_WARNINGS 1
typedef pair<int, int> PII;
class Graph {
public:
    Graph(int n, vector<vector<int>>& edges) {
        vector<vector<PII>> g1(n);
        g = g1;

        for (auto it : edges) {
            int a = it[0];
            int b = it[1];
            int c = it[2];
            g[a].push_back({ b,c });
        }
    }

    void addEdge(vector<int> edge) {
        int a = edge[0];
        int b = edge[1];
        int c = edge[2];
        g[a].push_back({ b,c });
    }

    int shortestPath(int node1, int node2) {
        priority_queue<PII, vector<PII>, greater<PII>> q;
        vector<int> dist(g.size(), INT_MAX);
        q.push({ 0,node1 });
        while (!q.empty()) {
            auto it = q.top();
            q.pop();
            int d = it.first;
            int pos = it.second;

            if (pos == node2)return d;

            for (auto it : g[pos]) {
                int a = it.first;
                int b = it.second;
                if (d + b < dist[a]) {
                    dist[a] = d + b;
                    q.push({ d + b,a });
                }
            }

        }
        return -1;
    }

    vector<vector<PII>> g;

};

/**
 * Your Graph object will be instantiated and called as such:
 * Graph* obj = new Graph(n, edges);
 * obj->addEdge(edge);
 * int param_2 = obj->shortestPath(node1,node2);
 */