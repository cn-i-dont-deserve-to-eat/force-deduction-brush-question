#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:

    bool average(int x) {
        vector<int> cont(10, 0);
        int k = x;
        while (k) {
            int b = k % 10;
            if (b == 0)return false;
            // if(b>7)return false;
            cont[b]++;
            //if(cont[b]>b)return false;
            k /= 10;
        }
        for (int i = 1; i <= 9; i++) {
            //cout<<cont[i]<<endl;
            if (cont[i] > 0 && cont[i] != i)return false;
        }
        return true;
    }
    int nextBeautifulNumber(int n) {
        for (int i = n + 1; i <= 1224444; i++) {
            if (average(i)) {
                return i;
            }
        }
        return 0;
    }
};