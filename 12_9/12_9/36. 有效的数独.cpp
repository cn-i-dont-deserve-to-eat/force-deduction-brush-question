#define _CRT_SECURE_NO_WARNINGS 1
int dx[8] = { 0,-1,-1,-1,0,1,1,1 };
int dy[8] = { -1,-1,0,1,1,1,0,-1 };
class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
        vector<vector<int>>stc(10, vector<int>(10));
        vector<vector<int>>str(10, vector<int>(10));
        vector<vector<vector<int>>> sts(3, vector<vector<int>>(3, vector<int>(10)));
        int m = board.size();
        int n = board[0].size();
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (board[i][j] == '.')continue;
                int u = board[i][j] - '0';

                str[i][u]++;
                if (str[i][u] > 1) {
                    // cout<<1<<endl;
                    return false;
                }
                stc[j][u]++;
                if (stc[j][u] > 1) {
                    //cout<<2<<endl;
                    return false;
                }
                int a = i / 3;
                int b = j / 3;
                sts[a][b][u]++;
                if (sts[a][b][u] > 1)return false;
                // for(int k=a*3;k<=a*3+2;k++){
                //     for(int z=b*3;z<=b*3+2;z++){
                //         //if(k>9||z>9)continue;
                //         if(k==i&&z==j)continue;
                //         if(board[k][z]==board[i][j])return false;
                //     }
                // }
            }
        }
        return true;
    }
};