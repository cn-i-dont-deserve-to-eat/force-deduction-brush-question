#define _CRT_SECURE_NO_WARNINGS 1
int minOperations(int* nums, int numsSize, int k) {
    int* p = (int*)calloc(numsSize + 1, sizeof(int));
    int ans = 0;
    for (int i = numsSize - 1; i >= 0; i--) {
        p[nums[i]]++;
        ans++;
        int flag = 1;
        for (int i = 1; i <= k; i++) {
            if (!p[i]) {
                flag = 0;
                break;
            }
        }
        if (flag) {
            return ans;
        }
    }
    return ans;

}