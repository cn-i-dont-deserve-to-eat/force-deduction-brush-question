#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int bfs(vector<int>& nums, int start, int goal, vector<int>& mp) {
        int len = nums.size();
        queue<int> q;
        int ans = 0;
        q.push(start);
        mp[start] = 1;
        while (!q.empty()) {
            int n = q.size();
            for (int j = 0; j < n; j++) {
                int it = q.front();
                //cout<<it<<endl;
                q.pop();
                // if(it==goal)return ans;
                for (int i = 0; i < len; i++) {
                    int t1 = it + nums[i];
                    int t2 = it - nums[i];
                    // cout<<t2<<endl;
                    int t3 = it ^ nums[i];
                    if (t1 == goal || t2 == goal || t3 == goal)return ans + 1;
                    if (t1 >= 0 && t1 <= 1000 && !mp[t1]) {
                        q.push(t1);
                        mp[t1] = 1;
                    }
                    if (t2 >= 0 && t2 <= 1000 && !mp[t2]) {
                        q.push(t2);
                        mp[t2] = 1;
                    }
                    if (t3 >= 0 && t3 <= 1000 && !mp[t3]) {
                        q.push(t3);
                        mp[t3] = 1;
                    }
                }
            }
            ans++;
        }
        return -1;
    }
    int minimumOperations(vector<int>& nums, int start, int goal) {
        vector<int>mp(1005, 0);
        return bfs(nums, start, goal, mp);
    }
};