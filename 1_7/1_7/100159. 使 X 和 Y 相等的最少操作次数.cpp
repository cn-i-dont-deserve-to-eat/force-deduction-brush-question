#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int fun(int x, int y, unordered_map<int, int>& mp) {
        if (x <= y)return y - x;
        if (mp.count(x))return mp[x];
        int res = x - y;
        res = min(res, fun(x / 5, y, mp) + x % 5 + 1);
        res = min(res, fun(x / 5 + 1, y, mp) + 5 - x % 5 + 1);
        res = min(res, fun(x / 11, y, mp) + x % 11 + 1);
        res = min(res, fun(x / 11 + 1, y, mp) + 11 - x % 11 + 1);
        mp[x] = res;
        return mp[x];
    }
    int minimumOperationsToMakeEqual(int x, int y) {
        unordered_map<int, int> mp;
        return fun(x, y, mp);
    }
};
//������
typedef pair<int, int> PII;
class Solution {
public:
    int fun(int x, int y, vector<int>& st) {
        if (y >= x)return y - x;
        queue<int> q;
        q.push(x);
        int ans = 0;
        while (!q.empty()) {
            int n = q.size();
            for (int i = 0; i < n; i++) {
                int a = q.front();
                q.pop();
                if (a == y)return ans;
                if (a + 1 >= 1e4 + 11 || a - 1 < 0)continue;
                if (!st[a - 1]) q.push(a - 1), st[a - 1] = 1;
                if (!st[a + 1])q.push(a + 1), st[a + 1] = 1;
                if (a % 5 == 0 && !st[a / 5])q.push(a / 5), st[a / 5] = 1;
                if (a % 11 == 0 && !st[a / 11])q.push(a / 11), st[a / 11] = 1;
            }
            ans++;
        }
        return ans;
    }
    int minimumOperationsToMakeEqual(int x, int y) {
        // unordered_map<int,int> mp;
        const int N = 1e4 + 11;
        vector<int>st(N, 0);
        return fun(x, y, st);
    }
};