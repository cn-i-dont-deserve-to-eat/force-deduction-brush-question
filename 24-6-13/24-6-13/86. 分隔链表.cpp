#define _CRT_SECURE_NO_WARNINGS 1
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* partition(ListNode* head, int x) {
        if (head == nullptr)return head;
        ListNode* l1 = new ListNode(-1);
        ListNode* cur1 = l1;

        ListNode* l2 = new ListNode(-1);
        ListNode* cur2 = l2;
        ListNode* cur = head;
        while (cur) {
            ListNode* t = cur->next;
            if (cur->val < x) {
                cur1->next = cur;
                cur1 = cur1->next;
            }
            else {
                cur2->next = cur;
                cur2 = cur2->next;
            }
            cur = t;
        }
        cur1->next = nullptr;
        cur2->next = nullptr;
        if (l1->next == nullptr)return l2->next;
        else {
            cur1->next = l2->next;
            return l1->next;
        }
    }
};