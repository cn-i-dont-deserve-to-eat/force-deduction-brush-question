class Solution {
public:
    int maxBottlesDrunk(int numBottles, int numExchange) {
        int a = numBottles;
        int ans = a;
        int b = numExchange;
        while (a >= b) {
            ans += 1;
            a -= b - 1;
            b++;
        }
        return ans;
    }
};