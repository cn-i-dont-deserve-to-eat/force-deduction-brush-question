#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:

    long long countAlternatingSubarrays(vector<int>& nums) {
        long long ans = 0;
        int n = nums.size();
        if (n == 1)return 1;
        int i = 1, j = 0;
        for (; i < n; i++) {
            if (nums[i] == nums[i - 1]) {
                int len = i - j;
                ans += ((long long)len * (len + 1) / 2);
                j = i;
            }
        }
        if (nums[n - 1] != nums[n - 2]) {
            int len = i - j;
            ans += ((long long)len * (len + 1) / 2);
        }
        else {
            ans += 1;
        }
        return ans;
    }
};