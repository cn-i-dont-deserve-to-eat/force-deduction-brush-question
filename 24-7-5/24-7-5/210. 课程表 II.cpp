#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int d[2010];
    vector<int> topu(int numCourses, vector<vector<int>>& g) {
        queue<int> q;
        vector<int> ans;
        for (int i = 0; i < numCourses; i++) {
            if (d[i] == 0) {
                q.push(i);
                ans.push_back(i);
            }
        }

        while (!q.empty()) {
            int t = q.front();
            q.pop();

            for (int i = 0; i < g[t].size(); i++) {
                d[g[t][i]]--;
                if (d[g[t][i]] == 0) {
                    q.push(g[t][i]);
                    ans.push_back(g[t][i]);
                }
            }
        }
        if (ans.size() == numCourses)return ans;
        return {};
    }
    vector<int> findOrder(int numCourses, vector<vector<int>>& prerequisites) {
        vector<vector<int>> g(numCourses);

        int n = prerequisites.size();
        for (int i = 0; i < n; i++) {
            int a = prerequisites[i][0];
            int b = prerequisites[i][1];
            g[b].push_back(a);
            d[a]++;
        }
        return topu(numCourses, g);
    }
};  