#define _CRT_SECURE_NO_WARNINGS 1
struct SkilpListNode {
    int _val;
    vector<SkilpListNode*> _nextv;

    SkilpListNode(int val, int level)
        :_val(val)
        , _nextv(level, nullptr)
    {

    }
};
class Skiplist {
public:
    typedef SkilpListNode Node;
    Skiplist() {
        srand(time(NULL));//设置随机数种子

        _head = new Node(-1, 1);//头节点，层数为1
    }

    bool search(int target) {
        Node* cur = _head;
        int level = _head->_nextv.size() - 1;
        while (level >= 0) {
            //层数从高往低开始
            if (cur->_nextv[level] && target > cur->_nextv[level]->_val) {
                cur = cur->_nextv[level];//如果目标值大于当前节点的第level层索引的值,可以跳过去，更新cur
            }
            else if (!cur->_nextv[level] || target < cur->_nextv[level]->_val) {
                level--;//下一个索引节点是空
                //如果目标值小于当前节点的第level层索引的值,往下层移动，也就是再去比较值更小的下级索引
            }
            else {
                return true;//找到目标值
            }
        }
        return false;
    }


    vector<Node*> FindPrevNode(int num) {
        Node* cur = _head;
        int level = _head->_nextv.size() - 1;
        vector<Node*> prev(level + 1, _head);//用一个数组存下新节点的所有层的前一个节点
        while (level >= 0) {
            //层数从高往低开始
            if (cur->_nextv[level] && num > cur->_nextv[level]->_val) {
                cur = cur->_nextv[level];
            }
            else if (!cur->_nextv[level] || num <= cur->_nextv[level]->_val) {
                prev[level] = cur;
                level--;
            }
        }
        return prev;
    }
    void add(int num) {
        vector<Node*> prev = FindPrevNode(num);
        int n = GetRandomLevel();
        Node* newnode = new Node(num, n);
        if (n > _head->_nextv.size()) {
            _head->_nextv.resize(n, nullptr);
            prev.resize(n, _head);
        }

        for (int i = 0; i < n; i++) {
            newnode->_nextv[i] = prev[i]->_nextv[i];
            prev[i]->_nextv[i] = newnode;
        }
    }

    bool erase(int num) {
        vector<Node*> prev = FindPrevNode(num);

        if (prev[0]->_nextv[0] == nullptr || prev[0]->_nextv[0]->_val != num) {
            return false;
        }
        Node* delnode = prev[0]->_nextv[0];
        int n = delnode->_nextv.size();
        for (int i = 0; i < n; i++) {
            prev[i]->_nextv[i] = delnode->_nextv[i];
        }
        delete delnode;
        //如果删除的节点是最高层数
        int i = _head->_nextv.size() - 1;
        while (i >= 0) {
            if (_head->_nextv[i] == nullptr) {
                --i;
            }
            else break;
        }
        _head->_nextv.resize(i + 1);
        return true;
    }


    int GetRandomLevel() {
        size_t level = 1;
        while (rand() <= RAND_MAX * _p && level < _maxlevel) {
            level++;
        }
        return level;
    }

private:
    Node* _head;
    size_t _maxlevel = 32;
    double _p = 0.25;//增加层数的概率

};

/**
 * Your Skiplist object will be instantiated and called as such:
 * Skiplist* obj = new Skiplist();
 * bool param_1 = obj->search(target);
 * obj->add(num);
 * bool param_3 = obj->erase(num);
 */