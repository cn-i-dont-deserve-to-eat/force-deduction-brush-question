#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int d[10000];

    int topu(int numCourses, vector<vector<int>>& g) {
        queue<int> q;
        int num = 0;
        for (int i = 0; i < numCourses; i++) {
            if (d[i] == 0)q.push(i), num++;
        }

        while (!q.empty()) {
            int t = q.front();
            q.pop();

            for (int i = 0; i < g[t].size(); i++) {
                // cout<<g[t][i]<<endl;
                d[g[t][i]]--;
                if (d[g[t][i]] == 0) {
                    //cout<<g[t][i]<<endl;
                    num++;
                    q.push(g[t][i]);
                }
            }
        }
        return num;

    }
    bool canFinish(int numCourses, vector<vector<int>>& prerequisites) {
        vector<vector<int>> g(numCourses);
        int n = prerequisites.size();
        for (int i = 0; i < n; i++) {
            int a = prerequisites[i][0];
            int b = prerequisites[i][1];
            g[b].push_back(a);
            d[a]++;
        }
        int ans = topu(numCourses, g);
        cout << ans << endl;
        if (ans == numCourses)return true;
        return false;
    }
};