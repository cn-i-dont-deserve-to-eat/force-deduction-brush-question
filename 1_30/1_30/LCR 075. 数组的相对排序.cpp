#define _CRT_SECURE_NO_WARNINGS 1


/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* relativeSortArray(int* arr1, int arr1Size, int* arr2, int arr2Size, int* returnSize) {
    int* arr3 = malloc(sizeof(int) * arr1Size);
    int pp = 0;
    int a[1010] = { 0 };
    int a2[1010] = { 0 };
    for (int i = 0; i < arr1Size; i++) {
        a[arr1[i]]++;
    }
    int end = arr1Size - 1;
    for (int i = 0; i < arr2Size; i++) {
        a2[arr2[i]]++;
    }
    //int p=0;
    for (int j = 0; j < arr2Size; j++) {
        int x = a[arr2[j]];
        while (x) {
            arr3[pp++] = arr2[j];
            x--;
        }
    }
    for (int i = 0; i < 1010; i++) {
        if (!a2[i] && a[i]) {
            int x = a[i];
            while (x) {
                arr3[pp++] = i;
                x--;
            }
        }
    }
    *returnSize = pp;
    return arr3;
}