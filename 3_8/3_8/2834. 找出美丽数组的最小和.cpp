#define _CRT_SECURE_NO_WARNINGS 1
//https://leetcode.cn/problems/find-the-minimum-possible-sum-of-a-beautiful-array/description/
const int mod = 1e9 + 7;

int exgcd(int a, int b, int& x, int& y) {
    if (!b) {
        x = 1;
        y = 0;
        return a;
    }
    int d = exgcd(b, a % b, y, x);
    y -= a / b * x;
    return d;
}

long long  km(long long a, long long  b) {
    long long res = 1;
    while (b) {
        if (b & 1)res = res * a % mod;
        a = a * a % mod;
        b >>= 1;
    }
    return res % mod;
}

int guim(long long a, long long b) {
    long long res = 0;
    while (b) {
        if (b & 1)res = (res + a) % mod;
        a = (a + a) % mod;
        b >>= 1;
    }
    return res;
}
class Solution {
public:
    int minimumPossibleSum(int n, int target) {
        int k = target / 2;
        if (k >= n)k = n;
        long long ans = (long long)(1 + k) * k / 2;
        // cout<<ans<<endl;
        ans = ans % mod;
        n = n - k;
        // if(n<0)n=0;
        long long a = target;
        //if(target%2==0)a++;
        //int x,y;
      // exgcd(2,mod,x,y);
        long long m = km(2, mod - 2);
        cout << m << endl;
        long long r1 = (long long)(2 * a + n - 1) * n % mod;
        // r1=(long long)r1*n%mod;
      //  cout<<r1<<endl;
        r1 = guim(r1, m);
        //cout<<ans<<" "<<r1<<endl;
        ans = (ans + r1) % mod;
        return ans % mod;
    }
};