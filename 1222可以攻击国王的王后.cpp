#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool check(int x) {
        if (x >= 0 && x < 8)return true;
        return false;
    }
    vector<vector<int>> queensAttacktheKing(vector<vector<int>>& queens, vector<int>& king) {
        int dx[8] = { -1,-1,-1,0,1,1,1,0 };
        int dy[8] = { -1,0,1,1,1,0,-1,-1 };
        int st[8] = { 0,0,0,0,0,0,0,0 };
        vector<vector<int>> ans;
        int x = king[0];
        int y = king[1];
        int g[10][10] = { 0 };
        memset(g, 0, sizeof g);
        for (auto it : queens) {
            int a = it[0];
            int b = it[1];
            g[a][b] = 1;
        }

        for (int i = 0; i < 8; i++) {
            for (int j = 1; j <= 8 && check(x + dx[i] * j) && check(y + dy[i] * j); j++) {
                if (!st[i] && g[x + dx[i] * j][y + dy[i] * j] == 1) {
                    st[i] = 1;
                    ans.push_back({ x + dx[i] * j,y + dy[i] * j });
                }
            }
        }
        return ans;
    }
};