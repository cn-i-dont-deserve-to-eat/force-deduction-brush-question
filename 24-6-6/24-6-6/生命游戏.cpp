#define _CRT_SECURE_NO_WARNINGS 1
int dx[] = { -1,-1,0,1,1,1,0,-1 };
int dy[] = { 0,1,1,1,0,-1,-1,-1 };
class Solution {
public:


    void gameOfLife(vector<vector<int>>& board) {
        int n = board.size();
        int m = board[0].size();
        function<int(int, int)> f1 = [&](int x, int y)->int {
            int num = 0;
            for (int i = 0; i < 8; i++) {
                int a = dx[i] + x;
                int b = dy[i] + y;
                if (a >= 0 && a < n && b >= 0 && b < m && (board[a][b] == 1 || board[a][b] == 2 || board[a][b] == 3))num++;
            }
            return num;
            };

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (board[i][j] == 0) {
                    int res = f1(i, j);
                    if (res == 3)board[i][j] = 4;
                    else board[i][j] = 5;
                }
                else {
                    int res = f1(i, j);

                    if (res < 2)board[i][j] = 3;
                    else if (res == 2 || res == 3) board[i][j] = 2;
                    else board[i][j] = 3;
                }
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                // cout<<board[i][j]<<" ";
                if (board[i][j] == 4 || board[i][j] == 2)board[i][j] = 1;
                else if (board[i][j] == 3 || board[i][j] == 5)board[i][j] = 0;
            }
            // cout<<endl;
        }

    }
};