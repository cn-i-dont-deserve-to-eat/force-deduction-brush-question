#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:

    bool isfun(unordered_map<char, int>& mp1, unordered_map<char, int>& mp2) {
        for (auto& it : mp2) {
            if (!mp1.count(it.first) || mp1[it.first] < it.second)return false;
        }
        return true;
    }
    string minWindow(string s, string t) {
        unordered_map<char, int> mp;
        for (auto it : t) {
            mp[it]++;
        }
        int ans = 1e9;
        int n = s.size();
        if (n < t.size())return "";
        int k = t.size();
        int cnt = 0;
        int pos = 0;
        unordered_map<char, int> mp1;
        for (int i = 0, j = 0; i < s.size(); i++) {
            if (mp.count(s[i])) {
                mp1[s[i]]++;
                // cout<<i<<endl;
                while (j <= i && isfun(mp1, mp)) {
                    if (i - j + 1 < ans) {
                        ans = i - j + 1;
                        pos = j;
                    }
                    //cout<<i<<"---"<<endl;
                    if (mp1.count(s[j]))mp1[s[j]]--;
                    j++;
                }

            }
        }
        if (ans == 1e9)return "";
        return s.substr(pos, ans);
    }
};  

//������

class Solution {
public:

    string minWindow(string s, string t) {
        unordered_map<char, int> mp;
        int k = 0;
        for (auto it : t) {
            mp[it]++;
            if (mp[it] == 1)k++;
        }
        int ans = 1e9;
        int n = s.size();
        if (n < t.size())return "";
        int cnt = 0;
        int pos = 0;
        unordered_map<char, int> mp1;
        for (int i = 0, j = 0; i < s.size(); i++) {
            if (mp.count(s[i])) {
                mp1[s[i]]++;
                if (mp1[s[i]] == mp[s[i]])cnt++;
                // cout<<i<<endl;
                while (j <= i && cnt == k) {
                    if (i - j + 1 < ans) {
                        ans = i - j + 1;
                        pos = j;
                    }
                    //cout<<i<<"---"<<endl;
                    if (mp1.count(s[j])) {
                        mp1[s[j]]--;
                        if (mp1[s[j]] < mp[s[j]])cnt--;
                    }
                    j++;
                }

            }
        }
        if (ans == 1e9)return "";
        return s.substr(pos, ans);
    }
};