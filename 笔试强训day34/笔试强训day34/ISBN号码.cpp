#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<string>
#include<vector>
using namespace std;

int main() {
    string str;
    cin >> str;
    int n = str.size();
    int ans = 0;
    int p = 1;
    for (int i = 0; i < n - 2; i++) {
        if (str[i] != '-') {
            int u = str[i] - '0';
            ans += p * u;
            p++;
        }
    }
    int u = str[n - 1] - '0';
    int k = ans % 11;
    if ((str[n - 1] == 'X' && k == 10) || k == u)cout << "Right" << endl;
    else {
        char ch;
        if (k == 10)ch = 'X';
        else ch = k + '0';
        str[n - 1] = ch;
        cout << str << endl;
    }
    return 0;
}
