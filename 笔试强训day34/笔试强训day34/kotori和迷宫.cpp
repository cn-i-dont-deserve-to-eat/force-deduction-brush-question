#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<queue>
#include<unordered_map>
using namespace std;
const int N = 35;
typedef pair<int, int> PII;
char g[N][N];
bool st[N][N];
bool ss[N][N];
int n, m;
int dx[] = { 0,0,-1,1 };
int dy[] = { -1,1,0,0 };

queue<PII> q;
int cnt;
int bfs(int x0, int y0) {
    int ans = 1e9;
    int res = 0;
    q.push({ x0,y0 });
    st[x0][y0] = true;
    while (!q.empty()) {
        int sz = q.size();
        for (int i = 0; i < sz; i++) {
            auto it = q.front();
            q.pop();
            int a = it.first;
            int b = it.second;

            if (ss[a][b]) {
                ans = min(ans, res);
                // cout<<a<<" "<<b<<endl;
                cnt++;
                continue;
            }
            for (int j = 0; j < 4; j++) {
                int x = a + dx[j];
                int y = b + dy[j];
                if (x >= 0 && x < n && y >= 0 && y < m && !st[x][y] && g[x][y] != '*') {
                    q.push({ x,y });
                    st[x][y] = true;
                }
            }

        }
        res++;
    }
    if (cnt == 0)return -1;
    return ans;
}

int main() {
    cin >> n >> m;
    int x0, y0;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cin >> g[i][j];
            if (g[i][j] == 'k') {
                x0 = i, y0 = j;
            }
            if (g[i][j] == 'e') {
                ss[i][j] = true;
            }
        }
    }

    int ans = bfs(x0, y0);
    if (ans == -1)cout << -1 << endl;
    else {
        cout << cnt << " " << ans << endl;
    }


    return 0;
}
