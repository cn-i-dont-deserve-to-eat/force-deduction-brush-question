#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int distanceTraveled(int mainTank, int additionalTank) {
        int s = 0;
        int cnt = 0;
        while (mainTank--) {
            s += 10;
            cnt++;
            if (cnt % 5 == 0 && additionalTank) {
                mainTank++;
                additionalTank--;
            }
        }
        return s;
    }
};