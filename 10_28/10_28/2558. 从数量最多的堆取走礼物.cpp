#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    long long pickGifts(vector<int>& gifts, int k) {
        priority_queue<int> q;
        for (int i = 0; i < gifts.size(); i++) {
            q.push(gifts[i]);
        }
        while (k--) {
            int it = q.top();
            q.pop();
            int a = sqrt(it);
            q.push(a);
        }

        long long ans = 0;
        while (!q.empty()) {
            ans += q.top();
            q.pop();
        }
        return ans;
    }
};