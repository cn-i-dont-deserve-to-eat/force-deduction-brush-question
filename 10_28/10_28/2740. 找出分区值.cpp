#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int findValueOfPartition(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        int mi_d = 1e9;
        for (int i = 1; i < nums.size(); i++)
        {
            if (nums[i] - nums[i - 1] < mi_d) {
                mi_d = nums[i] - nums[i - 1];
            }
        }
        return mi_d;
    }
};