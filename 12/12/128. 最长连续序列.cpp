#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int longestConsecutive(vector<int>& nums) {
        sort(nums.begin(), nums.end());

        int len = nums.size();
        if (len <= 1)return len;
        int ans = 0;
        int i, j;
        int cnt = 0;
        for (i = 1, j = 0; i < len; i++) {
            if (nums[i] - nums[i - 1] == 1 || nums[i] == nums[i - 1]) {
                if (nums[i] == nums[i - 1])cnt++;
                continue;
            }
            ans = max(ans, i - j - cnt);
            j = i;
            cnt = 0;
        }
        //if(nums[len-1]-nums[len-2]==1){
        ans = max(ans, i - j - cnt);
        //}
        return max(ans, 1);
    }
};