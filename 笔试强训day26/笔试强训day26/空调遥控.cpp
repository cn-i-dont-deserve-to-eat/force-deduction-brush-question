#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<algorithm>
using namespace std;
const int N = 1e6 + 10;
int a[N];
int b[N];
int s[N];
int main() {
    int n, p;
    cin >> n >> p;
    int maxn = 0;
    for (int i = 0; i < n; i++) {
        cin >> a[i];
        int l = max(0, a[i] - p);
        int r = min(n - 1, a[i] + p);
        b[l]++;
        b[r + 1]--;
        maxn = max(a[i], maxn);
    }
    int ans = 0;
    for (int i = 0; i <= maxn + p; i++) {
        s[i] = s[i - 1] + b[i];
        ans = max(ans, s[i]);
    }
    cout << ans << endl;
    return 0;
}