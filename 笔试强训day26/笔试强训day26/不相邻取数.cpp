#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
const int N = 1e6 + 10;
int a[N];
long long f[N];
int main() {
    int n;
    cin >> n;
    for (int i = 1; i <= n; i++)cin >> a[i];
    f[1] = a[1];
    for (int i = 2; i <= n; i++) {
        f[i] = max(f[i - 2] + a[i], f[i - 1]);
    }
    cout << f[n] << endl;
    return 0;
}
