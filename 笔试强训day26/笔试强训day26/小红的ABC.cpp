#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<string>
#include<cstring>
using namespace std;
const int N = 110;
bool f[N][N];
int main() {
    string str;
    cin >> str;
    int n = str.size();
    int flag = 0;
    int ans = 1e9;
    for (int len = 1; len <= n; len++) {
        if (flag)break;
        for (int l = 0; l + len - 1 < n; l++) {
            int r = l + len - 1;
            if (len == 1) {
                f[l][r] = true;
                continue;
            }
            if (str[l] == str[r]) {
                if (len == 2)f[l][r] = true;
                else f[l][r] = f[l + 1][r - 1];
            }
            else f[l][r] = false;
            if (f[l][r]) {
                flag = 1;
                ans = len;
                break;
            }
        }
    }
    if (flag)cout << ans << endl;
    else cout << -1 << endl;
    return 0;
}
// 64 λ������� printf("%lld")