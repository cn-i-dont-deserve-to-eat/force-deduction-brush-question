#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool canConstruct(string ransomNote, string magazine) {
        int a[27] = { 0 };
        int b[27] = { 0 };
        int len = ransomNote.size();
        int len2 = magazine.size();
        if (len > len2)return false;
        for (int i = 0; i < len; i++) {
            int u = ransomNote[i] - 'a';
            a[u]++;
        }
        for (int i = 0; i < len2; i++) {
            int u = magazine[i] - 'a';
            b[u]++;
        }
        for (int i = 0; i < 26; i++) {
            if (a[i] == 0)continue;
            if (b[i] < a[i])return false;
        }

        return true;

    }
};