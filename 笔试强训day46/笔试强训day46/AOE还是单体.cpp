#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<algorithm>
using namespace std;
const int N = 2e5 + 10;
int a[N];
int main() {
    int n, x;
    cin >> n >> x;
    long long s = 0;
    for (int i = 0; i < n; i++) {
        cin >> a[i];
        s += a[i];
    }
    sort(a, a + n);
    int res = 0;
    long long ans = 0;
    long long mp2 = 0;
    for (int i = 0; i < n; i++) {
        if (a[i] - res <= 0)continue;
        if ((n - i) >= x) {
            ans += (a[i] - res) * x;
            mp2 += (a[i] - res) * (n - i);
            // cout<<(a[i]-res)*x<<" ";
            res = a[i];
        }
    }

    // cout<<res<<" "<<ans<<" "<<mp2<<endl;

    cout << ans + s - mp2 << endl;


    return 0;
}
