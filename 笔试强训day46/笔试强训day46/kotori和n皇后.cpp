#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<unordered_map>
using namespace std;
const int N = 1e5 + 10;
unordered_map<long long, bool> col, row, d, ud;

int main() {
    int k;
    cin >> k;
    int f = 1e9;
    for (int i = 1; i <= k; i++) {
        long long x, y;
        cin >> x >> y;
        if (col[x] || row[y] || d[y - x] || ud[y + x])f = min(f, i);
        col[x] = true;
        row[y] = true;
        d[y - x] = true;
        ud[x + y] = true;

    }
    int t;
    cin >> t;
    while (t--) {
        int x;
        cin >> x;
        if (x < f)cout << "No" << endl;
        else cout << "Yes" << endl;
    }

    return 0;
}
// 64 λ������� printf("%lld")