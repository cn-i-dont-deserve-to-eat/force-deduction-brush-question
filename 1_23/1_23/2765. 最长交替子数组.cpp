#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int alternatingSubarray(vector<int>& nums) {
        int n = nums.size();
        int ans = 0;
        int i, j;
        int d = 1;
        int cnt = 1;
        for (i = 1; i < n; i++) {
            if (nums[i] - nums[i - 1] == d) {
                d = d * -1;
                cnt++;
                // cout<<1<<endl;
            }
            else if (cnt > 1) {
                //cout<<2<<endl;
                ans = max(ans, cnt);
                cnt = 1;
                d = 1;
                i--;
            }
            // cout<<d<<endl;
        }
        ans = max(ans, cnt);
        if (ans <= 1)return -1;
        return ans;
    }
};