#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool isAcronym(vector<string>& words, string s) {
        int len = words.size();
        if (len != s.size())return false;
        for (int i = 0; i < len; i++) {
            if (words[i][0] != s[i])return false;
        }
        return true;
    }
};