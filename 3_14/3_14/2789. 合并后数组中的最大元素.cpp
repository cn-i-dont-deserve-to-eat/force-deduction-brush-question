#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    long long maxArrayValue(vector<int>& nums) {
        int n = nums.size();
        long long ans = 0;
        long long t = nums[n - 1];
        for (int i = n - 2, j = n - 1; i >= 0; i--) {
            if (nums[i] <= t) {
                t += nums[i];
            }
            else {
                ans = max(ans, t);
                t = nums[i];
            }
        }
        if (t >= nums[0]) {
            ans = max(ans, t);
        }
        return ans;
    }
}; 