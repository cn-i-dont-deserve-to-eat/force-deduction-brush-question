#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool validPartition(vector<int>& nums) {
        int n = nums.size();
        if (n == 1)return false;
        vector<bool> f(n + 1);
        f[0] = true;
        f[1] = false;
        for (int i = 1; i < n; i++) {
            if ((nums[i] == nums[i - 1] && f[i - 1]) || (i >= 2 && nums[i] == nums[i - 1] && nums[i - 1] == nums[i - 2] && f[i - 2])
                || (i >= 2 && f[i - 2] && nums[i] - nums[i - 1] == 1 && nums[i - 1] - nums[i - 2] == 1)) {
                f[i + 1] = true;
            }
        }
        return f[n];
    }
};