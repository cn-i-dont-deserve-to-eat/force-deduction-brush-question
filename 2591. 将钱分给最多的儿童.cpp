#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int distMoney(int money, int children) {
        if (money < children)return -1;
        if (money == 4 && children == 1)return -1;

        int ans = 0;
        if (money > children * 8)return children - 1;

        money -= children;

        ans += money / 7;

        money -= money / 7 * 7;

        if (money == 3 && ans == children - 1)ans--;
        return ans;
    }
};