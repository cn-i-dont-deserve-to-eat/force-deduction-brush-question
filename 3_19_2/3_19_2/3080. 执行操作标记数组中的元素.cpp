#define _CRT_SECURE_NO_WARNINGS 1
typedef pair<int, int> PII;
class Solution {
public:
    vector<long long> unmarkedSumArray(vector<int>& nums, vector<vector<int>>& queries) {
        int len = nums.size();
        vector<bool> st(len, false);
        priority_queue<PII, vector<PII>, greater<PII>>q;
        long long sum = 0;
        for (int i = 0; i < len; i++) {
            sum += nums[i];
            q.push({ nums[i],i });
        }
        int m = queries.size();
        vector<long long> ans;
        for (int i = 0; i < m; i++) {
            int index = queries[i][0];
            int k = queries[i][1];
            if (!st[index]) {
                st[index] = true;
                sum -= nums[index];
                int t = k;
                while (t && !q.empty()) {
                    auto it = q.top();
                    int a = it.first;
                    int b = it.second;
                    q.pop();
                    if (!st[b]) {
                        st[b] = true;
                        sum -= a;
                        t--;
                    }
                }
                ans.push_back(sum);
            }
            else {
                int t = k;
                while (t && !q.empty()) {
                    auto it = q.top();
                    int a = it.first;
                    int b = it.second;
                    q.pop();
                    if (!st[b]) {
                        st[b] = true;
                        sum -= a;
                        t--;
                    }
                }
                ans.push_back(sum);
            }
        }
        return ans;
    }
};