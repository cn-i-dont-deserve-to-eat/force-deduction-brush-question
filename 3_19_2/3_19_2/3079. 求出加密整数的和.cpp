#define _CRT_SECURE_NO_WARNINGS 1
int get_n(int x) {
    int res = 0;
    while (x) {
        res++;
        x /= 10;
    }
    return res;
}
int get_max(int x) {
    int res = 0;
    while (x) {
        res = max(res, x % 10);
        x /= 10;
    }
    return res;
}

int get_enc(int x) {
    int n = get_n(x);
    int a = get_max(x);

    int res = 0;
    while (n--) {
        res = res * 10 + a;
    }
    return res;
}

class Solution {
public:
    int sumOfEncryptedInt(vector<int>& nums) {
        int ans = 0;
        for (auto it : nums) {
            ans += get_enc(it);
        }
        return ans;
    }
};