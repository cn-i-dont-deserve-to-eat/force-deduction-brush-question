
typedef pair<int, char> PII;
class Solution {
public:
    string minimizeStringValue(string s) {
        int len = s.size();
        string ans = s;
        unordered_map<int, int> mp;
        int cnt = 0;
        int cnt1 = 0;
        int f[26] = { 0 };
        vector<int> pos;
        for (int i = 0; i < len; i++) {
            if (s[i] == '?') {
                pos.push_back(i);
                cnt1++;
                continue;
            }
            int u = s[i] - 'a';
            f[u]++;
        }
        priority_queue<PII, vector<PII>, greater<PII>> q;
        for (int i = 0; i < 26; i++) {
            q.push({ f[i],i });
        }
        string res = "";
        while (cnt1) {
            auto it = q.top();
            q.pop();
            char u = it.second + 'a';
            int c = it.first;
            res += u;
            cnt1--;
            c++;
            q.push({ c,u - 'a' });
        }
        sort(res.begin(), res.end());
        for (int i = 0; i < res.size(); i++) {
            ans[pos[i]] = res[i];
        }
        return ans;

    }
};  