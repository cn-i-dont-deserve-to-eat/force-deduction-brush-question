#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;
vector<int> ans;
int main() {
	int t;
	cin >> t;
	while (t--) {
		ans.clear();
		int x, y,n;
		cin >> x >> y >> n;
		int d = y - x;
		if (x > y) {
			cout << -1 << endl;
			continue;
		}
		if (d <= 1) {
			cout << -1 << endl;
			continue;
		}
		if (d == 2) {
				cout << -1 << endl;
				continue;
		}
		int k = n - 2;
		//int s = k * (k + 1) / 2;
		if (d > k) {
			ans.push_back(y);
			int temp = 1;
			for (int i = 1; i <= n-2; i++) {
				ans.push_back(y - temp);
				temp+=i+1;
			}
			ans.push_back(x);
			if (ans[n-2] - ans[n-1] <= ans[n-3] - ans[n-2]) {
				cout << -1 << endl;
				continue;
			}

			for (int i = n-1; i >=0; i--) {
				cout << ans[i] << " ";
			}
			cout << endl;
		}
		else {
			cout << -1 << endl;
		}
	}

	return 0;
}