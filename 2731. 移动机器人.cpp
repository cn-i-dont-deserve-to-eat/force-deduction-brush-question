#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int sumDistance(vector<int>& nums, string s, int d) {
        int mod = 1e9 + 7;
        int len = nums.size();
        for (int i = 0; i < len; i++) {
            if (s[i] == 'L') {
                nums[i] -= d;
            }
            else {
                nums[i] += d;
            }
        }

        long long ans = 0;
        sort(nums.begin(), nums.end());
        for (int i = 1; i < len; i++) {
            long long k = (long long)((long long)nums[i] - nums[i - 1]) * i % mod * (len - i) % mod;
            ans = (ans + k) % mod;
            ans %= mod;
        }
        return ans;
    }
};