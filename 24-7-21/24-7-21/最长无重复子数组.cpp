#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:

    int maxLength(vector<int>& arr) {
        int n = arr.size();
        int ans = 0;
        unordered_map<int, int> mp;
        for (int i = 0, j = 0; i < n; i++)
        {
            mp[arr[i]]++;
            while (mp[arr[i]] > 1)
            {
                mp[arr[j]]--;
                j++;
            }
            ans = max(ans, i - j + 1);
        }
        return ans;
    }
};