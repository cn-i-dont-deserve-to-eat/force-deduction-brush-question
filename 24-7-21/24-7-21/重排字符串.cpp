#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<string>
#include<queue>
#include<vector>
using namespace std;
typedef pair<int, int> PII;
int main() {
    int ch[26] = { 0 };
    string str;
    int n;
    cin >> n;
    cin >> str;
    int mx = 0;
    for (int i = 0; i < n; i++) {
        int u = str[i] - 'a';
        ch[u]++;
        mx = max(ch[u], mx);
    }
    if (mx - (n - mx) > 1) {
        cout << "no" << endl;
        return 0;
    }
    priority_queue<PII> q;

    for (int i = 0; i < 26; i++) {
        if (ch[i]) q.push({ ch[i],i });
    }
    string ans;
    while (q.size() > 1)
    {
        auto t1 = q.top();
        q.pop();
        int a1 = t1.first;
        int b1 = t1.second;

        auto t2 = q.top();
        q.pop();
        int a2 = t2.first;
        int b2 = t2.second;
        ans += (b1 + 'a');
        ans += (b2 + 'a');

        if (a1 - 1)q.push({ a1 - 1,b1 });
        if (a2 - 1)q.push({ a2 - 1,b2 });
    }
    if (q.size() && q.top().first > 1)
    {
        cout << "no" << endl;
    }
    else {
        if (q.size())ans += (q.top().second + 'a');
        cout << "yes" << endl;
        cout << ans << endl;
    }

    return 0;
}
