#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<string>
using namespace std;

int main() {
    int t;
    cin >> t;
    while (t--)
    {
        int n, k;
        cin >> n >> k;
        string str;
        cin >> str;
        int ans = 0;
        for (int i = 0, j = 0; i < n; i++)
        {
            if (str[i] == 'L') {
                ans--;
                j = i + 1;
            }
            else {
                if (i - j + 1 >= 3)ans += k;
                else ans++;
            }
        }
        cout << ans << endl;

    }

    return 0;
}
