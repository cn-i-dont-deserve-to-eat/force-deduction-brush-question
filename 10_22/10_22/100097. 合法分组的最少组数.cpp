class Solution {
public:
    int minGroupsForValidAssignment(vector<int>& nums) {
        map<int, int> mp;
        int len = nums.size();
        int mi = 1e9 + 10;
        for (auto it : nums) {
            mp[it]++;
        }
        for (auto it : mp) {
            mi = min(it.second, mi);
        }
        int ans = 1e9;
        for (int k = 1; k <= mi; k++) {
            int tn = 0;
            int f = 1;
            for (auto it : mp) {
                int cnt = it.second;
                int a = cnt / (k + 1);
                int b = cnt - a * (k + 1);
                if (cnt % (k + 1) == 0) {
                    tn += a;
                }
                else if (a + b >= k) {
                    tn += a + 1;
                }
                else {
                    int a = cnt / (k);
                    int b = cnt - a * (k);
                    if (b > a) {
                        f = 0;
                        break;
                    }
                    tn += a;
                }
            }
            if (f) ans = min(ans, tn);
            cout << ans << endl;
        }

        return ans;
    }
};