#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int minimumSum(vector<int>& nums) {
        //int ans=0;
        int len = nums.size();
        int lmi = nums[0];
        vector<int> s(len + 10);
        s[len - 1] = nums[len - 1];
        for (int i = len - 2; i >= 0; i--) {
            s[i] = min(nums[i], s[i + 1]);
        }
        int ans = 1e9;
        for (int i = 1; i < len - 1; i++) {
            if (nums[i] > lmi && nums[i] > s[i + 1]) {
                ans = min(ans, nums[i] + lmi + s[i + 1]);
            }
            lmi = min(lmi, nums[i]);
        }
        if (ans == 1e9)return -1;
        return ans;
    }
};