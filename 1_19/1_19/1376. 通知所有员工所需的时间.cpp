#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:

    int dfs(int u, vector<vector<int>>& g, vector<int>& informTime) {
        if (informTime[u] == 0)return 0;
        int ma = 0;
        for (auto it : g[u]) {
            ma = max(ma, dfs(it, g, informTime));
        }
        return informTime[u] + ma;
    }
    int numOfMinutes(int n, int headID, vector<int>& manager, vector<int>& informTime) {
        vector<vector<int>> g(n);
        for (int i = 0; i < n; i++) {
            if (manager[i] == -1)continue;
            g[manager[i]].push_back(i);
        }

        return dfs(headID, g, informTime);
    }
};