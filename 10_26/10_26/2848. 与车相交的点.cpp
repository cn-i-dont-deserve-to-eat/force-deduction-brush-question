#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int numberOfPoints(vector<vector<int>>& nums) {
        int len = nums.size();
        vector<int> s(110);
        int mx = 0;
        for (int i = 0; i < len; i++) {
            int a = nums[i][0];
            int b = nums[i][1];
            mx = max(mx, b);

            s[a]++;
            s[b + 1]--;
        }
        //    for(int i=1;i<=mx;i++){
        //        cout<<s[i]<<" "<<i<<endl;
        //    }
        vector<int> s1(110);
        for (int i = 1; i <= mx; i++) {
            s1[i] = s[i] + s1[i - 1];
            //  cout<<s1[i]<<" "<<i<<endl;
        }
        int cnt = 0;
        for (int i = 1; i <= mx; i++) {
            if (s1[i] >= 1)cnt++;
        }
        return cnt;
    }
};