#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int countDigits(int num) {
        int ans = 0;
        int k = num;
        while (k) {
            int b = k % 10;
            if (num % b == 0)ans++;
            k /= 10;
        }
        return ans;
    }
};