#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool isfun(int x) {
        int k = log(x) / log(10) + 1;
        //cout<<x<<"--- "<<k<<endl;
        if (k % 2)return false;
        int a = 0;
        int b = 0;
        for (int i = 1; i <= k; i++) {
            if (i <= k / 2) {
                a += x % 10;
                x /= 10;
            }
            else {
                b += x % 10;
                x /= 10;
            }
        }
        if (a == b)return true;
        return false;
    }
    int countSymmetricIntegers(int low, int high) {
        int ans = 0;
        for (int i = low; i <= high; i++) {
            if (isfun(i)) {
                // cout<<i<<endl;
                ans++;
            }
        }
        return ans;
    }
};