#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool isReachableAtTime(int sx, int sy, int fx, int fy, int t) {
        int dx = abs(fx - sx);
        int dy = abs(fy - sy);
        int k = min(dx, dy);
        int min_s = k + max(dx, dy) - k;
        // cout<<min_s<<endl;
        if (min_s == 0) {
            if (t == 1)return false;
            else return true;
        }
        if (t < min_s)return false;
        return true;

    }
};