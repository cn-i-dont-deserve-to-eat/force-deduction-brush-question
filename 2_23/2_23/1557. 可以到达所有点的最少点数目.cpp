#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<int> findSmallestSetOfVertices(int n, vector<vector<int>>& edges) {
        // int m=edges.size();
        // int n=edges[0].size();

        vector<int> d(n + 1, 0);
        for (auto it : edges) {
            d[it[1]]++;
        }
        vector<int> ans;
        for (int i = 0; i < n; i++)if (!d[i])ans.push_back(i);
        return ans;

    }
};