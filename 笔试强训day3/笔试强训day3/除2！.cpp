#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<queue>
using namespace std;

int main() {
    int n, k;
    cin >> n >> k;
    long long sum = 0;
    priority_queue<int> q;
    while (n--) {
        int x;
        cin >> x;
        sum += x;
        if (x % 2 == 0)q.push(x);
    }

    while (!q.empty() && k) {
        int it = q.top();
        q.pop();
        it /= 2;
        sum -= it;
        k--;
        if (it % 2 == 0)q.push(it);
    }
    cout << sum;
    return 0;
}