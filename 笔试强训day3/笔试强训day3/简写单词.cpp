#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<string>
using namespace std;

int main() {
    string  str;
    string ans;
    getline(cin, str);
    if (str[0] > 'Z')ans += (str[0] - 32);
    else ans += str[0];
    for (int i = 1; i < str.size(); i++) {
        if (str[i] != ' ' && str[i - 1] == ' ') {
            if (str[i] > 'Z')ans += (str[i] - 32);
            else ans += str[i];
        }
    }
    cout << ans;

    return 0;
}
