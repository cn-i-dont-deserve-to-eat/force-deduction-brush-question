#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
typedef long long ll;
const int N = 1e7 + 10;
ll s[N];
int main() {
    int n;
    int x;
    cin >> n >> x;
    for (int i = 1; i <= n; i++) {
        int k;
        cin >> k;
        s[i] = s[i - 1] + k;
    }
    int ans = 1e9;
    int star = 1;
    for (int i = 1; i <= n; i++) {
        int l = i - 1;
        int r = n + 1;
        while (l + 1 != r) {
            int mid = (l + r) >> 1;
            if (s[mid] - s[i - 1] >= x)r = mid;
            else l = mid;
        }
        if (s[r] - s[i - 1] >= x) {
            if (r - i + 1 < ans) {
                ans = r - i + 1;
                star = i;
            }
        }
    }
    cout << star << " " << star + ans - 1;
    return 0;
}