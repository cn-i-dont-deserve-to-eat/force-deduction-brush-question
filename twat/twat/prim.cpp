#include<iostream>
#include<map>
#include<algorithm>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<string>
using namespace std;
const int N = 2e6 + 10, M = 2e7 + 10;
map<string, char>mp;
typedef long long LL;
int prime[M], st[M];
int n, m;
int cnt;
void getprim(int n) {
	for (int i = 2; i <= n; i++) {
		if (!st[i]) {
			prime[cnt++] = i;
		}
		for (int j = 0; i <= n / prime[j]; j++) {
			st[prime[j] * i] = true;
			if (i % prime[j] == 0) break;
		}
	}
}
bool fun(int x)
{
	for (int i = 2; i <= x/i; i++) {
		{
			if (x % i == 0)
			{
				return 0;
			}
		}

	}
	return 1;
}
int main()
{
	//scanf("%d", &n);
	cin >> n;
    getprim(n);
	//cout << "cnt==" << cnt << endl;
	int ans = 0;
	//cout << "m==" << m << endl;
	for (int i = 0; i < cnt; i++) {
		int x = n - prime[i];
		//cout << "prime===" << prime[i] << endl;
		if (x <= 0)break;
		//cout << "x==" << x << endl;
		if (x == 1) {
			ans++;
			continue;
		}
		if (fun(x))continue;
		int cnt1 = 0;
		for (int j = 1; j  <= x/j; j++) {
			if (x % j == 0) {
				x /= j;
				cnt1++;
			}
		}
		//cout << "cnt==" << cnt << endl;
		int z = sqrt(x);
		if (z * z == x) {
			cnt1--;
			cnt1 *= 2;
			cnt1++;
		}
		else cnt1 *= 2;

		//cout << "cnt==" << cnt << endl;
		ans += cnt1;
	}
	printf("%d\n", ans);
	return 0;
}
