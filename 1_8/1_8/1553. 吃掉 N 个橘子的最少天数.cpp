#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
    long long fun(int n, unordered_map<long long, int>& mp) {
        if (mp.count(n))return mp[n];
        if (n <= 1)return n;
        long long res = n;
        res = min(res, fun(n / 2, mp) + n % 2 + 1);
        res = min(res, fun(n / 3, mp) + n % 3 + 1);
        mp[n] = res;
        return mp[n];
    }
public:


    int minDays(int n) {
        unordered_map<long long, int> mp;
        return fun(n, mp);
    }
};