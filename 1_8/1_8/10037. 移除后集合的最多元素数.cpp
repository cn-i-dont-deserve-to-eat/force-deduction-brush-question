#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int maximumSetSize(vector<int>& nums1, vector<int>& nums2) {
        unordered_map<int, int> mp1;
        unordered_map<int, int> mp2;
        int len1 = nums1.size();
        int len2 = nums2.size();
        int res1 = 0;
        for (int i = 0; i < len1; i++) {
            if (!mp1[nums1[i]]) {
                mp1[nums1[i]]++;
                res1++;
            }
        }
        int res2 = 0;
        int res = 0;
        for (int i = 0; i < len2; i++) {
            if (!mp2[nums2[i]]) {
                mp2[nums2[i]]++;
                res2++;
            }
        }
        for (auto it : mp1) {
            if (mp2[it.first])res++;
        }
        //res1=mp1.size();
       // res2=mp2.size();
        cout << res1 << " " << res2 << " " << res << endl;
        if (mp1.size() <= len1 / 2 && mp2.size() <= len2 / 2)return res1 + res2 - res;
        if (mp1.size() <= len1 / 2) {
            res2 -= res;
            if (res2 > len2 / 2) {
                return res1 + len2 / 2;
            }
            else {
                return res1 + res2;
            }
        }
        if (res2 <= len2 / 2) {
            res1 -= res;
            if (res1 > len1 / 2) {
                return res2 + len1 / 2;
            }
            else {
                return res1 + res2;
            }
        }
        else {
            int ans = res1 + res2 - res;
            int mn = min(res, res1 - len1 / 2);
            if (res1 > len1 / 2)ans -= (res1 - len1 / 2) - mn;
            res -= mn;
            res2 -= min(res, res2 - len1 / 2);
            if (res2 > len1 / 2)ans -= (res2 - len1 / 2);
            return ans;

        }
    }
};