#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<int> distinctDifferenceArray(vector<int>& nums) {
        int n = nums.size();
        vector<int> s1(n);
        unordered_map<int, int> mp;

        vector<int> s2(n + 1);
        int cur = 0;

        for (int i = 0; i < n; i++) {
            if (!mp[nums[i]]) {
                cur++;
                mp[nums[i]]++;
            }
            s1[i] = cur;
            // cout<<s1[i]<<endl;
        }
        mp.clear();
        cur = 0;
        for (int i = n - 1; i >= 0; i--) {
            if (!mp[nums[i]]) {
                cur++;
                mp[nums[i]]++;
            }
            s2[i] = cur;
        }
        // cout<<cur<<endl;
        vector<int> ans(n);
        for (int i = 0; i < n; i++) {
            ans[i] = s1[i] - s2[i + 1];
        }
        return ans;
    }
};