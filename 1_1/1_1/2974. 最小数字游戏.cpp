#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<int> numberGame(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        int len = nums.size();
        for (int i = 0; i < len; i += 2) {
            swap(nums[i], nums[i + 1]);
        }
        return nums;
    }
};