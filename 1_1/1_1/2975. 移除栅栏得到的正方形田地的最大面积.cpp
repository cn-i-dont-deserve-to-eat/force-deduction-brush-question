#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int maximizeSquareArea(int m, int n, vector<int>& hFences, vector<int>& vFences) {
        int mod = 1e9 + 7;

        if (m == n)return (long long)(m - 1) * (n - 1) % mod;
        // int ans=0;
        unordered_map<int, int> mp;
        hFences.push_back(1);
        hFences.push_back(m);
        vFences.push_back(1);
        vFences.push_back(n);
        sort(hFences.begin(), hFences.end());
        sort(vFences.begin(), vFences.end());
        int len1 = hFences.size();
        int len2 = vFences.size();
        for (int i = 0; i < len1; i++) {
            for (int j = i + 1; j < len1; j++) {
                mp[abs(hFences[j] - hFences[i])]++;
                // cout<<m<<" "<<hFences[j]-hFences[i]<<endl;
            }
        }

        long long ans = 0;

        for (int i = 0; i < len2; i++) {
            for (int j = i + 1; j < len2; j++) {
                int d = abs(vFences[j] - vFences[i]);
                // cout<<m<<" "<<d<<endl;
                if (mp[d]) {
                    long long res = (long long)d * d;
                    if (res > ans)ans = res;
                }
            }
        }
        if (ans == 0)ans = -1;
        return ans % mod;
    }
};