#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    string capitalizeTitle(string title) {
        int n = title.size();
        for (int j = 0, i = 0; i < n; i++) {
            if (title[i] == ' ' || i == n - 1) {
                if (i == n - 1 && title[i] != ' ')i++;
                if (i - j <= 2) {
                    while (j < i) {
                        if (title[j] >= 'A' && title[j] <= 'Z')title[j] += 32;
                        j++;
                    }
                    j++;
                }
                else {
                    if (title[j] >= 'a' && title[j] <= 'z')title[j] -= 32;
                    j++;
                    while (j < i) {
                        if (title[j] >= 'A' && title[j] <= 'Z')title[j] += 32;
                        j++;
                    }
                    j++;
                }
            }
            //  cout<<j<<" "<<i<<endl;
        }
        return title;
    }
};