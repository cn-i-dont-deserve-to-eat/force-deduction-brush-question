#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>

int caculate(char* s) {
    int x = 0, y = 0, coins = 0;
    for (int i = 0; s[i] != '\0'; i++) {
        if (s[i] == 'U') {
            y++;
        }
        else if (s[i] == 'R') {
            x++;
        }
        if (x == y) {
            coins++;
        }
    }
    return coins - 1;
}

int main() {
    char s[100] = { 0 };
    int n = 0;
    scanf("%d", &n);
    getchar();
    scanf("%s", s);

    int coins = calculate(s);
    printf("%d", coins);
    return 0;
}