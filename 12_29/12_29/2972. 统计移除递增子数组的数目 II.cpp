#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    long long incremovableSubarrayCount(vector<int>& nums) {
        int len = nums.size();
        int leftend = 0;
        while (leftend < len - 1 && nums[leftend] < nums[leftend + 1]) {
            leftend++;
        }
        if (leftend == len - 1)return len * (len + 1) / 2;
        int rightstar = len - 1;
        while (nums[rightstar] > nums[rightstar - 1]) {
            rightstar--;
        }
        long long ans = len - rightstar + 1;
        for (int i = 0; i <= leftend; i++) {
            int targe = nums[i] + 1;
            int l = rightstar - 1;
            int r = len;
            while (l + 1 != r) {
                int mid = (l + r) >> 1;
                if (nums[mid] >= targe)r = mid;
                else l = mid;
            }
            ans += len - r + 1;
        }
        return ans;
    }
};