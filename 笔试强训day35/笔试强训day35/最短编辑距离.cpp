#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
const int N = 1100;
int f[N][N];

int main() {
    string a, b;
    cin >> a >> b;

    int n = a.size();
    int m = b.size();
    for (int i = 1; i <= n; i++) {
        f[i][0] = i;
    }

    for (int j = 1; j <= m; j++) {
        f[0][j] = j;
    }
    f[0][0] = 0;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            if (a[i - 1] == b[j - 1]) {
                f[i][j] = min(f[i - 1][j - 1], min(f[i - 1][j] + 1, f[i][j - 1] + 1));
            }
            else {
                f[i][j] = min(min(f[i - 1][j], f[i][j - 1]), f[i - 1][j - 1]) + 1;
            }
        }
    }
    cout << f[n][m] << endl;
    return 0;
}
