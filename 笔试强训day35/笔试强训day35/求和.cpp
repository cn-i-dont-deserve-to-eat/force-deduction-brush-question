#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<vector>
#include<algorithm>
using namespace std;
bool st[10];
void dfs(int u, vector<int>& t, int n, int m, vector<vector<int>>& ans, int s) {
    if (s == m) {
        ans.push_back(t);
        return;
    }
    if (u == n || s > m)return;

    for (int i = 0; i < n; i++) {
        if (!st[i + 1] && s + i + 1 <= m) {
            st[i + 1] = true;
            t.push_back(i + 1);
            dfs(u + 1, t, n, m, ans, s + i + 1);
            t.pop_back();
            st[i + 1] = false;
        }
    }
}

int main() {
    int n, m;
    cin >> n >> m;
    vector<int> t;
    vector<vector<int>> ans;
    dfs(0, t, n, m, ans, 0);
    for (auto& it : ans) {
        sort(it.begin(), it.end());
    }
    sort(ans.begin(), ans.end());
    ans.erase(unique(ans.begin(), ans.end()), ans.end());
    for (auto& it : ans) {
        for (auto tt : it) {
            cout << tt << " ";
        }
        cout << endl;
    }
    return 0;

}
