#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int findChampion(vector<vector<int>>& grid) {
        int m = grid.size();
        int n = grid[0].size();
        for (int i = 0; i < n; i++) {
            int f = 0;
            for (int j = 0; j < m; j++) {
                if (grid[j][i] == 1) {
                    f = 1;
                    break;
                }
            }
            if (!f)return i;
        }
        return 0;

    }
};