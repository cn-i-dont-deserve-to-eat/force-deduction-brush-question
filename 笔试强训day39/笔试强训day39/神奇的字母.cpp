#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<string>
using namespace std;

int main() {
    string str;
    int a[26] = { 0 };
    char ans = 'a';
    int res = 0;
    while (getline(cin, str)) {
        //cout<<1<<endl;
        for (auto it : str) {
            if (it == ' ')continue;
            int u = it - 'a';
            a[u]++;
            if (a[u] > res) {
                res = a[u];
                ans = u + 'a';
            }
        }
    }
    cout << ans << endl;
    return 0;
}
