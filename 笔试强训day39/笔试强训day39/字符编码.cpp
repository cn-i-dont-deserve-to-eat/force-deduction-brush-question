#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<unordered_map>
#include<queue>
#include<vector>
using namespace std;

int main() {
    string str;
    while (cin >> str) {
        unordered_map<char, int> mp;
        for (auto it : str) {
            mp[it]++;
        }
        // vector<int> res;
        priority_queue<int, vector<int>, greater<int>> q;
        for (auto it : mp) {
            // res.push_back(it.second);
            q.push(it.second);
        }

        int ans = 0;
        if (q.size() == 1)ans += q.top();
        while (!q.empty() && q.size() > 1) {
            int ch1 = q.top();
            q.pop();
            int ch2 = q.top();
            q.pop();
            ans += ch1 + ch2;
            q.push(ch1 + ch2);
        }
        cout << ans << endl;

    }


    return 0;
}
