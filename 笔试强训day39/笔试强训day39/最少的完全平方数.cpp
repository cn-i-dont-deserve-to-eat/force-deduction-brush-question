#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<vector>
#include<cstring>
using namespace std;
const int N = 1e4 + 10;
int dp[110][N];
int main() {
    int n;
    cin >> n;
    vector<int> res;
    for (int i = 1; i <= n / i; i++) {
        res.push_back(i * i);
        //  cout<<i*i<<" "<<endl;
        if (i * i == n) {
            cout << 1 << endl;
            return 0;
        }
    }
    memset(dp, 0x3f, sizeof dp);
    dp[0][0] = 0;
    int m = res.size();
    for (int i = 1; i <= m; i++) {
        for (int j = 0; j <= n; j++) {
            for (int k = 0; k * res[i - 1] <= j; k++) {
                dp[i][j] = min(dp[i - 1][j], dp[i - 1][j - k * res[i - 1]] + k);
            }
        }
    }
    cout << dp[m][n] << endl;
    return 0;
}
