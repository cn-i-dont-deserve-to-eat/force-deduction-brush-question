#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
const int N = 510;
char g[N][N];
int f[N][N];



int main() {
    int n, m;
    cin >> n >> m;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            cin >> g[i][j];
        }
    }

    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            int k = 0;
            if (g[i][j] == 'l')k = 4;
            else if (g[i][j] == 'o')k = 3;
            else if (g[i][j] == 'v')k = 2;
            else if (g[i][j] == 'e')k = 1;

            f[i][j] = max(f[i - 1][j] + k, f[i][j - 1] + k);
        }
    }

    cout << f[n][m] << endl;
    return 0;
}
