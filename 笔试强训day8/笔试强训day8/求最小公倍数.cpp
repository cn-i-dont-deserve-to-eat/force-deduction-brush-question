#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

int gcd(int a, int b) {
    return b == 0 ? a : gcd(b, a % b);
}

int main() {
    int a, b;
    cin >> a >> b;
    cout << (long long)a * b / gcd(a, b) << endl;
    return 0;
}
