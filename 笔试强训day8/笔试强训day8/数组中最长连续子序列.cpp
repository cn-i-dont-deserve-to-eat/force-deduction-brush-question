#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:

    int MLS(vector<int>& arr) {

        if (arr.size() == 1)return 1;

        sort(arr.begin(), arr.end());
        arr.erase(unique(arr.begin(), arr.end()), arr.end());
        int ans = 0;
        int i = 1, j = 0;
        int n = arr.size();
        for (; i < n; i++) {
            if (arr[i] - arr[i - 1] != 1) {
                ans = max(ans, i - j);
                j = i;
            }
        }
        if (arr[n - 1] - arr[n - 2] == 1) {
            ans = max(ans, i - j);
        }

        return ans;
    }
};