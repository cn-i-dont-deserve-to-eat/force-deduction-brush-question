#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int getp(int x) {
        int res = 0;
        while (x) {
            x /= 10;
            res++;
        }
        return res;
    }
    vector<int> countNumbers(int cnt) {
        vector<int> ans;
        for (int i = 1;; i++) {
            if (getp(i) >= cnt + 1)break;
            ans.push_back(i);
        }
        return ans;
    }
};