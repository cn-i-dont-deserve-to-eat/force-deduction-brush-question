#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int minStoneSum(vector<int>& piles, int k) {
        int len = piles.size();
        priority_queue<int> q;
        int s = 0;
        for (int i = 0; i < len; i++)q.push(piles[i]), s += piles[i];
        while (k--) {
            int t = q.top();
            // cout<<t<<endl;
            q.pop();
            q.push((t - t / 2));
            s -= t / 2;
        }
        return s;
    }
};