#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>

using namespace std;
const int N = 6e5 + 100;
int a[N];
int main() {
    int n;
    cin >> n;
    string str;
    cin >> str;
    str += str;
    for (int i = 1; i <= 2 * n; i++) {
        a[i] = a[i - 1] + (str[i - 1] == '1' ? 1 : 0);
    }

    int ans = 0;
    for (int l = 1; l <= n; l++) {
        int r = l + n - 1;
        int mid = l + n / 2 - 1;
        if (a[r] - a[mid] == a[mid] - a[l - 1]) {
            ans++;
        }
    }
    cout << ans << endl;

    return 0;
}
