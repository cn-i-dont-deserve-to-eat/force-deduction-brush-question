#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<cstring>
using namespace std;
const int N = 110;
int a[N][N];
int f[N];
int main() {
    int n;
    cin >> n;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            cin >> a[i][j];
            a[i][j] += a[i][j - 1];
        }
    }
    int ans = -1e9;
    for (int a1 = 1; a1 <= n; a1++) {
        for (int a2 = a1; a2 <= n; a2++) {
            memset(f, 0, sizeof f);
            for (int b1 = 1; b1 <= n; b1++) {
                int s = a[b1][a2] - a[b1][a1 - 1];
                f[b1] = max(f[b1 - 1] + s, s);
                ans = max(f[b1], ans);
            }
        }
    }
    cout << ans << endl;
    return 0;
}
