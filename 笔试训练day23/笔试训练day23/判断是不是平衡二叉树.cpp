#define _CRT_SECURE_NO_WARNINGS 1

class Solution {
public:
    int getdeth(TreeNode* root) {
        if (root == nullptr)return 0;
        return max(getdeth(root->left), getdeth(root->right)) + 1;
    }
    bool IsBalanced_Solution(TreeNode* pRoot) {
        if (pRoot == nullptr)return true;
        if (abs(getdeth(pRoot->left) - getdeth(pRoot->right)) > 1) {
            return false;
        }
        else return IsBalanced_Solution(pRoot->left) && IsBalanced_Solution(pRoot->right);
    }
};


class Solution {
public:
    bool dfs(TreeNode* root, int* depth) {
        if (root == nullptr) {
            *depth = 0;
            return true;
        }
        //先判断左子树
        int leftdepth = 0;
        if (dfs(root->left, &leftdepth) == false) return false;
        int rightdepth = 0;
        if (dfs(root->right, &rightdepth) == false) return false;

        //再判断当前树,当前树是否满足平衡二叉树，看左右子树高度差的绝对值是否>1
        if (abs(leftdepth - rightdepth) > 1)return false;
        //左右子树，当前树都满足平衡二叉树的前提下才计算深度并返回true
        *depth = max(leftdepth, rightdepth) + 1;
        return true;

    }
    bool IsBalanced_Solution(TreeNode* pRoot) {
        int n = 0;
        return dfs(pRoot, &n);
    }
};