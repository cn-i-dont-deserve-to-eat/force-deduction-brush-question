#define _CRT_SECURE_NO_WARNINGS 1

struct Node* copyRandomList(struct Node* head) {
    if (head == NULL)return NULL;
    struct Node* phead = head;
    struct Node* newhead = (struct Node*)malloc(sizeof(struct Node));//新链的头指针
    struct Node* np = newhead;
    struct Node* star = newhead;
    while (phead) {
        struct Node* t = phead->next;//临时变量，存放phead的下一个节点，防止丢失
        newhead->val = phead->val;
        newhead->random = phead->random;//为了方便后续建立random联系
        phead->next = newhead;//哈希的思想，建立新、旧两个节点的联系
        if (t == NULL) {//如果下一个节点是NULL,就不用开辟空间了
            newhead->next = NULL;
        }
        else {//否则就创造一个节点空间，建立next关系
            newhead->next = (struct Node*)malloc(sizeof(struct Node));
            newhead = newhead->next;
        }
        phead = t;
    }

    while (np != NULL) {
        if (np->random != NULL) {
            np->random = np->random->next;//通过旧链找到新链应该指向的节点
        }
        np = np->next;
    }
    return star;

}