#define _CRT_SECURE_NO_WARNINGS 1
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode* detectCycle(struct ListNode* head) {
    if (head == NULL)return NULL;
    struct ListNode* slow = head;
    struct ListNode* fast = head;
    struct ListNode* meet = NULL;
    struct ListNode* star = head;
    while (fast && fast->next) {
        slow = slow->next;
        fast = fast->next->next;
        if (slow == fast) {
            meet = slow;
            break;
        }
    }
    if (meet) {
        while (star != meet) {
            star = star->next;
            meet = meet->next;
        }
        return meet;
    }
    else {
        return NULL;
    }


}