#define _CRT_SECURE_NO_WARNINGS 1
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB) {
    struct ListNode* endA = headA;
    struct ListNode* endB = headB;
    struct ListNode* starA = headA;
    struct ListNode* starB = headB;
    int numA = 1;
    int numB = 1;
    while (endA->next) {
        endA = endA->next;
        numA++;
    }
    while (endB->next) {
        endB = endB->next;
        numB++;
    }

    if (endA != endB)return NULL;
    int d = abs(numA - numB);
    if (numA > numB) {
        while (d--) {
            starA = starA->next;
        }
    }
    else {
        while (d--) {
            starB = starB->next;
        }
    }
    struct ListNode* meet = NULL;
    while (starA != starB) {
        starA = starA->next;
        starB = starB->next;
    }
    meet = starA;
    return meet;
}