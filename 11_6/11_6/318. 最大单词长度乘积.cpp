#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int maxProduct(vector<string>& words) {
        int len = words.size();
        int k = 1;
        // for(int i=1;i<=26;i++){
        //    k=k*2;
        // }
        // k--;
        map<string, int>mp;
        map<string, int> mp2;
        for (auto it : words) {
            if (mp[it])continue;
            int k1 = 0;
            for (int i = 0; i < it.size(); i++) {
                int u = it[i] - 'a';
                k1 |= (1 << u);
            }
            mp[it] = k1;
            mp[it] = it.size();
        }
        int ans = 0;
        for (int i = 0; i < len; i++) {
            for (int j = i + 1; j < len; j++) {
                // if(mp[words[i]]==k||mp[words[j]]==k)continue;
                if ((mp[words[i]] & mp[words[j]]) == 0) {
                    ans = max(ans, (int)(words[i].size() * words[j].size()));
                }

            }
        }
        return ans;


    }
};