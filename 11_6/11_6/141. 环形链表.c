#define _CRT_SECURE_NO_WARNINGS 1
bool hasCycle(struct ListNode* head) {
    if (head == NULL)return NULL;
    struct ListNode* slow = head;
    struct ListNode* fast = head;
    while (fast && fast->next) {
        slow = slow->next;
        fast = fast->next->next;
        if (slow == fast) {
            return true;
        }
    }
    return false;
}