#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int singleNumber(vector<int>& nums) {
        long long ans = 0;
        for (int i = 31; i >= 0; i--) {
            int res = 0;
            for (auto it : nums) {
                if ((it >> i) & 1) {
                    res += 1;
                }
            }
            if (res % 3) {
                ans = ans * 2 + 1;
            }
            else {
                ans = ans * 2;
            }
        }
        return ans;
    }
};