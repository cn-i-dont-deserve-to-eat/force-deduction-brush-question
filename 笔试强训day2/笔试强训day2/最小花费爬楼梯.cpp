#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
const int N = 1e5 + 10;
int a[N];//从1开始爬
int b[N];//从0开始爬
int c[N];//花费
int main() {
    int n;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> c[i];
    }
    a[1] = 0;
    a[2] = c[1];//初始化
    for (int i = 3; i <= n; i++) {
        a[i] = min(a[i - 1] + c[i - 1], a[i - 2] + c[i - 2]);
    }
    b[0] = 0;
    b[1] = b[0] + c[0];//初始化
    for (int i = 2; i <= n; i++) {
        b[i] = min(b[i - 1] + c[i - 1], b[i - 2] + c[i - 2]);
    }
    cout << min(b[n], a[n]);
    return 0;
}
