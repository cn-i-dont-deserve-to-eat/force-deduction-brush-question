#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<string>
#include<algorithm>
using namespace std;

int main() {
    string a, b, c;
    int f1 = 0;
    int f2 = 0;
    int p1 = 0;
    int p2 = 0;
    int n;
    cin >> n;
    cin >> a >> b;

    int ans = 1e9;
    for (int i = 1; i <= n; i++) {
        cin >> c;
        if (c == a) {
            p1 = i;
            f1 = 1;
        }
        else if (c == b) {
            p2 = i;
            f2 = 1;
        }

        if (f1 && f2) {//两个都找到了
            ans = min(ans, abs(p2 - p1));
        }
    }
    if (ans == 1e9)cout << -1;
    else cout << ans;
    return 0;
}
