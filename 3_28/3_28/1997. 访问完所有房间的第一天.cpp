#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int firstDayBeenInAllRooms(vector<int>& nextVisit) {
        int mod = 1e9 + 7;
        int n = nextVisit.size();
        vector<long long> s(n + 1);

        for (int i = 0; i < n - 1; i++) {
            int j = nextVisit[i];
            s[i + 1] = (2 * s[i] - s[j] + 2 + mod) % mod;
        }
        return s[n - 1];
    }
};