#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int similarPairs(vector<string>& words) {
        int n = words.size();
        vector<int> f(n);

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < words[i].size(); j++) {
                int u = words[i][j] - 'a';
                f[i] |= (1 << u);
            }
        }
        int ans = 0;
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (f[i] == f[j])ans++;
            }
        }
        return ans;
    }
};