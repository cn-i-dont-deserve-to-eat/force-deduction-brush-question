#define _CRT_SECURE_NO_WARNINGS 1
typedef pair<int, int> PII;
class Solution {
public:
    bool isPossible(int n, vector<vector<int>>& edges) {
        vector<int> d(n + 1);
        unordered_set<int> g[n + 1];
        for (auto it : edges) {
            int a = it[0];
            int b = it[1];
            d[a]++;
            d[b]++;
            g[a].insert(b);
            g[b].insert(a);
        }
        vector<int> e;
        // int t1=-1,t2=-1;
        for (int i = 1; i <= n; i++) {
            if (d[i] % 2) {
                e.push_back(i);
            }
        }
        cout << e.size();
        if (e.size() > 4)return false;
        if (e.size() == 1)return false;
        if (e.size() == 3)return false;
        if (e.size() == 0)return true;
        if (e.size() == 2) {
            if (!g[e[0]].count(e[1]))return true;
            for (int i = 1; i <= n; i++) {
                if (i == e[0] || i == e[1])continue;
                if (!g[i].count(e[0]) && !g[i].count(e[1]))return true;
            }
            return false;
        }
        //  return false;

        vector<vector<int>> res(200, vector<int>(4));
        int cnt = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (i == j)continue;
                for (int k = 0; k < 4; k++) {
                    if (k == j || k == i)continue;
                    for (int l = 0; l < 4; l++) {
                        if (l == k || l == i || l == j || k == j || k == i || j == i)continue;
                        res[cnt++] = { e[i],e[j],e[k],e[l] };
                    }
                }

            }

        }

        for (int i = 0; i < cnt; i++) {
            int a = res[i][0];
            int b = res[i][1];
            int c = res[i][2];
            int d = res[i][3];
            if (!g[a].count(b) && !g[c].count(d)) {
                //cout<<"aaaa"<<endl;
                //cout<<a<<" "<<b<<" "<<c<<" "<<d<<endl;
                return true;
            }
        }
        return false;

    }
};