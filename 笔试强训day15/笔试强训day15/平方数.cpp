#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<math.h>
#include<algorithm>
using namespace std;
typedef long long ll;
int main() {
    ll x;
    cin >> x;
    ll a = sqrt(x);
    if (abs(a * a - x) < abs((a + 1) * (a + 1) - x)) {
        cout << a * a << endl;
    }
    else {
        cout << (a + 1) * (a + 1) << endl;
    }
    return 0;
}
