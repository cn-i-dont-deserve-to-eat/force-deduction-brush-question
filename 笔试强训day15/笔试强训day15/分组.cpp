#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<algorithm>
#include<unordered_map>
using namespace std;
const int N = 1e5 + 10;
unordered_map<int, int> mp;
int mr;
int n;
int m;

bool check(int mid) {//mid为人数最多的数量
    if (mr < mid)return false;
    int res = 0;
    for (auto it : mp) {
        if (it.second > mid) {
            res += (it.second + mid - 1) / mid;
        }
        else if (it.second <= mid) {
            res++;
        }
    }
    if (res > m)return false;
    return true;
}

int main() {
    cin >> n >> m;

    for (int i = 1; i <= n; i++) {
        int x;
        cin >> x;
        mp[x]++;
        mr = max(mr, mp[x]);
    }

    int l = 0, r = mr + 1;
    while (l + 1 != r) {
        int mid = (l + r) >> 1;
        if (check(mid))r = mid;
        else l = mid;
    }
    if (r != mr + 1)cout << r << endl;
    else cout << -1 << endl;
    return 0;
}
