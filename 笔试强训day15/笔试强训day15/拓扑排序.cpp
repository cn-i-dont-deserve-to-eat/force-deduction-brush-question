#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<vector>
#include<queue>
using namespace std;
const int N = 2e5 + 10;
int n, m;
int d[N];
vector<int> ans;

bool topu(vector<vector<int>>& g) {
    queue<int> q;
    for (int i = 1; i <= n; i++) {
        if (d[i] == 0) {
            ans.push_back(i);
            q.push(i);
        }
    }

    while (!q.empty()) {
        int t = q.front();
        q.pop();
        //cout<<t<<" kkk ";

        for (auto it : g[t]) {
            d[it]--;
            if (d[it] == 0) {
                q.push(it);
                ans.push_back(it);
            }
        }
    }
    //cout<<ans.size()<<endl;
    if (ans.size() == n)return true;
    return false;
}

int main() {
    cin >> n >> m;
    vector<vector<int>> g(n + 1);
    while (m--) {
        int a, b;
        cin >> a >> b;
        g[a].push_back(b);
        d[b]++;
    }
    if (!topu(g))cout << -1 << endl;
    else {
        for (int i = 0; i < ans.size(); i++) {
            if (i != ans.size() - 1)cout << ans[i] << " ";
            else cout << ans[i];
        }
    }

    return 0;
}
