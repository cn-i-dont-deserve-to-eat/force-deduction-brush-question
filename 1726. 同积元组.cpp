#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int tupleSameProduct(vector<int>& nums) {
        vector<int> num1;
        sort(nums.begin(), nums.end());
        nums.erase(unique(nums.begin(), nums.end()), nums.end());//排序去重
        map<int, int> mp;
        for (int i = 0; i < nums.size(); i++) {
            for (int j = i + 1; j < nums.size(); j++) {
                num1.push_back(nums[i] * nums[j]);
                mp[nums[i] * nums[j]]++;//映射二元积的数量
            }
        }       
        int ans = 0;
        for (int i = 0; i < num1.size(); i++) {
            if (mp[num1[i]] >= 2) {
                ans += mp[num1[i]] - 1;
                mp[num1[i]]--;//消除影响
            }
        }
        return ans * 8//每一个四积元组的顺序可以有八种
    }
};