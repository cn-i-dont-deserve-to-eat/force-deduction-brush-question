#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
#include<math.h>
using namespace std;
typedef long long LL;

int get_p(int x) {
	int res = 0;
	while (x) {
		x /= 10;
		res++;
	}
	return res;
}


LL fun(int x,int t) {
	if (x < t)return 0;
	int n = get_p(x);
	LL res = 0;

	for (int i = n; i >= 1; i--) {
		int p = pow(10, i-1);
		int d = x % p;
		int g = x / p/10;
		int k = x / p % 10;
			
		if (t)res += (LL)g * p;
		else res += (LL)(g - 1) * p;

		if (k > t)res += (LL)p;
		if (k == t)res += (LL)d + 1;

	}
	return res;
}


int main() {

	int l, r;
	cin >> l >> r;
	cout<<fun( 1, 2) << endl;
	cout << fun(r,2) - fun(l-1,2);

	return 0;
}