#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:

    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        unordered_map<int, int> mp;
        vector<int> ans;
        for (auto it : nums1)mp[it] = 1;
        for (auto it : nums2) {
            if (mp.count(it) && mp[it] == 1) {
                ans.push_back(it);
                mp[it]--;
            }
        }
        return ans;
    }
};