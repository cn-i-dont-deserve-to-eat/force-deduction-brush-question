#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<string>
#include<stack>
#include<vector>
using namespace std;

int main() {
    string str;
    cin >> str;
    stack<char> st;
    for (int i = 0; i < str.size(); i++) {
        if (st.empty()) {
            st.push(str[i]);
            continue;
        }
        if (st.top() == str[i]) {
            st.pop();
        }
        else {
            st.push(str[i]);
        }
    }
    string ans;
    if (st.empty()) {
        cout << 0;
        return 0;
    }
    while (!st.empty()) {
        ans += st.top();
        st.pop();
    }
    for (int i = ans.size() - 1; i >= 0; i--)cout << ans[i];

}
