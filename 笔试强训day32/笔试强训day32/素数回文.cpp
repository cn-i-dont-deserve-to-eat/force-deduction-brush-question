#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<vector>
#include<algorithm>
using namespace std;

bool isprime(long long x) {
    if (x == 1)return false;
    for (int i = 2; i <= x / i; i++) {
        if (x % i == 0)return false;
    }
    return true;
}

int main() {
    string str;
    cin >> str;
    int n = str.size();
    for (int i = n - 2; i >= 0; i--) {
        str.push_back(str[i]);
    }
    long long ans = 0;
    for (int i = 0; i < str.size(); i++) {
        int u = str[i] - '0';
        ans = ans * 10 + u;
    }
    if (isprime(ans)) {
        cout << "prime" << endl;
    }
    else {
        cout << "noprime" << endl;
    }
    return 0;
}
