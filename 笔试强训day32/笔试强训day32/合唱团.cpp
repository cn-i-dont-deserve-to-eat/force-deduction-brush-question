#define _CRT_SECURE_NO_WARNINGS 1
#include <climits>
#include <iostream>
#include<vector>
using namespace std;
typedef long long LL;
const int N = 1100;
LL a[N];

int main() {
    int n, k, d;
    cin >> n >> k >> d;
    for (int i = 0; i < n; i++)cin >> a[i];
    vector<vector<LL>> minf(n + 1, vector<LL>(k + 1));
    vector<vector<LL>> maxf(n + 1, vector<LL>(k + 1));
    for (int i = 0; i < n; i++) {
        minf[i][1] = a[i];
        maxf[i][1] = a[i];
    }
    for (int i = 0; i < n; i++) {
        for (int j = 2; j <= k; j++) {
            for (int z = max(0, i - d); z < i; z++) {
                maxf[i][j] = max(max(maxf[z][j - 1] * a[i], minf[z][j - 1] * a[i]), maxf[i][j]);
                minf[i][j] = min(min(maxf[z][j - 1] * a[i], minf[z][j - 1] * a[i]), minf[i][j]);
            }
        }
    }
    LL ans = maxf[0][k];
    for (int i = 1; i < n; i++) {
        ans = max(ans, maxf[i][k]);
    }
    cout << ans << endl;
    return 0;
}
