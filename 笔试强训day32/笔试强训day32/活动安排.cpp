#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<algorithm>
#include<vector>
using namespace std;
typedef pair<int, int> PII;

bool cmp(PII& A, PII& B) {
    if (A.second == B.second) {
        return A.first < B.first;
    }
    return A.second < B.second;
}

int main() {
    int n;
    cin >> n;
    vector<PII> v(n);
    for (int i = 0; i < n; i++) {
        cin >> v[i].first >> v[i].second;
    }

    sort(v.begin(), v.end(), cmp);
    int ans = 1;
    int t = v[0].second;
    for (int i = 1; i < n; i++) {
        if (v[i].first >= t) {
            t = v[i].second;
            ans++;
        }
    }
    cout << ans << endl;
    return 0;
}
