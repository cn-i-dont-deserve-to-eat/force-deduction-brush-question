#define _CRT_SECURE_NO_WARNINGS 1

class Solution {
public:

    int lengthOfLongestSubsequence(vector<int>& nums, int target) {
        vector<int> f(target + 1, INT_MIN);
        f[0] = 0;
        int s = 0;
        for (int i = 0; i < nums.size(); i++) {
            for (int j = target; j >= nums[i]; j--) {
                f[j] = max(f[j], f[j - nums[i]] + 1);
            }
        }


        return max(-1, f[target]);
    }
};