#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int minCostClimbingStairs(vector<int>& cost) {
        int n = cost.size();
        vector<int>f(n + 10);
        vector<int>f1(n + 10);
        f[0] = 0;
        f[1] = 0;

        for (int i = 2; i <= n; i++) {
            f[i] = min(f[i - 1] + cost[i - 1], f[i - 2] + cost[i - 2]);
        }
        f1[0] = 0;
        f1[1] = cost[0];
        for (int i = 2; i <= n; i++) {
            f1[i] = min(f1[i - 1] + cost[i - 1], f1[i - 2] + cost[i - 2]);
        }
        return min(f1[n], f[n]);
    }
};