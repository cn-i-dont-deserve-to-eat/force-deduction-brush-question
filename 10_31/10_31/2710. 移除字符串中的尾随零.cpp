#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    string removeTrailingZeros(string num) {
        if (num.size() == 1 && num[0] == '0')return "0";
        int len = num.size();
        string ans = "";
        int k = len;
        while (num[k - 1] == '0' && k - 1 > 0) {
            k--;
        }

        ans = num.substr(0, k);
        return ans;
    }
};