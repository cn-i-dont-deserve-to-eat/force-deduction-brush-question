#define _CRT_SECURE_NO_WARNINGS 1
class MyStack {
public:
    MyStack() {

    }

    void push(int x) {
        q1.push(x);
    }

    int pop() {
        int sz1 = q1.size();
        for (int i = 0; i < sz1 - 1; i++) {
            int t = q1.front();
            q2.push(t);
            q1.pop();
        }
        int res = q1.front();
        q1.pop();
        while (!q2.empty()) {
            int t = q2.front();
            q2.pop();
            q1.push(t);
        }
        return res;
    }

    int top() {
        int sz1 = q1.size();
        for (int i = 0; i < sz1 - 1; i++) {
            int t = q1.front();
            q2.push(t);
            q1.pop();
        }
        int res = q1.front();
        q1.pop();
        q2.push(res);
        while (!q2.empty()) {
            int t = q2.front();
            q2.pop();
            q1.push(t);
        }
        return res;
    }

    bool empty() {
        return q1.size() == 0;
    }
    queue<int> q1;
    queue<int> q2;
};

/**
 * Your MyStack object will be instantiated and called as such:
 * MyStack* obj = new MyStack();
 * obj->push(x);
 * int param_2 = obj->pop();
 * int param_3 = obj->top();
 * bool param_4 = obj->empty();
 */