#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<int> singleNumber(vector<int>& nums) {
        int a = 0;
        int b = 0;
        int res = 0;
        for (int i = 0; i < nums.size(); i++) {
            res ^= nums[i];
        }

        int pos = 0;
        for (int i = 0; i < 32; i++) {
            if ((res >> i) & 1) {
                pos = i;
                break;
            }
        }

        for (int i = 0; i < nums.size(); i++) {
            if ((nums[i] >> pos) & 1) {
                a ^= nums[i];
            }
            else {
                b ^= nums[i];
            }
        }

        vector<int> ans;
        ans.push_back(a);
        ans.push_back(b);
        return ans;
    }
};