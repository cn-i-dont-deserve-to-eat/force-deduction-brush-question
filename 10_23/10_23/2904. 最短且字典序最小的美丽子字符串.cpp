#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    string shortestBeautifulSubstring(string s, int k) {
        vector<string> buti;

        int length = s.size();
        int f1 = 0;
        for (int len = 1; len <= length && !f1; len++) {
            for (int l = 0; l < length && l + len - 1 < length; l++) {
                int r = l + len - 1;
                int f = 0;
                int cnt = 0;
                string t;
                for (int i = l; i <= r; i++) {
                    t += s[i];
                    if (s[i] == '1') {
                        cnt++;
                        if (cnt >= k) {
                            f = 1;
                            buti.push_back(t);
                            break;
                        }
                    }
                }
                if (f) {
                    f1 = len;
                }
            }
        }
        if (f1 == 0)return "";
        string ans;
        for (int i = 0; i < f1; i++) {
            ans += '1';
        }
        for (auto it : buti) {
            if (ans > it) {
                ans = it;
            }
        }

        return ans;

    }
};