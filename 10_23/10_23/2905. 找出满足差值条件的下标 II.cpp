#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<int> findIndices(vector<int>& nums, int indexDifference, int valueDifference) {

        int lma = 0;
        int lmi = 1e9;
        int lma_pos = 0;
        int lmi_pos = 0;
        int len = nums.size();
        if (indexDifference > len - 1)return { -1,-1 };
        vector<int>rma(len + 10);
        vector<int>rmi(len + 10);
        vector<int>rma_pos(len + 10);
        vector<int>rmi_pos(len + 10);
        rma[len - 1] = rmi[len - 1] = nums[len - 1];
        rma_pos[len - 1] = rmi_pos[len - 1] = len - 1;
        for (int i = len - 2; i >= 0; i--) {
            if (rma[i + 1] < nums[i]) {
                rma[i] = nums[i];
                rma_pos[i] = i;
            }
            else {
                rma[i] = rma[i + 1];
                rma_pos[i] = rma_pos[i + 1];
            }

            if (rmi[i + 1] > nums[i]) {
                rmi[i] = nums[i];
                rmi_pos[i] = i;
            }
            else {
                rmi[i] = rmi[i + 1];
                rmi_pos[i] = rmi_pos[i + 1];
            }
        }
        int f = 0;
        for (int l = 0; l + indexDifference < len; l++) {
            if (nums[l] > lma) {
                lma = nums[l];
                lma_pos = l;
            }
            if (nums[l] < lmi) {
                lmi = nums[l];
                lmi_pos = l;
            }
            if (abs(lma - rmi[l + indexDifference]) >= valueDifference) {
                return { lma_pos,rmi_pos[l + indexDifference] };
            }
            if (abs(lmi - rma[l + indexDifference]) >= valueDifference) {
                return { lmi_pos,rma_pos[l + indexDifference] };
            }


        }
        return { -1,-1 };

    }
};