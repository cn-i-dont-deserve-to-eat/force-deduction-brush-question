#define _CRT_SECURE_NO_WARNINGS 1
const int N = 1e5 + 10, M = 31 * N;//每个元素都有31位二进制序列
int son[M][2], idx;


class Solution {
public:
	void insert(int x) {
		int p = 0;
		for (int i = 30; i >= 0; i--) {
			int u = x >> i & 1;
			if (!son[p][u])son[p][u] = ++idx;//更新节点编号
			p = son[p][u];//找到儿子节点的编号
		}
	}

	long long query(int x) {
		long long res = 0;
		int p = 0;
		for (int i = 30; i >= 0; i--) {
			int u = x >> i & 1;
			if (son[p][!u]) {
				res = res * 2 + 1;//在当前节点存在！u,说明这位异或得1
				p = son[p][!u];
			}
			else {
				res = res * 2;//在当前节点不存在！u,说明这位异或得0
				p = son[p][u];
			}
		}
		return res;
	}
	int findMaximumXOR(vector<int>& nums) {
		memset(son, 0, sizeof(son));
		idx = 0;
		int ans = 0;
		for (auto it : nums) {
			insert(it);
			int res = query(it);
			ans = max(ans, res);
		}
		return  ans;

	}
};