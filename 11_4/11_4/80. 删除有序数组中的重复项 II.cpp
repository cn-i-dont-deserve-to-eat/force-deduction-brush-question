#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        if (nums.size() <= 2)return nums.size();
        int len = nums.size();
        int a = nums[0];
        int count = 1;
        int ans = len;
        int j = 0;
        for (int i = 1; i < len; i++) {
            if (nums[i] == a) {
                count++;

            }
            else {
                count = min(count, 2);
                while (count--) {
                    nums[j++] = a;
                }
                count = 1;
                a = nums[i];
            }
        }
        count = min(count, 2);
        while (count--) {
            nums[j++] = a;
        }
        return j;
    }
};