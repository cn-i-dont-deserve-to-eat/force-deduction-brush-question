#define _CRT_SECURE_NO_WARNINGS 1
bool cmp(struct TreeNode* root1, struct TreeNode* root2) {
    if (root1 == NULL && root2 == NULL)return true;
    if (root1 == NULL && root2 != NULL)return false;
    if (root1 != NULL && root2 == NULL)return false;
    if (root1->val == root2->val) {
        return cmp(root1->right, root2->left) && cmp(root1->left, root2->right);
    }
    else return false;
}

bool isSymmetric(struct TreeNode* root) {
    return cmp(root->left, root->right);
}