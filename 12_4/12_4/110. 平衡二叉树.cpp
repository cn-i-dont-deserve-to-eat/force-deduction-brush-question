#define _CRT_SECURE_NO_WARNINGS 1
int geth(struct TreeNode* root) {
    if (root == NULL)return 0;
    return fmax(geth(root->left), geth(root->right)) + 1;
}
bool pre(struct TreeNode* root) {
    if (root == NULL)return true;

    if (abs(geth(root->left) - geth(root->right)) <= 1)return pre(root->left) && pre(root->right);
    return false;
}
bool isBalanced(struct TreeNode* root) {
    return pre(root);
}