#define _CRT_SECURE_NO_WARNINGS 1
void uninordered(struct TreeNode* root, int* sum) {
    if (root == NULL)return;
    uninordered(root->right, sum);
    (*sum) += root->val;
    root->val = (*sum);
    uninordered(root->left, sum);
}


struct TreeNode* bstToGst(struct TreeNode* root) {
    int sum = 0;
    uninordered(root, &sum);
    return root;
}