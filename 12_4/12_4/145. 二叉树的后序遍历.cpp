#define _CRT_SECURE_NO_WARNINGS 1
int gets(struct TreeNode* root) {
    if (root == NULL)return 0;
    return gets(root->left) + gets(root->right) + 1;
}

void lastordered(struct TreeNode* root, int* ans, int* pi) {
    if (root == NULL)return;
    int pos = pi;
    // (*pi)++;
    lastordered(root->left, ans, pi);
    lastordered(root->right, ans, pi);
    ans[*(pi)] = root->val;
    (*pi)++;
}

int* postorderTraversal(struct TreeNode* root, int* returnSize) {
    int num = gets(root);
    *returnSize = num;
    int* ans = (int*)malloc(sizeof(int) * num);
    int x = 0;
    lastordered(root, ans, &x);
    return ans;
}