#define _CRT_SECURE_NO_WARNINGS 1
int gets(struct TreeNode* root) {
    if (root == NULL)return 0;
    return gets(root->left) + gets(root->right) + 1;
}
void inordered(struct TreeNode* root, int* ans, int* pi) {
    if (root == NULL)return;
    inordered(root->left, ans, pi);
    ans[(*pi)] = root->val;
    (*pi)++;
    inordered(root->right, ans, pi);
}
int* inorderTraversal(struct TreeNode* root, int* returnSize) {
    int num = gets(root);
    *returnSize = num;
    int* ans = (int*)malloc(sizeof(int) * num);
    int x = 0;
    inordered(root, ans, &x);
    return ans;
}