#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool canReach(vector<int>& arr, int start) {
        int n = arr.size();
        queue<int> q;
        q.push(start);
        bool v[n];
        memset(v, false, sizeof(v));
        while (!q.empty()) {
            int sz = q.size();
            for (int i = 0; i < sz; i++) {
                auto it = q.front();
                q.pop();

                if (arr[it] == 0)return true;
                if (v[it])continue;
                v[it] = true;
                if (it + arr[it] >= 0 && it + arr[it] < n && !v[arr[it] + it]) {
                    q.push(it + arr[it]);
                }
                if (it - arr[it] >= 0 && it - arr[it] < n && !v[it - arr[it]]) {
                    q.push(it - arr[it]);
                }

            }
        }
        return false;
    }
};