
#include <iostream>
#include <vector>
#include<stdio.h>
#include <cstdio>

using namespace std;
const int N = 2e5 + 10;
int arr[N];


int main() {
    int n = 0;
    scanf("%d", &n);
    int x1 = 0;
    for (int i = 1; i <= n; i++) {
        int x2;
        scanf("%d", &x2);
        arr[i] = x2 - x1;
        x1 = x2;
    }
    int L = 0;
    scanf("%d", &L);
    int q = 0;
    scanf("%d", &q);
    while (q-- > 0) {
        int l = 0;
        int r = 0;
        scanf("%d%d", &l, &r);
        if (r < l) {
            int temp = r;
            r = l;
            l = temp;
        }
        int count = 0;
        int cnt = 1;
        for (int i = l + 1; i <= r; i++) {
            if (count + arr[i] > L) {
                cnt++;
                i--;
                count = 0;
            }
            else {
                count += arr[i];
            }
        }
        printf("%d\n", cnt);
    }
    return 0;
}
