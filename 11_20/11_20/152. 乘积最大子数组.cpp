#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int maxProduct(vector<int>& nums) {
        int len = nums.size();

        vector<int>dpmax(len + 1), dpmin(len + 1);
        dpmin[0] = dpmax[0] = nums[0];
        for (int i = 1; i < len; i++) {
            dpmax[i] = max(dpmax[i - 1] * nums[i], max(dpmin[i - 1] * nums[i], nums[i]));
            dpmin[i] = min({ dpmin[i - 1] * nums[i],dpmax[i - 1] * nums[i],nums[i] });
        }
        int ans = dpmax[0];
        for (int i = 1; i < len; i++) {
            ans = max(ans, dpmax[i]);
        }
        return ans;
    }
};