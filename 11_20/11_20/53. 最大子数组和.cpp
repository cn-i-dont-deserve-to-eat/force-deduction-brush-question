#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        int len = nums.size();
        if (len == 1)return nums[0];
        vector<int> dp(len + 1);
        int f = 0;
        int temp = -1e9;
        dp[0] = nums[0];
        for (int i = 1; i < len; i++) {
            if (dp[i - 1] > 0) {
                dp[i] = dp[i - 1] + nums[i];
            }
            else {
                dp[i] = nums[i];
            }
        }

        int ans = dp[0];
        for (int i = 1; i < len; i++) {
            ans = max(dp[i], ans);
        }
        // if(ans<0)return 
       // if(ans<0)
        return ans;

    }
};