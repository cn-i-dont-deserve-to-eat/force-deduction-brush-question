#define _CRT_SECURE_NO_WARNINGS 1
const int mod = 1e9 + 7;
typedef long long LL;
LL gui(LL a, LL b) {
    LL res = 0;
    while (b) {
        if (b & 1)res = (res + a) % mod;
        b >>= 1;
        a = (a + a) % mod;
    }
    return res % mod;
}
LL qmi(LL a, LL b) {
    LL res = 1;
    while (b) {
        if (b & 1)res = gui(res, a) % mod;
        b >>= 1;
        a = gui(a, a) % mod;
    }
    return res % mod;
}

class Solution {
public:
    LL get_pow(int a, int b) {
        LL res = 1;
        while (b--) {
            res = res * a;
        }
        return res;
    }
    int minNonZeroProduct(int p) {
        LL k = get_pow(2, p) - 1;
        k %= mod;
        LL n = get_pow(2, p - 1) - 1;
        // n%=mod;
        LL m = get_pow(2, p) - 2;
        //  m%=mod;
         //cout<<k<<" "<<n<<" "<<m<<endl;
        LL ans = qmi(m, n);
        //  cout<<ans<<" "<<k<<endl;
         // ans=gui(ans,k)%mod;
        ans = (ans * k) % mod;
        return ans;
    }
};