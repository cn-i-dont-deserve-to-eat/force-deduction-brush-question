#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;
const int N = 2e4 + 10;
int st[N];
vector<int> nums(N);
int main() {
	int n;
	int k;
	int m;
	cin >> n>>k>>m;
	for (int i = 0; i < n; i++) {
		cin >> nums[i];

	}
	int ans = 0;
	int ans1 = 0;
	int ans3 = 0;
	for (int i = 0, j = 0; i < n; i++) {
		st[nums[i]]++;
		if (st[nums[i]] == 1)ans3++;
		ans1 += nums[i];
		
		if (i - j + 1 >= k) {
			if (ans3 < m) {
				st[nums[j]]--;
				ans1 -= nums[j];
				if (st[nums[j]] == 0)ans3--;
				j++;
			}
			else {
				ans = max(ans, ans1);
				st[nums[j]]--;
				ans1 -= nums[j];
				if (st[nums[j]] == 0)ans3--;
				j++;

			}
		}
	}
	cout << ans;

	return 0;
}