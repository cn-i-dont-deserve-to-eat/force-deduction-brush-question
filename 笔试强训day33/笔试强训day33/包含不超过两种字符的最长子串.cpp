#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

int a[200];
int main() {
    string str;
    cin >> str;
    int n = str.size();
    int ans = 0;
    int cnt = 0;
    for (int i = 0, j = 0; i < n; i++) {
        a[str[i]]++;
        if (a[str[i]] == 1)cnt++;
        while (j < i && cnt>2) {
            a[str[j]]--;
            if (a[str[j]] == 0)cnt--;
            j++;
        }
        ans = max(ans, i - j + 1);
    }
    cout << ans << endl;

    return 0;
}
  