#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    void dfs(int u, vector<string>& ans, string t, int n, string str, vector<bool>& st) {
        if (u == n) {
            ans.push_back(t);
            return;
        }
        for (int i = 0; i < n; i++) {
            if (!st[i]) {
                st[i] = true;
                t.push_back(str[i]);
                dfs(u + 1, ans, t, n, str, st);
                st[i] = false;
                t.pop_back();
            }
        }
    }
    vector<string> Permutation(string str) {
        int n = str.size();
        vector<bool> st(300, false);
        vector<string> ans;
        string t = "";
        dfs(0, ans, t, n, str, st);
        sort(ans.begin(), ans.end());
        ans.erase(unique(ans.begin(), ans.end()), ans.end());
        return ans;
    }
};