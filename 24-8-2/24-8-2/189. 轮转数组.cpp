#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        int n = nums.size();
        vector<int> ans(n * 2 + 10);
        k = k % n;
        for (int i = 0; i < n; i++)ans[i] = nums[i];
        for (int i = n; i < 2 * n; i++)ans[i] = nums[i - n];
        int l = n - k;
        int r = n - k + n - 1;
        for (int i = l; i <= r; i++)
        {
            nums[i - l] = ans[i];
        }
    }
};

class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        int n = nums.size();
        k %= n;
        reverse(nums.begin(), nums.end());
        reverse(nums.begin(), nums.begin() + k);
        reverse(nums.begin() + k, nums.end());
    }
};