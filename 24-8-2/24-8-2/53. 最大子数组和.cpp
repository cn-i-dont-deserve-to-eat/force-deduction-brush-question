#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        int n = nums.size();
        vector<int> f(n + 1);
        f[1] = nums[0];
        for (int i = 2; i <= n; i++)
        {
            f[i] = max(nums[i - 1], f[i - 1] + nums[i - 1]);
        }
        int ans = -1e9;
        for (int i = 1; i <= n; i++)
        {
            ans = max(ans, f[i]);
        }
        return ans;
    }
};