#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<vector<int>> merge(vector<vector<int>>& intervals) {
        sort(intervals.begin(), intervals.end());
        int l = 0;
        int r = 0;
        int n = intervals.size();
        l = intervals[0][0];
        r = intervals[0][1];
        vector<vector<int>> v;
        for (int i = 1; i < n; i++)
        {
            if (intervals[i][0] > r)
            {
                v.push_back({ l,r });
                l = intervals[i][0];
                r = intervals[i][1];
            }
            else {
                r = max(r, intervals[i][1]);
            }
        }

        v.push_back({ l,r });
        return v;
    }
};