#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int minimumSubarrayLength(vector<int>& nums, int k) {
        int n = nums.size();
        int ans = 0;
        for (int len = 1; len <= n; len++) {
            for (int l = 0; l + len - 1 < n; l++) {
                int r = l + len - 1;
                int res = 0;
                for (int j = l; j <= r; j++) {
                    res |= nums[j];
                }
                if (res >= k) {
                    ans = len;
                    return ans;
                }
            }
        }
        return -1;
    }
};