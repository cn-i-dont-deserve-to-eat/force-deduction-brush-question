#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int minimumLevels(vector<int>& possible) {
        int n = possible.size();
        vector<int>s(n);
        s[0] = possible[0] == 0 ? -1 : 1;
        for (int i = 1; i < n; i++) {
            if (!possible[i])s[i] += s[i - 1] - 1;
            else s[i] += s[i - 1] + 1;
        }
        int ans = 0;
        for (int i = 0; i < n - 1; i++) {
            if (s[i] > s[n - 1] - s[i]) {
                return i + 1;
            }
        }
        return -1;
    }
};