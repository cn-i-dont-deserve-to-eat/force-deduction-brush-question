#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int minimumSubarrayLength(vector<int>& nums, int k) {
        int n = nums.size();
        int arr[32] = { 0 };

        int ans = 1e6;
        int sum = 0;
        for (int i = 0, j = 0; i < n; i++) {
            sum |= nums[i];
            for (int k = 31; k >= 0; k--) {
                if ((nums[i] >> k) & 1) {
                    arr[k]++;
                }
            }
            if (sum < k)continue;

            while (sum >= k && j <= i) {
                for (int k = 31; k >= 0; k--) {
                    if ((nums[j] >> k) & 1) {
                        arr[k]--;
                    }
                }

                j++;
                int t = 0;
                for (int k = 0; k < 32; k++) {
                    if (arr[k]) {
                        t = t | (1 << k);
                    }
                }
                sum = t;
            }

            for (int k = 31; k >= 0; k--) {
                if ((nums[j - 1] >> k) & 1) {
                    arr[k]++;
                }
            }
            j--;
            ans = min(ans, i - j + 1);
            //  cout<<ans<<" ";
        }
        if (ans == 1e6)return -1;
        return ans;
    }
};