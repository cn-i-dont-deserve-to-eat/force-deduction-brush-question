#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int maximumSum(vector<int>& nums) {
        map<int, vector<int>> mp;
        int len = nums.size();
        vector<int> cnt(len);
        for (int i = 0; i < nums.size(); i++) {
            int k = 0;
            int t = nums[i];
            while (t) {
                k += t % 10;
                t /= 10;
            }
            cnt[i] = k;
            mp[k].push_back(nums[i]);
            // mp[k]++;
        }
        for (auto& it : mp) {
            sort(it.second.begin(), it.second.end());
            // cout<<it.first<<"----";
            // for(auto a:it.second){
            //     cout<<a<<" ";
            // }
            // cout<<endl;
        }
        int ans = 0;
        for (auto it : mp) {
            int a = it.first;
            // vector<int> b=it.second;
            int l = it.second.size();
            if (l < 2)continue;
            //cout<<"==="<<l<<" "<<it.second[l-1]<<" "<<it.second[l-2]<<endl;
            ans = max(ans, it.second[l - 1] + it.second[l - 2]);
        }
        if (ans == 0)return -1;
        return ans;

    }
};