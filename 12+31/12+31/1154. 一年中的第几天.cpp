#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool isprim(int x) {
        if ((x % 4 == 0 && x % 100 != 0) || (x % 400 == 0))return true;
        return false;
    }
    int dayOfYear(string date) {
        int mon[12] = { 31,28,31,30,31,30,31,31,30,31,30,31 };
        int y = 0;
        int m = 0;
        int d = 0;
        for (int i = 0; i < 4; i++) {
            y = y * 10 + date[i] - '0';
        }
        for (int i = 5; i <= 6; i++) {
            m = m * 10 + date[i] - '0';
        }
        for (int i = 8; i <= 9; i++) {
            d = d * 10 + date[i] - '0';
        }
        int ans = 0;

        for (int i = 1; i <= m - 1; i++) {
            if (i == 2 && isprim(y))ans++;
            ans += mon[i - 1];
        }
        ans += d;
        return ans;

    }
};