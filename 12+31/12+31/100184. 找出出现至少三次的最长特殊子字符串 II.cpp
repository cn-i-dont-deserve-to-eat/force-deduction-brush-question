#define _CRT_SECURE_NO_WARNINGS 1
bool cmp(int a, int b) {
    return a > b;
}
class Solution {
public:

    int maximumLength(string s) {
        map<int, vector<int>> mp;
        int len = s.size();
        int j = 0;
        for (int i = 1; i < len; i++) {
            if (s[i] != s[i - 1]) {
                int u = s[i - 1] - 'a';
                mp[u].push_back(i - j);
                j = i;
            }
        }
        // if(s[len-1]==s[len-2]){
        int u = s[len - 1] - 'a';
        mp[u].push_back(len - j);
        // }
         // for(auto it:mp){
         //     char u=it.first+'a';
         //     cout<<u<<" ";
         //     for(auto v:it.second){
         //         cout<<v<<" ";
         //     }
         //     cout<<endl;
         // }
        int ans = 0;
        for (auto it : mp) {
            int n = it.second.size();
            vector<int>res = it.second;
            if (n == 1) {
                if (res[0] >= 3)ans = max(ans, res[0] - 2);
                continue;
            }
            sort(res.begin(), res.end(), cmp);
            //  for(auto v:it.second){
            //     cout<<v<<" ";
            // }
            cout << endl;
            if (n == 2) {
                cout << it.first << " " << res[0] << endl;
                ans = max({ ans,res[0] - 2,min(res[0] - 1,res[1]) });
            }
            else {
                ans = max({ ans,res[0] - 2,min(res[0] - 1,res[1]),min({res[0],res[1],res[2]}) });
            }
        }
        if (ans == 0)ans = -1;
        return ans;
    }
};