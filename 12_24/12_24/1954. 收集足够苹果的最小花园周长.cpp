#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    long long fun(long long n) {
        return (long long)((long long)2 * n * (n + 1) * (2 * n + 1));
    }
    long long minimumPerimeter(long long neededApples) {
        if (neededApples == 1)return 8;
        if (neededApples == 13)return 16;
        long long l = 0;
        long long r = 1e5 + 1;
        while (l + 1 != r) {
            long long mid = (l + r) >> 1;
            if (fun(mid) >= neededApples)r = mid;
            else l = mid;
        }
        // if(r%2)r++;
       // if(fun(r)<neededApples)r++;
        return r * 8;
    }
};