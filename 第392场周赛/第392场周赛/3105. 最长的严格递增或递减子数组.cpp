#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int longestMonotonicSubarray(vector<int>& nums) {
        int ans1 = 0;
        int ans2 = 0;
        int n = nums.size();
        if (n == 1)return 1;
        int j1 = 0;
        int j2 = 0;
        for (int i = 1; i < n; i++) {
            if (nums[i] > nums[i - 1]) {
                ans2 = max(ans2, i - j2);
                j2 = i;
            }
            else if (nums[i] < nums[i - 1]) {
                ans1 = max(ans1, i - j1);
                j1 = i;
            }
            else {
                ans2 = max(ans2, i - j2);
                ans1 = max(ans1, i - j1);
                j1 = j2 = i;
            }
        }
        //cout<<ans1<<" "<<ans2<<endl;
        if (nums[n - 1] > nums[n - 2]) {
            ans1 = max(ans1, n - j1);
        }
        if (nums[n - 1] < nums[n - 2]) {
            ans2 = max(ans2, n - j2);
        }
        return max(ans1, ans2);

    }
};