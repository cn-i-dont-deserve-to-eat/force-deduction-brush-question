#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    string getSmallestString(string s, int k) {
        int n = s.size();
        string ans = "";
        for (int i = 0; i < n; i++) {
            ans += s[i];
            if (k <= 0)continue;
            if (s[i] == 'a')continue;
            if ((s[i] - 'a') <= k || 'z' + 1 - s[i] <= k) {
                ans[i] = 'a';
                k -= min((s[i] - 'a'), ('z' + 1 - s[i]));
            }

            else {
                ans[i] = s[i] - k;
                k = 0;
            }
        }
        return ans;

    }
};