#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        int len = strs.size();
        map<string, vector<string>> mp;
        vector<vector<string>> ans;
        for (int i = 0; i < len; i++) {
            string str = strs[i];
            sort(str.begin(), str.end());
            mp[str].push_back(strs[i]);
        }
        int cnt = 0;
        for (auto it : mp) {
            ans.push_back(it.second);
        }
        return ans;
    }
};