#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    long long countSubstrings(string s, char c) {
        long long  sum = 0;
        long long  ans = 0;
        for (int i = 0; i < s.size(); i++) {
            if (s[i] == c) {
                sum++;
                ans += sum;
            }
        }
        return ans;
    }
};