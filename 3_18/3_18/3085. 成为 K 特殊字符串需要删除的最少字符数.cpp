#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int minimumDeletions(string word, int k) {
        int f[26] = { 0 };
        for (int i = 0; i < word.size(); i++) {
            int u = word[i] - 'a';
            f[u]++;
        }
        vector<int> a;
        for (int i = 0; i < 26; i++) {
            if (f[i])a.push_back(f[i]);
        }
        //for(auto it:a)cout<<it<<" ";
        //cout<<endl;
        int ans = 1e9;;
        for (int i = 0; i < a.size(); i++) {
            //  int m=a[i];
            int res = 0;
            for (int j = 0; j < a.size(); j++) {
                if (i == j)continue;
                if (a[j] < a[i]) {
                    res += a[j];
                }
                else {
                    if (a[j] >= a[i] + k) {
                        res += a[j] - a[i] - k;
                    }
                }
            }
            ans = min(res, ans);
        }
        return ans;
    }
};