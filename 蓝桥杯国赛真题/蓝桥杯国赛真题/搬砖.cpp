#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<algorithm>
#include<cstring>
using namespace std;
const int N = 1100, M = 2200;
int w[N], v[N];
int f[N][M];
int main()
{

    int n;
    cin >> n;
    int s = 0;
    for (int i = 0; i < n; i++) {
        cin >> w[i] >> v[i];
        s += w[i];
    }
    memset(f, 0, sizeof f);
    for (int i = 1; i <= n; i++) {
        for (int j = 0; j < s; j++) {
            if (j <= v[i - 1] && j >= w[i - 1])f[i][j] = max(f[i - 1][j], f[i - 1][j - w[i - 1]] + v[i - 1]);
            else f[i][j] = max(f[i - 1][j], f[i][j]);
            cout << f[i][j] << " ";
        }
        cout << endl;
    }
    int ans = 0;
    for (int j = 0; j < s; j++)ans = max(ans, f[n][j]);
    cout << ans << endl;
    return 0;
}