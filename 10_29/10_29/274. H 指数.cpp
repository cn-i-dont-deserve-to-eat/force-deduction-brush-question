#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int hIndex(vector<int>& citations) {
        //if(citations.size()==1&)return 1;
        sort(citations.begin(), citations.end());
        int len = citations.size();
        int max_h = 0;
        for (int i = 0; i < len; i++) {
            int cnt = 0;
            for (int j = i; j < len; j++) {
                if (citations[j] >= citations[i])cnt++;
            }
            if (cnt >= citations[i]) {
                max_h = max(max_h, citations[i]);
            }
            else {
                max_h = max(max_h, cnt);
            }
        }
        // if()
        //  if(len==1&&citations[0]==0)return 0;
        //  if(len==1)return 1;
        return max_h;
    }
};