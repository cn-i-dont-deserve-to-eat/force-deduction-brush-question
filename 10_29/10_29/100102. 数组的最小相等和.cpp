#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    long long minSum(vector<int>& nums1, vector<int>& nums2) {
        long long sum1 = 0;
        long long sum2 = 0;
        int len1 = nums1.size();
        int len2 = nums2.size();
        int cnt1 = 0;
        int cnt2 = 0;
        for (int i = 0; i < len1; i++) {
            if (nums1[i] == 0)cnt1++;
            sum1 += nums1[i];
        }
        for (int i = 0; i < len2; i++) {
            if (nums2[i] == 0)cnt2++;
            sum2 += nums2[i];
        }
        cout << sum1 << "  11 " << cnt1 << endl;
        cout << sum2 << "  22 " << cnt2 << endl;
        if (cnt1 == 0 && sum1 < sum2 + cnt2)return -1;
        if (cnt2 == 0 && sum2 < sum1 + cnt1)return -1;

        if (sum1 + cnt1 > sum2 + cnt2) {
            long long d = sum1 - sum2;
            return sum1 + cnt1;
        }
        else if (sum1 == sum2) {
            return sum1 + max(cnt1, cnt2);
        }
        else {
            return sum2 + cnt2;
        }
    }
};  