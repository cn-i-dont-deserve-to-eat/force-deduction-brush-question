#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int findKOr(vector<int>& nums, int k) {
        int ans = 0;
        for (int i = 0; i < 32; i++) {
            int cnt = 0;
            for (int j = 0; j < nums.size(); j++) {
                if ((1 << i) & nums[j]) {
                    cnt++;
                }
            }
            if (cnt >= k)ans += (1 << i);
        }
        return ans;
    }
};