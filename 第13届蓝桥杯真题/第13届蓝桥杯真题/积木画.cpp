#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>

using namespace std;
const int N = 1e7 + 10;
long long f[N][3];
int p = 1e9 + 7;

int main() {
    int n;
    cin >> n;
    f[1][0] = 1;
    f[1][1] = 0;
    f[1][2] = 0;
    f[0][0] = f[0][1] = f[0][2] = 1;
    for (int i = 2; i <= n; i++) {
        f[i][0] = (f[i - 1][0] + f[i - 2][0] + f[i - 1][1] + f[i - 1][2]) % p;
        f[i][1] = (f[i - 1][2] + f[i - 2][0]) % p;
        f[i][2] = (f[i - 2][0] + f[i - 1][1]) % p;
    }
    cout << f[n][0] << endl;
    return 0;
}