#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
using namespace std;
typedef long long LL;
const int N = 1e5 + 10;
int a[N];
int b[N];
LL p = 1e9 + 7;
int main() {
    int n;
    cin >> n;
    int ma, mb;
    cin >> ma;
    for (int i = 1; i <= ma; i++) {
        cin >> a[i];
    }

    cin >> mb;
    for (int i = ma - mb + 1; i <= ma; i++) {
        cin >> b[i];

    }
    // for(int i=1;i<=ma;i++)cout<<b[i]<<" ";
   //  cout<<endl;
    LL res1 = 0;
    LL res2 = 0;
    // int tt=0;
    for (int i = 1; i <= ma; i++) {
        long long t = 0;
        t = max(a[i], b[i]);
        t = max((LL)2, t + 1);
        res1 = (res1 * t % p + a[i]) % p;
        res2 = (res2 * t % p + b[i]) % p;
    }
    cout << (res1 - res2 + p) % p << endl;

    return 0;
}