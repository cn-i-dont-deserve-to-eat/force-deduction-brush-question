#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>

using namespace std;
const int N = 510;
int a[N][N];
int s[N][N];
int main() {
    int n, m, k;
    cin >> n >> m >> k;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            cin >> a[i][j];
        }
    }

    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            s[i][j] = s[i][j - 1] + s[i - 1][j] - s[i - 1][j - 1] + a[i][j];
            //    cout<<s[i][j]<<" ";
        }
        //  cout<<endl;
    }
    //  cout<<s[2][1]-s[0][1]-s[2][0]+s[0][0]<<endl;
    long long ans = 0;
    for (int c = 1; c <= m; c++) {
        for (int d = c; d <= m; d++) {
            for (int i = 1, j = 1; i <= n; i++) {
                while (j <= i && s[i][d] - s[j - 1][d] - s[i][c - 1] + s[j - 1][c - 1] > k) {
                    j++;
                }
                if (j <= i) {
                    ans += (i - j + 1);
                }
            }
        }
    }
    cout << ans << endl;

    return 0;
}