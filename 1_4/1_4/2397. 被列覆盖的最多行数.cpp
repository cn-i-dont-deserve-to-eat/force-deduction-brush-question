#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int get(int x) {
        int res = 0;
        while (x) {
            int b = x % 2;
            x /= 2;
            if (b)res++;
        }
        return res;
    }
    int maximumRows(vector<vector<int>>& matrix, int numSelect) {
        int m = matrix.size();
        int n = matrix[0].size();
        vector<int>mask(m, 0);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j])mask[i] |= (1 << (n - j - 1));
            }
        }
        int res = 0;
        int cur = 0;
        int li = (1 << n);
        int ans = 0;
        while (cur < li) {
            cur++;
            if (get(cur) != numSelect)continue;
            int t = 0;
            for (int j = 0; j < m; j++) {
                if ((mask[j] & cur) == mask[j]) {
                    t++;
                }
            }
            ans = max(ans, t);
        }
        return ans;
    }
};