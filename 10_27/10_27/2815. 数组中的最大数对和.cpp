#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
    int getmaxp(int x) {
        int res = 0;
        while (x) {
            int b = x % 10;
            res = max(res, b);
            x /= 10;
        }
        return res;
    }
public:
    int maxSum(vector<int>& nums) {
        int len = nums.size();
        int ans = -1;
        for (int i = 0; i < len; i++) {
            for (int j = i + 1; j < len; j++) {
                if (getmaxp(nums[i]) == getmaxp(nums[j])) {
                    ans = max(ans, nums[j] + nums[i]);
                }
            }
        }
        // if(ans==0)
        return ans;
    }
};