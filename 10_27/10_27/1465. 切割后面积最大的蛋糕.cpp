#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int maxArea(int h, int w, vector<int>& horizontalCuts, vector<int>& verticalCuts) {
        int mod = 1e9 + 7;
        int len = horizontalCuts.size();
        sort(horizontalCuts.begin(), horizontalCuts.end());
        long long max_hd = max(horizontalCuts[0] - 0, h - horizontalCuts[len - 1]);

        //int t=horizontalCuts[0];
        for (int i = 1; i < len; i++) {
            if (horizontalCuts[i] - horizontalCuts[i - 1] > max_hd) {
                max_hd = horizontalCuts[i] - horizontalCuts[i - 1];
            }
        }



        sort(verticalCuts.begin(), verticalCuts.end());
        len = verticalCuts.size();
        long long  max_wd = max(verticalCuts[0] - 0, w - verticalCuts[len - 1]);;

        for (int i = 1; i < len; i++) {
            if (verticalCuts[i] - verticalCuts[i - 1] > max_wd) {
                max_wd = verticalCuts[i] - verticalCuts[i - 1];
            }
        }

        return max_hd * max_wd % (mod);

    }
};