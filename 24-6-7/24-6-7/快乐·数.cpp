#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    int getfun(int x) {
        int res = 0;
        while (x) {
            int b = x % 10;
            x /= 10;
            res += b * b;
        }
        return res;
    }
    bool isHappy(int n) {
        unordered_map<int, int> mp;
        while (getfun(n) != 1) {
            n = getfun(n);
            mp[n]++;
            if (mp[n] > 1)return false;
        }
        return true;
    }
};
class Solution {
public:
    int getfun(int x) {
        int res = 0;
        while (x) {
            int b = x % 10;
            x /= 10;
            res += b * b;
        }
        return res;
    }
    bool isHappy(int n) {
        int fast = n;
        int slow = n;
        slow = getfun(slow);
        fast = getfun(fast);
        fast = getfun(fast);
        while (fast != slow) {
            slow = getfun(slow);
            fast = getfun(fast);
            fast = getfun(fast);
        }
        if (fast == 1)return true;
        return false;
    }
};