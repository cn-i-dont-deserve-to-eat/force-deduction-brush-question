#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    string getfun(long long x) {
        int f = 0;
        if (x < 0) {
            f = -1;
            x = -x;
        }
        string res = "";
        if (x == 0)return "0";
        while (x) {
            char b = x % 10 + '0';
            res += b;
            x /= 10;
        }
        reverse(res.begin(), res.end());
        if (f == -1)return "-" + res;
        else return res;
    }
    vector<string> summaryRanges(vector<int>& nums) {
        vector<string> ans;
        int n = nums.size();
        if (n == 0)return {};

        string t = getfun(nums[0]);
        for (int i = 1; i < n; i++) {
            if ((long long)nums[i] - nums[i - 1] != 1) {
                if (t != getfun(nums[i - 1])) {
                    t += "->";
                    t += getfun(nums[i - 1]);
                }
                ans.push_back(t);
                t = getfun(nums[i]);
            }
        }
        if (t != getfun(nums[n - 1])) {
            t += "->";
            t += getfun(nums[n - 1]);
        }
        ans.push_back(t);
        return ans;
    }
};