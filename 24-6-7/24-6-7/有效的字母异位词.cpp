#define _CRT_SECURE_NO_WARNINGS 1
class Solution {
public:
    bool isAnagram(string s, string t) {
        sort(s.begin(), s.end());
        sort(t.begin(), t.end());
        return t == s;
    }
};