#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<algorithm>
using namespace std;
const int N = 1e6 + 100;
long long f[N];

int main() {
    int n;
    cin >> n;
    long long ans = -1e16;
    for (int i = 1; i <= n; i++) {
        cin >> f[i];
        f[i] = max(f[i - 1] + f[i], f[i]);
        ans = max(f[i], ans);
    }
    cout << ans << endl;
    return 0;
}
// 64 λ������� printf("%lld")