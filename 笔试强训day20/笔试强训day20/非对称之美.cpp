#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include<string>
using namespace std;

int main() {
    string str;
    cin >> str;
    if (str.size() <= 1) {
        cout << 0 << endl;
        return 0;
    }
    int l = 0;
    int r = str.size() - 1;

    while (l < r) {
        if (str[l] == str[r]) {
            l++;
            r--;
        }
        else {
            cout << str.size() << endl;
            return 0;
        }
    }
    l = 1;
    r = str.size() - 1;
    while (l < r) {
        if (str[l] == str[r]) {
            l++;
            r--;
        }
        else {
            cout << str.size() - 1 << endl;
            return 0;
        }
    }
    cout << 0 << endl;
    return 0;
}
