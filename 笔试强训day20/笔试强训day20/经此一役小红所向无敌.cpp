#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
typedef long long LL;

int main() {
    LL a, h, b, k;
    cin >> a >> h >> b >> k;

    LL cnt1 = (h - 1 + b) / b;
    LL cnt2 = (k - 1 + a) / a;
    LL cnt = min(cnt1, cnt2);
    LL ans = cnt * (a + b);
    //  cout<<cnt1<<" "<<cnt2<<endl;
    if (cnt1 == cnt2) {
        cout << ans << endl;
        return 0;
    }
    else if (cnt1 > cnt2) {
        ans += a * 10;
    }
    else {
        ans += b * 10;
    }

    cout << ans << endl;
    return 0;
}
